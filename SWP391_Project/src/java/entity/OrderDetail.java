/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author Admin
 */
public class OrderDetail {
    private int id;
    private String image;
    private String name;
    private String category;
    private double price;
    private int quantity;
    private double amount;

    public OrderDetail() {
    }

    public OrderDetail(int id, String image, String name, String category, double price, int quantity, double amount) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.category = category;
        this.price = price;
        this.quantity = quantity;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }



    @Override
    public String toString() {
        return "Order Detail{" +
            "image=" + image +
            ", name='" + name + '\'' +
            ", category='" + category + '\'' +
            ", price='" + price + '\'' +
            ", quantity='" + quantity + '\'' +
            ", amount=" + amount + 
            "}";
    }
    
}
