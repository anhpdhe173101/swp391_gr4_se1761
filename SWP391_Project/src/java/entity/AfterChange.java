/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author Phuc Lam
 */
public class AfterChange {
    private int aID;
    private String fullName;
    private int gender;
    private String phone;
    private String Address;

    public AfterChange() {
    }

    public AfterChange(int aID, String fullName, int gender, String phone, String Address) {
        this.aID = aID;
        this.fullName = fullName;
        this.gender = gender;
        this.phone = phone;
        this.Address = Address;
    }

    public int getaID() {
        return aID;
    }

    public void setaID(int aID) {
        this.aID = aID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }
    
    
    

    
    
      
}
