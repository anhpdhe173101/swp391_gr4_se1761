/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Product {
    private int id;
    private String name;
    private int cateID;
    private double origin_price;
    private double sale_price;
    private String description;
    private String image;
    private int quantity;
    private Date date;
    private int status;
    private String season;
    public Product() {
    }

    public Product(int id, String name, int cateID, double origin_price, double sale_price, String description, String image, int quantity, Date date, int status) {
        this.id = id;
        this.name = name;
        this.cateID = cateID;
        this.origin_price = origin_price;
        this.sale_price = sale_price;
        this.description = description;
        this.image = image;
        this.quantity = quantity;
        this.date = date;
        this.status = status;
    }

    public Product(int id, String name, int cateID, double origin_price, double sale_price, String description, String image, int quantity, Date date, int status, String season) {
        this.id = id;
        this.name = name;
        this.cateID = cateID;
        this.origin_price = origin_price;
        this.sale_price = sale_price;
        this.description = description;
        this.image = image;
        this.quantity = quantity;
        this.date = date;
        this.status = status;
        this.season = season;
    }

    public Product(int id, String name, int cateID, double origin_price, double sale_price, String description, String image, int quantity,  int status, String season) {
        this.id = id;
        this.name = name;
        this.cateID = cateID;
        this.origin_price = origin_price;
        this.sale_price = sale_price;
        this.description = description;
        this.image = image;
        this.quantity = quantity;
        this.status = status;
        this.season = season;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCateID() {
        return cateID;
    }

    public void setCateID(int cateID) {
        this.cateID = cateID;
    }

    public double getOrigin_price() {
        return origin_price;
    }

    public void setOrigin_price(double origin_price) {
        this.origin_price = origin_price;
    }

    public double getSale_price() {
        return sale_price;
    }

    public void setSale_price(double sale_price) {
        this.sale_price = sale_price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    
    @Override
    public String toString() {
    return "Product{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", categoryID=" + cateID +
            ", price=" + origin_price +
            ", salePrice=" + sale_price +
            ", description='" + description + '\'' +
            ", image='" + image + '\'' +
            ", quantity=" + quantity +
            ", date=" + date +
            ", status= "+ status +
            '}';
}
    
    
}
