/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author GL
 */
public final class Cart {

    private List<Item> items;

    public Cart() {
        items = new ArrayList<>();
    }

    public List<Item> getItems() {
        return items;
    }

    //LamTP
    //This function get product form item list
    private Item getItemByProductID(int id) {
        for (Item i : items) {
            if (i.getProduct().getId() == id) {
                return i;
            }
        }
        return null;
    }

    //LamTP
    //This function use to get product quanity from list
    public int getquantityById(int id) {
        return getItemByProductID(id).getQuantity();
    }

    //LamTP
    public void addItem(Item new_item) {
        //check if product is exist
        if (getItemByProductID(new_item.getProduct().getId()) != null) {
            //get product save in Cart
            Item item_inCart = getItemByProductID(new_item.getProduct().getId());
            //Update quanity of product
            item_inCart.setQuantity(item_inCart.getQuantity() + new_item.getQuantity());

            //If product note exist, add new
        } else {
            items.add(new_item);
        }
    }

    public void removeItem(int id) {
        if (getItemByProductID(id) != null) {
            items.remove(getItemByProductID(id));
        }
    }

    public double getTotalMoney() {
        int t = 0;
        for (Item i : items) {
            t += (i.getQuantity() * i.getPrice());
        }
        return t;
    }
    
    
    private Product getProductById(int id, List<Product> list) {
        for (Product i : list) {
            if (i.getId() == id) {
                return i;
            }
        }
        return null;
    }

    public Cart(String txt, List<Product> list) {
        items = new ArrayList<>();
        try {
            //Check if cart exist(it mean cart cookies
            if (txt != null && txt.length() != 0) {
                // We get each product from cookies
                // We split "-" because it use to seperate each item
                String[] item = txt.split("-");              
                for (String item_detail : item) {
                    // ":" use to seperate product id and quantity
                    String[] n = item_detail.split(":");
                    
                    int id = Integer.parseInt(n[0]);
                    int quantity = Integer.parseInt(n[1]);
                    
                    Product p = getProductById(id, list);
                    Item t = new Item(p, quantity,  p.getSale_price());
                    addItem(t);
                }
            }
        } catch (NumberFormatException e) {
        }

    }

}
