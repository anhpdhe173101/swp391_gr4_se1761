/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author Admin
 */
public class ProductChart {
    private String season;
    private int number_Product;
    private String name;
    public ProductChart() {
    }

    public ProductChart(String season, int number_Product) {
        this.season = season;
        this.number_Product = number_Product;
    }

    public ProductChart(int number_Product, String name) {
        this.number_Product = number_Product;
        this.name = name;
    }
    
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public int getNumber_Product() {
        return number_Product;
    }

    public void setNumber_Product(int number_Product) {
        this.number_Product = number_Product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Product Chart {"+
                "name=" + name +
            ", Number Product='" + number_Product + 
            "}";
    }
    
    
}
