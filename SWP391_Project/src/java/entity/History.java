/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Date;

/**
 *
 * @author Phuc Lam
 */
public class History {
    private User change;
    private User edit;
    private Date date;
    private AfterChange after;
    private BeforeChange before;

    public History() {
    }

    public History(User change, User edit, Date date, AfterChange after, BeforeChange before) {
        this.change = change;
        this.edit = edit;
        this.date = date;
        this.after = after;
        this.before = before;
    }

    public User getChange() {
        return change;
    }

    public void setChange(User change) {
        this.change = change;
    }

    public User getEdit() {
        return edit;
    }

    public void setEdit(User edit) {
        this.edit = edit;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AfterChange getAfter() {
        return after;
    }

    public void setAfter(AfterChange after) {
        this.after = after;
    }

    public BeforeChange getBefore() {
        return before;
    }

    public void setBefore(BeforeChange before) {
        this.before = before;
    }
    
    
   
}
