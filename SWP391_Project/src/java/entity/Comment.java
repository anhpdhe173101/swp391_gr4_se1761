/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Date;

/**
 *
 * @author Acer
 */
public class Comment {
    private int commentID;
    private int postUserID;
    private int blogID;
    private String commentContent;
    private String postUserName;
    private Date commentDate;

    public Comment() {
    }

    public Comment(int commentID, int postUserID, int blogID, String commentContent, String postUserName, Date commentDate) {
        this.commentID = commentID;
        this.postUserID = postUserID;
        this.blogID = blogID;
        this.commentContent = commentContent;
        this.postUserName = postUserName;
        this.commentDate = commentDate;
    }

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public int getPostUserID() {
        return postUserID;
    }

    public void setPostUserID(int postUserID) {
        this.postUserID = postUserID;
    }

    public int getBlogID() {
        return blogID;
    }

    public void setBlogID(int blogID) {
        this.blogID = blogID;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getPostUserName() {
        return postUserName;
    }

    public void setPostUserName(String postUserName) {
        this.postUserName = postUserName;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    
}
