/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.OrderChart;
import entity.OrderDetail;
import entity.OrderList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Admin
 */
public class OrderDAO extends DBContext {

    public Vector<OrderList> getAllOrder() {
        Vector<OrderList> vector = new Vector<>();
        String query = "SELECT \n"
                + "    o.id AS order_id,\n"
                + "    ci.fullName AS full_name,\n"
                + "    u.email,\n"
                + "    ci.phone,\n"
                + "    ci.Address AS address,\n"
                + "    o.date AS order_date,\n"
                + "    (SELECT SUM(od.quantity * od.price) FROM OrderDetail od WHERE od.orderID = o.id) AS total,\n"
                + "    os.name AS status\n"
                + "FROM \n"
                + "    [dbo].[Order] o\n"
                + "JOIN \n"
                + "    [dbo].[ContactInfo] ci ON o.contactInfoID = ci.id\n"
                + "JOIN \n"
                + "    [dbo].[User] u ON o.userID = u.id\n"
                + "JOIN \n"
                + "    [dbo].[OrderStatus] os ON o.statusID = os.id;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new OrderList(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        rs.getDouble(7),
                        rs.getString(8)));
            }
        } catch (Exception e) {
        }
        return vector;
    }

    public Vector<OrderList> getOrderByFilter(String start, String end, String status) {
        Vector<OrderList> vector = new Vector<>();
        String query = "SELECT \n"
                + "    o.id AS order_id,\n"
                + "    ci.fullName AS full_name,\n"
                + "    u.email,\n"
                + "    ci.phone,\n"
                + "    ci.Address AS address,\n"
                + "    o.date AS order_date,\n"
                + "    (SELECT SUM(od.quantity * od.price) FROM OrderDetail od WHERE od.orderID = o.id) AS total,\n"
                + "    os.name AS status\n"
                + "FROM \n"
                + "    [dbo].[Order] o\n"
                + "JOIN \n"
                + "    [dbo].[ContactInfo] ci ON o.contactInfoID = ci.id\n"
                + "JOIN \n"
                + "    [dbo].[User] u ON o.userID = u.id\n"
                + "JOIN \n"
                + "    [dbo].[OrderStatus] os ON o.statusID = os.id\n"
                + "WHERE 1=1 "; // Chèn điều kiện mặc định để đảm bảo rằng WHERE luôn đúng

        // Thêm điều kiện cho ngày nếu có dữ liệu
        if (start != null && !start.isEmpty() && end != null && !end.isEmpty()) {
            query += "AND o.date BETWEEN ? AND ? ";
        }

        // Thêm điều kiện cho status nếu có dữ liệu
        if (status != null && !status.isEmpty()) {
            query += "AND os.name = ? ";
        }

        try {
            PreparedStatement st = connection.prepareStatement(query);

            // Set parameters for date range and status if applicable
            int paramIndex = 1;
            if (start != null && !start.isEmpty() && end != null && !end.isEmpty()) {
                st.setString(paramIndex++, start);
                st.setString(paramIndex++, end);
            }

            if (status != null && !status.isEmpty()) {
                st.setString(paramIndex, status);
            }

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new OrderList(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        rs.getDouble(7),
                        rs.getString(8)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vector;
    }

    public OrderList getOrderByID(String id) {
        String query = "SELECT \n"
                + "    o.id AS order_id,\n"
                + "    ci.fullName AS full_name,\n"
                + "    u.email,\n"
                + "    ci.phone,\n"
                + "    ci.Address AS address,\n"
                + "    o.date AS order_date,\n"
                + "    (SELECT SUM(od.quantity * od.price) FROM OrderDetail od WHERE od.orderID = o.id) AS total,\n"
                + "    os.name AS status\n"
                + "FROM \n"
                + "    [dbo].[Order] o\n"
                + "JOIN \n"
                + "    [dbo].[ContactInfo] ci ON o.contactInfoID = ci.id\n"
                + "JOIN \n"
                + "    [dbo].[User] u ON o.userID = u.id\n"
                + "JOIN \n"
                + "    [dbo].[OrderStatus] os ON o.statusID = os.id\n"
                + "WHERE o.id =?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new OrderList(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        rs.getDouble(7),
                        rs.getString(8));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Vector<OrderDetail> getDetailByOID(String oid) {
        Vector<OrderDetail> vector = new Vector<>();
        String query = "SELECT \n"
                + "   p.id, p.image,\n"
                + "    p.name AS productName,\n"
                + "    c.name AS categoryName,\n"
                + "    od.price,\n"
                + "    od.quantity,\n"
                + "    (od.price * od.quantity) AS amount\n"
                + "FROM \n"
                + "    OrderDetail od\n"
                + "JOIN \n"
                + "    Product p ON od.productID = p.id\n"
                + "JOIN \n"
                + "    Category c ON p.categoryID = c.id\n"
                + "where od.orderID =?";
        try {
            PreparedStatement st = connection.prepareStatement(query);

            st.setString(1, oid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new OrderDetail(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getInt(6), rs.getDouble(7)));
            }
        } catch (SQLException e) {
        }
        return vector;
    }

    public void UpdateStatus(String status, String oid) {
        String query = "UPDATE [dbo].[Order]\n"
                + "   SET \n"
                + "      [statusID] = ?\n"
                + " WHERE [id] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, status);
            st.setString(2, oid);

            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public Vector<OrderChart> get7OrderChartByDay() {
        Vector<OrderChart> vector = new Vector<>();
        String query = "SELECT\n"
                + "    [order_date],\n"
                + "    [done_order],\n"
                + "    [total_order],\n"
                + "    [success_rate],\n"
                + "    [revenue]\n"
                + "FROM (\n"
                + "    SELECT\n"
                + "        DATEADD(DAY, DATEDIFF(DAY, 0, [O].[date]), 0) AS [order_date],\n"
                + "        SUM(CASE WHEN [O].[statusID] = 3 THEN 1 ELSE 0 END) AS [done_order],\n"
                + "        COUNT([O].[id]) AS [total_order],\n"
                + "        CONVERT(decimal(5,2), (SUM(CASE WHEN [O].[statusID] = 3 THEN 1 ELSE 0 END) * 100.0) / NULLIF(COUNT([O].[id]), 0)) AS [success_rate],\n"
                + "        SUM(CASE WHEN [O].[statusID] = 3 THEN [OD].[quantity] * [OD].[price] ELSE 0 END) AS [revenue]\n"
                + "    FROM\n"
                + "        [dbo].[Order] AS [O]\n"
                + "    INNER JOIN\n"
                + "        [dbo].[OrderDetail] AS [OD] ON [O].[id] = [OD].[orderID]\n"
                + "    WHERE\n"
                + "        [O].[statusID] IN (1, 2, 3) -- Wait, Process, Done\n"
                + "        AND [O].[date] >= DATEADD(DAY, -8, GETDATE()) -- 7 days ago from current date\n"
                + "    GROUP BY\n"
                + "        DATEADD(DAY, DATEDIFF(DAY, 0, [O].[date]), 0)\n"
                + ") AS subquery\n"
                + "ORDER BY\n"
                + "    [order_date] DESC;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new OrderChart(rs.getDate("order_date"), rs.getInt("done_order"), rs.getInt("total_order"), rs.getDouble("success_rate"), rs.getDouble("revenue")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vector;
    }

    public Vector<OrderChart> get7OrderChartByMonth() {
        Vector<OrderChart> vector = new Vector<>();
        String query = "	SELECT\n"
                + "    [order_date],\n"
                + "    [done_order],\n"
                + "    [total_order],\n"
                + "    [success_rate],\n"
                + "    [revenue]\n"
                + "FROM (\n"
                + "    SELECT\n"
                + "        DATEADD(WEEK, DATEDIFF(WEEK, 0, [O].[date]), 0) AS [order_date],\n"
                + "        SUM(CASE WHEN [O].[statusID] = 3 THEN 1 ELSE 0 END) AS [done_order],\n"
                + "        COUNT([O].[id]) AS [total_order],\n"
                + "        CONVERT(decimal(5,2), (SUM(CASE WHEN [O].[statusID] = 3 THEN 1 ELSE 0 END) * 100.0) / NULLIF(COUNT([O].[id]), 0)) AS [success_rate],\n"
                + "        SUM(CASE WHEN [O].[statusID] = 3 THEN [OD].[quantity] * [OD].[price] ELSE 0 END) AS [revenue]\n"
                + "    FROM\n"
                + "        [dbo].[Order] AS [O]\n"
                + "    INNER JOIN\n"
                + "        [dbo].[OrderDetail] AS [OD] ON [O].[id] = [OD].[orderID]\n"
                + "    WHERE\n"
                + "        [O].[statusID] IN (1, 2, 3) -- Wait, Process, Done\n"
                + "        AND [O].[date] >= DATEADD(WEEK, -8, GETDATE()) -- 7 weeks ago from current date\n"
                + "    GROUP BY\n"
                + "        DATEADD(WEEK, DATEDIFF(WEEK, 0, [O].[date]), 0)\n"
                + ") AS subquery\n"
                + "ORDER BY\n"
                + "    [order_date] DESC;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new OrderChart(rs.getDate("order_date"), rs.getInt("done_order"), rs.getInt("total_order"), rs.getDouble("success_rate"), rs.getDouble("revenue")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vector;
    }

    public static void main(String[] args) {
        OrderDAO dao = new OrderDAO();
//        dao.UpdateStatus("1", "1");
//        OrderList order = dao.getOrderByID("1");
//        System.out.println(order);
        //Vector<OrderDetail> list = dao.getDetailByOID("1");
        Vector<OrderChart> list = dao.get7OrderChartByMonth();
        //Vector<OrderList> list = dao.getAllOrder();
        for (OrderChart list1 : list) {
            System.out.println(list1);
        }
    }
}
