/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Cart;
import entity.Item;
import entity.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;

/**
 *
 * @author Phuc Lam
 */
public class CartDao extends DBContext {

    public void addOrder(User u, Cart cart) {
         LocalDateTime curDate = LocalDateTime.now();
        String date = curDate.toString();
        try {
            String sql = "INSERT INTO [dbo].[Order] ([userID], [contactInfoID], "
                    + "[date], [note], [statusID])\n"
                    + "VALUES     (?, ?, ?, ?, ?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, u.getId());
            st.setInt(2, u.getId());
            st.setString(3, date);
            st.setString(4, "Note for Order 1");
            st.setInt(5, 1);
            st.executeUpdate();
            
            String sql1 = "Select top 1 id from [Order] order by id DESC";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            ResultSet rs = st1.executeQuery();
            if (rs.next()) {
                int oid = rs.getInt("id");
                for (Item i : cart.getItems()) {
                    String sql2 = "INSERT INTO [dbo].[OrderDetail] ([orderID], [productID], [quantity], [price])\n"
                            + "VALUES  (?, ?, ?, ?)";
                    PreparedStatement st2 = connection.prepareStatement(sql2);
                    st2.setInt(1, oid);
                    st2.setInt(2, i.getProduct().getId());
                    st2.setDouble(3, i.getQuantity());
                    st2.setDouble(4, i.getPrice());
                    st2.executeUpdate();
                }
            }
        } catch (Exception e) {

        }
    }

}
