/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Acer
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import entity.Slider;
import java.util.ArrayList;
import java.util.List;

public class SliderDAO extends DBContext {

    //Hoang Anh
    //Retrieves all sliders from a database with a status of 1 (true) and returns them as a list of Slider objects.
    public List<Slider> getSlidersByStatus() {
        String sql = "SELECT [id], [name], [description], [url], [image], "
                + "[status] FROM [dbo].[Slider] WHERE [status] = 1";
        List<Slider> sliders = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                sliders.add(new Slider(rs.getInt("id"), rs.getString("name"),
                        rs.getString("description"), rs.getString("url"),
                        rs.getString("image"), rs.getBoolean("status")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sliders;
    }

    public List<Slider> filterByStatus(boolean status) {
        String sql = "SELECT [id], [name], [description], [url], [image], "
                + "[status] FROM [dbo].[Slider] WHERE [status] = ?";
        List<Slider> sliders = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                sliders.add(new Slider(rs.getInt("id"), rs.getString("name"),
                        rs.getString("description"), rs.getString("url"),
                        rs.getString("image"), rs.getBoolean("status")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sliders;
    }

    public List<Slider> getAll() {
        String sql = "SELECT [id], [name], [description], [url], [image], "
                + "[status] FROM [dbo].[Slider]";
        List<Slider> sliders = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                sliders.add(new Slider(rs.getInt("id"), rs.getString("name"),
                        rs.getString("description"), rs.getString("url"),
                        rs.getString("image"), rs.getBoolean("status")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sliders;
    }

    public Slider getSliderByID(int id) {
        String sql = "SELECT [id], [name], [description], [url], [image], "
                + "[status] FROM [dbo].[Slider] WHERE [id] = ?";
        Slider slider = null;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                slider = new Slider(rs.getInt("id"), rs.getString("name"),
                        rs.getString("description"), rs.getString("url"),
                        rs.getString("image"), rs.getBoolean("status"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return slider;
    }

    public void insertSlider(String name, String description, String url, String image, String status) {
        String query = "INSERT INTO [dbo].[Slider]\n"
                + "           ([name]\n"
                + "           ,[description]\n"
                + "           ,[url]\n"
                + "           ,[image]\n"
                + "           ,[status])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, name);
            st.setString(2, description);
            st.setString(3, url);
            st.setString(4, image);
            st.setString(5, status);
            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Slider> SortByStatus() {
        String sql = "SELECT [id], [name], [description], [url], [image], "
                + "[status] FROM [dbo].[Slider] ORDER BY [status]";
        List<Slider> sliders = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                sliders.add(new Slider(rs.getInt("id"), rs.getString("name"),
                        rs.getString("description"), rs.getString("url"),
                        rs.getString("image"), rs.getBoolean("status")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sliders;
    }

    public List<Slider> sortByName() {
        String sql = "SELECT [id], [name], [description], [url], [image], "
                + "[status] FROM [dbo].[Slider] ORDER BY [name]";
        List<Slider> sliders = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                sliders.add(new Slider(rs.getInt("id"), rs.getString("name"),
                        rs.getString("description"), rs.getString("url"),
                        rs.getString("image"), rs.getBoolean("status")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sliders;
    }

    public List<Slider> searchByName(String name) {
        String sql = "SELECT [id], [name], [description], [url], [image], "
                + "[status] FROM [dbo].[Slider] WHERE [name] LIKE ?";
        List<Slider> sliders = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + name + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                sliders.add(new Slider(rs.getInt("id"), rs.getString("name"),
                        rs.getString("description"), rs.getString("url"),
                        rs.getString("image"), rs.getBoolean("status")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sliders;
    }

    public void updateSlider(String name, String description, String url,
            String image, String status, int id) {
        String sql = "UPDATE [dbo].[Slider]\n"
                + "   SET [name] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[url] = ?\n"
                + "      ,[image] = ?\n"
                + "      ,[status] = ?\n"
                + " WHERE [id] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setString(2, description);
            st.setString(3, url);
            st.setString(4, image);
            st.setString(5, status);
            st.setInt(6, id);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Slider viewSlider(int id) {
        String sql = "SELECT [id], [name], [description], [url], [image], "
                + "[status] FROM [dbo].[Slider] WHERE [id] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Slider(rs.getInt("id"), rs.getString("name"),
                        rs.getString("description"), rs.getString("url"),
                        rs.getString("image"), rs.getBoolean("status"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void SetActive(String id) {
        String query = "UPDATE [dbo].[Slider] "
                + "SET [status] = 1 "
                + "WHERE [id] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void SetInactive(String id) {
        String query = "UPDATE [dbo].[Slider] "
                + "SET [status] = 0 "
                + "WHERE [id] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Slider getLastByID() {
        String sql = "SELECT TOP 1 [id], [name], [description], [url], [image], "
                + "[status] FROM [dbo].[Slider] ORDER BY [id] DESC";
        Slider slider = null;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                slider = new Slider(rs.getInt("id"), rs.getString("name"),
                        rs.getString("description"), rs.getString("url"),
                        rs.getString("image"), rs.getBoolean("status"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return slider;
    }
    
    public void deleteByID(int id) {
        String sql = "DELETE FROM [dbo].[Slider] WHERE [id] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Slider> getListByPage(List<Slider> list, int start, int end) {
        List<Slider> arr = new ArrayList<>();
        for (int i = start; i < end && i < list.size(); i++) {
            arr.add(list.get(i));
        }
        return arr;
    }
    

    public static void main(String[] args) {
        SliderDAO sliderDAO = new SliderDAO();
        String name = "testSlider"; // replace with the name you want to insert
        String description = "testDescription"; // replace with the description you want to insert
        String url = "/shop"; // replace with the url you want to insert
        String image = "testImage"; // replace with the image you want to insert
        String status = "1"; // replace with the status you want to insert

        sliderDAO.insertSlider(name, description, url, image, status);

        System.out.println("Slider inserted successfully!");
    }
}
