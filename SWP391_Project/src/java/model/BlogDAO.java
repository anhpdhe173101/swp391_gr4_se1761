/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Acer
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import entity.Blog;
import entity.PostChart;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

//Hoang Anh
public class BlogDAO extends DBContext {

    //Method used to get all information from Blog
    public List<Blog> getAll() {
        String sql = "SELECT * FROM Blog WHERE [status] = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Blog> list = new ArrayList<>();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryID(rs.getInt("categoryID"));
                b.setAuthorID(rs.getInt("authorID"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setContent(rs.getString("content"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to get all information from Blog List for Marketing
    public List<Blog> getForList() {
        String sql = "SELECT \n"
                + "    Blog.id, \n"
                + "    Blog.image,\n"
                + "    Blog.title,\n"
                + "    Blog.opening,\n"
                + "    Blog.date_posted,\n"
                + "    Blog.Status,\n"
                + "    COUNT(Comment.CommentID) AS CommentCount, \n"
                + "    [User].fullName AS AuthorName, \n"
                + "    BlogCategory.categoryName AS CategoryName\n"
                + "FROM \n"
                + "    Blog\n"
                + "LEFT JOIN \n"
                + "    Comment ON Blog.id = Comment.BlogID\n"
                + "INNER JOIN \n"
                + "    [User] ON Blog.authorID = [User].id\n"
                + "INNER JOIN \n"
                + "    BlogCategory ON Blog.categoryID = BlogCategory.categoryID\n"
                + "GROUP BY \n"
                + "    Blog.id, \n"
                + "    Blog.image,\n"
                + "    Blog.title,\n"
                + "    Blog.opening,\n"
                + "    Blog.date_posted,\n"
                + "    Blog.Status,\n"
                + "    [User].fullName, \n"
                + "    BlogCategory.categoryName;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Blog> list = new ArrayList<>();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryName(rs.getString("categoryName"));
                b.setAuthorName(rs.getString("authorName"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to add new blog
    public void addNewBlog(int categoryID, int authorID, String image, String title, String opening, String content) {
        String sql = "INSERT INTO Blog "
                + "           (categoryID, authorID, image, title, opening, content) "
                + "     VALUES "
                + "           (?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, categoryID);
            st.setInt(2, authorID);
            st.setString(3, image);
            st.setString(4, title);
            st.setString(5, opening);
            st.setString(6, content);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Method used to delete blog by its ID
    public void deleteByID(int id) {
        String sql = "DELETE FROM Blog WHERE id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Method used to take blog ID only
    public List<Integer> getBlogID() {
        String sql = "SELECT id FROM Blog";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Integer> list = new ArrayList<>();
            while (rs.next()) {
                list.add(rs.getInt("id"));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to get top 3 blog by posted date
    public List<Blog> getTop3Newest() {
        String sql = "SELECT TOP 3 * FROM Blog WHERE status = 1 ORDER BY date_posted DESC, id DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Blog> list = new ArrayList<>();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryID(rs.getInt("categoryID"));
                b.setAuthorID(rs.getInt("authorID"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setContent(rs.getString("content"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to get blog information by its ID
    public Blog getBlogByID(int id) {
        String sql = "SELECT \n"
                + "    Blog.id, \n"
                + "    Blog.image,\n"
                + "    Blog.title,\n"
                + "    Blog.opening,\n"
                + "    Blog.date_posted,\n"
                + "    Blog.Status,\n"
                + "    COUNT(Comment.CommentID) AS CommentCount, \n"
                + "    [User].fullName AS AuthorName, \n"
                + "    BlogCategory.categoryName AS CategoryName\n"
                + "FROM \n"
                + "    Blog\n"
                + "LEFT JOIN \n"
                + "    Comment ON Blog.id = Comment.BlogID\n"
                + "INNER JOIN \n"
                + "    [User] ON Blog.authorID = [User].id\n"
                + "INNER JOIN \n"
                + "    BlogCategory ON Blog.categoryID = BlogCategory.categoryID\n"
                + "WHERE \n"
                + "    Blog.Status = ?\n"
                + "GROUP BY \n"
                + "    Blog.id, \n"
                + "    Blog.image,\n"
                + "    Blog.title,\n"
                + "    Blog.opening,\n"
                + "    Blog.date_posted,\n"
                + "    Blog.Status,\n"
                + "    [User].fullName, \n"
                + "    BlogCategory.categoryName";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryName(rs.getString("categoryName"));
                b.setAuthorName(rs.getString("authorName"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setStatus(rs.getBoolean("status"));
                return b;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to get blog information by its ID
        public Blog getBlogByID_2(int id) {
        String sql = "SELECT Blog.*, [User].fullName as authorName FROM Blog "
                + "INNER JOIN [User] ON Blog.authorID = [User].id WHERE Blog.id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryID(rs.getInt("categoryID"));
                b.setAuthorID(rs.getInt("authorID"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setContent(rs.getString("content"));
                b.setStatus(rs.getBoolean("status"));
                b.setAuthorName(rs.getString("authorName")); // Add this line
                return b;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to get all blog information tha belong to a categoryID
    public List<Blog> getBlogByCategoryID(int categoryID) {
        String sql = "SELECT * FROM Blog WHERE categoryID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, categoryID);
            ResultSet rs = st.executeQuery();
            List<Blog> list = new ArrayList<>();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryID(rs.getInt("categoryID"));
                b.setAuthorID(rs.getInt("authorID"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setContent(rs.getString("content"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to get blog by its title
    public List<Blog> searchByTitle(String key) {
        String sql = "SELECT * FROM Blog WHERE title LIKE ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + key + "%");
            ResultSet rs = st.executeQuery();
            List<Blog> list = new ArrayList<>();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryID(rs.getInt("categoryID"));
                b.setAuthorID(rs.getInt("authorID"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setContent(rs.getString("content"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to get author name for each blog
    public List<Blog> getAuthorByID(int id) {
        String sql = "SELECT Blog.id, User.fullName AS authorName FROM Blog "
                + "INNER JOIN [User] ON Blog.authorID = [User].id WHERE Blog.authorID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            List<Blog> list = new ArrayList<>();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryID(rs.getInt("categoryID"));
                b.setAuthorID(rs.getInt("authorID"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setContent(rs.getString("content"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to sort a list of blogs in ascending order based on their titles. 
    public List<Blog> orderA_Z() {
        List<Blog> list = getAll();
        list.sort((Blog b1, Blog b2) -> b1.getTitle().compareTo(b2.getTitle()));
        return list;
    }

    //Method used to sort a list of blogs in descending order based on their titles. 
    public List<Blog> orderZ_A() {
        List<Blog> list = getAll();
        list.sort((Blog b1, Blog b2) -> b2.getTitle().compareTo(b1.getTitle()));
        return list;
    }

    //Method used to sort a list of blogs in descending order based on their total comment. 
    public List<Blog> orderByComments() {
        String sql = "SELECT Blog.*, COUNT(Comment.BlogID) as CommentCount "
                + "FROM Blog LEFT JOIN Comment "
                + "ON Blog.id = Comment.BlogID "
                + "GROUP BY Blog.id "
                + "ORDER BY CommentCount DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Blog> list = new ArrayList<>();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryID(rs.getInt("categoryID"));
                b.setAuthorID(rs.getInt("authorID"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setContent(rs.getString("content"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to paging blog page
    public List<Blog> getListByPage(List<Blog> list, int start, int end) {
        List<Blog> arr = new ArrayList<>();
        for (int i = start; i < end && i < list.size(); i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    //Method used to set a blog's status to active
    public void setActive(String id) {
        String query = "UPDATE Blog "
                + "SET status = 1 "
                + "WHERE id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Method used to set a blog's status to inactive
    public void setInactive(String id) {
        String query = "UPDATE Blog "
                + "SET status = 0 "
                + "WHERE id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Blog> sortByTitle() {
        String sql = "SELECT * FROM Blog ORDER BY title";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Blog> list = new ArrayList<>();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryID(rs.getInt("categoryID"));
                b.setAuthorID(rs.getInt("authorID"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setContent(rs.getString("content"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Blog> sortByDate() {
        String sql = "SELECT * FROM Blog ORDER BY date_posted";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Blog> list = new ArrayList<>();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryID(rs.getInt("categoryID"));
                b.setAuthorID(rs.getInt("authorID"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setContent(rs.getString("content"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Blog> sortByStatus() {
        String sql = "SELECT * FROM Blog ORDER BY status";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Blog> list = new ArrayList<>();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryID(rs.getInt("categoryID"));
                b.setAuthorID(rs.getInt("authorID"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setContent(rs.getString("content"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method used to get all information from Blog List for Marketing (by its status)
    public List<Blog> filterByStatus(boolean status) {
        String sql = "SELECT \n"
                + "    Blog.id, \n"
                + "    Blog.image,\n"
                + "    Blog.title,\n"
                + "    Blog.opening,\n"
                + "    Blog.date_posted,\n"
                + "    Blog.Status,\n"
                + "    COUNT(Comment.CommentID) AS CommentCount, \n"
                + "    [User].fullName AS AuthorName, \n"
                + "    BlogCategory.categoryName AS CategoryName\n"
                + "FROM \n"
                + "    Blog\n"
                + "LEFT JOIN \n"
                + "    Comment ON Blog.id = Comment.BlogID\n"
                + "INNER JOIN \n"
                + "    [User] ON Blog.authorID = [User].id\n"
                + "INNER JOIN \n"
                + "    BlogCategory ON Blog.categoryID = BlogCategory.categoryID\n"
                + "WHERE \n"
                + "    Blog.Status = ?\n"
                + "GROUP BY \n"
                + "    Blog.id, \n"
                + "    Blog.image,\n"
                + "    Blog.title,\n"
                + "    Blog.opening,\n"
                + "    Blog.date_posted,\n"
                + "    Blog.Status,\n"
                + "    [User].fullName, \n"
                + "    BlogCategory.categoryName";
        List<Blog> blogs = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setCategoryName(rs.getString("categoryName"));
                b.setAuthorName(rs.getString("authorName"));
                b.setImage(rs.getString("image"));
                b.setTitle(rs.getString("title"));
                b.setOpening(rs.getString("opening"));
                b.setDatePosted(rs.getDate("date_posted"));
                b.setStatus(rs.getBoolean("status"));
                blogs.add(b);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return blogs;
    }

    public Vector<PostChart> get7PostChartByDay() {
        Vector<PostChart> vector = new Vector<>();
        String query = "SELECT \n"
                + "    date_posted AS [Date],\n"
                + "    COUNT(*) AS PostCount\n"
                + "FROM \n"
                + "    Blog\n"
                + "WHERE\n"
                + "    date_posted >= DATEADD(DAY, -7, GETDATE())\n"
                + "GROUP BY \n"
                + "    date_posted\n"
                + "ORDER BY \n"
                + "    date_posted ;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new PostChart(rs.getDate(1), rs.getInt(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vector;
    }

    public Vector<PostChart> get7PostChartByWeek() {
        Vector<PostChart> vector = new Vector<>();
        String query = "SELECT \n"
                + "    DATEADD(WEEK, DATEDIFF(WEEK, 0, date_posted), 0) AS WeekStart,\n"
                + "    COUNT(*) AS PostCount\n"
                + "FROM \n"
                + "    Blog\n"
                + "WHERE\n"
                + "    date_posted >= DATEADD(WEEK, -7, GETDATE())\n"
                + "GROUP BY \n"
                + "    DATEADD(WEEK, DATEDIFF(WEEK, 0, date_posted), 0)\n"
                + "ORDER BY \n"
                + "    WeekStart DESC;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new PostChart(rs.getDate(1), rs.getInt(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vector;
    }
    
    public static void main(String[] args) {
        BlogDAO blogDAO = new BlogDAO();
        List<Blog> blogs = blogDAO.getTop3Newest();
        for (Blog blog : blogs) {
            System.out.println("Blog ID: " + blog.getId());
            System.out.println("Blog Image: " + blog.getImage());
            System.out.println("Blog Title: " + blog.getTitle());
            System.out.println("Blog Opening: " + blog.getOpening());
            System.out.println("Category: " + blog.getCategoryName());
            System.out.println("Author: " + blog.getAuthorName());
            System.out.println("Blog Opening: " + blog.getOpening());
            System.out.println("Date Posted: " + blog.getDatePosted());
            System.out.println("Status: " + blog.isStatus());
            System.out.println("-------------------------");
        }

    }

}
