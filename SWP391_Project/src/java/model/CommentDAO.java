/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Acer
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import entity.Comment;

/**
 *
 * @author Acer
 */
public class CommentDAO extends DBContext {

    public List<Comment> getCommentsForBlog(int blogId) {
        String sql = "SELECT Comment.*, [User].fullName as postUserName \n"
                + "FROM Comment \n"
                + "INNER JOIN [User] ON Comment.PostUserID = [User].id \n"
                + "WHERE Comment.BlogID = ?";
    try {
        PreparedStatement st = connection.prepareStatement(sql);
        st.setInt(1, blogId);
        ResultSet rs = st.executeQuery();
        List<Comment> list = new ArrayList<>();
        while (rs.next()) {
            Comment c = new Comment();
            c.setCommentID(rs.getInt("CommentID"));
            c.setPostUserID(rs.getInt("PostUserID"));
            c.setBlogID(rs.getInt("BlogID"));
            c.setCommentContent(rs.getString("CommentContent"));
            c.setCommentDate(rs.getDate("CommentDate"));
            c.setPostUserName(rs.getString("postUserName")); // Assuming you have a setter for this
            list.add(c);
        }
        return list;
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return null;
}


    public void addNewComment(int postUserID, int BlogID, String commentContent) {
        String sql = "INSERT INTO Comment(PostUserID, BlogID, CommentContent) VALUES (?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, postUserID);
            st.setInt(2, BlogID);
            st.setString(3, commentContent);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void DeleteComment(int userID, int blogID, String content) {
        String sql = "DELETE FROM Comment WHERE PostUserID = ? AND BlogID = ? AND CommentContent = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            st.setInt(2, blogID);
            st.setString(3, content);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public int countCommentByBlogID(int blogID) {
        String sql = "SELECT COUNT(*) "
                + "FROM Comment "
                + "WHERE BlogID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, blogID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void main(String[] args) {
    CommentDAO commentDAO = new CommentDAO();

    // Set the properties of the test comment
    int postUserID = 1;
    int blogID = 1;
    String commentContent = "This is a test comment.";

    // Add the new comment
    commentDAO.addNewComment(postUserID, blogID, commentContent);

    // Fetch the comments for the blog to check if the new comment was added
    List<Comment> comments = commentDAO.getCommentsForBlog(blogID);
    if (comments != null) {
        for (Comment comment : comments) {
            System.out.println("Comment Date: " + comment.getCommentDate());
            System.out.println("Post User ID: " + comment.getPostUserID());
            System.out.println("Blog ID: " + comment.getBlogID());
            System.out.println("Comment Content: " + comment.getCommentContent());
            System.out.println("-------------------------");
        }
    } else {
        System.out.println("No comments found for blog ID: " + blogID);
    }
}


}
