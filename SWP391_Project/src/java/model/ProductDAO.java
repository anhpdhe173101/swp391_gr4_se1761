/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Category;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import entity.Product;
import entity.ProductChart;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

//Hoang Anh
//Retrieves the top 4 most recent products from a database and returns them as a list of Product objects.
public class ProductDAO extends DBContext {

    //DuyAnh
    // get data for all product list
    public Vector<Product> getAllProduct() {
        Vector<Product> vector = new Vector<>();
        String query = "WITH ProductInfo AS (\n"
                + "    SELECT\n"
                + "        P.id AS ProductID,\n"
                + "        P.name AS ProductName,\n"
                + "        P.categoryID,\n"
                + "        ISNULL(SP.price, P.price) AS SalePrice,\n"
                + "        P.price AS OriginPrice,\n"
                + "        P.description,\n"
                + "        P.image,\n"
                + "        (\n"
                + "            SELECT ISNULL(SUM(ps.quantity), 0)\n"
                + "            FROM ProductStock ps\n"
                + "            WHERE ps.productID = P.id\n"
                + "        ) AS TotalStockQuantity,\n"
                + "        (\n"
                + "            SELECT ISNULL(SUM(od.quantity), 0)\n"
                + "            FROM OrderDetail od\n"
                + "            WHERE od.productID = P.id\n"
                + "        ) AS TotalOrderQuantity,\n"
                + "        P.status,\n"
                + "        P.season\n"
                + "    FROM\n"
                + "        Product P\n"
                + "    LEFT JOIN SalePrice SP ON P.id = SP.productID\n"
                + "        AND GETDATE() BETWEEN SP.startDate AND SP.endDate\n"
                + "    -- Thay thế 3 bằng ID của sản phẩm bạn muốn tính toán\n"
                + ")\n"
                + "SELECT\n"
                + "    ProductID,\n"
                + "    ProductName,\n"
                + "    categoryID,\n"
                + "    OriginPrice,\n"
                + "    SalePrice,\n"
                + "    description,\n"
                + "    image,\n"
                + "    TotalStockQuantity - TotalOrderQuantity AS Quantity,\n"
                + "    status,\n"
                + "    season\n"
                + "FROM\n"
                + "    ProductInfo  \n"
                + "	ORDER BY\n"
                + "    CASE\n"
                + "         WHEN season = 'spring' AND MONTH(GETDATE()) BETWEEN 1 AND 3 THEN 1\n"
                + "        WHEN season = 'summer' AND MONTH(GETDATE()) BETWEEN 4 AND 6 THEN 1\n"
                + "        WHEN season = 'autumn' AND MONTH(GETDATE()) BETWEEN 7 AND 9 THEN 1\n"
                + "        WHEN season = 'winter' AND MONTH(GETDATE()) NOT BETWEEN 1 AND 9 THEN 1\n"
                + "        ELSE 2\n"
                + "    END,\n"
                + "    season;";// sale price = origin price if not have
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return vector;
    }
    
    
        public Vector<Product> getAllProductForGuest() {
        Vector<Product> vector = new Vector<>();
        String query = "WITH ProductInfo AS (\n"
                + "    SELECT\n"
                + "        P.id AS ProductID,\n"
                + "        P.name AS ProductName,\n"
                + "        P.categoryID,\n"
                + "        ISNULL(SP.price, P.price) AS SalePrice,\n"
                + "        P.price AS OriginPrice,\n"
                + "        P.description,\n"
                + "        P.image,\n"
                + "        (\n"
                + "            SELECT ISNULL(SUM(ps.quantity), 0)\n"
                + "            FROM ProductStock ps\n"
                + "            WHERE ps.productID = P.id\n"
                + "        ) AS TotalStockQuantity,\n"
                + "        (\n"
                + "            SELECT ISNULL(SUM(od.quantity), 0)\n"
                + "            FROM OrderDetail od\n"
                + "            WHERE od.productID = P.id\n"
                + "        ) AS TotalOrderQuantity,\n"
                + "        P.status,\n"
                + "        P.season\n"
                + "    FROM\n"
                + "        Product P\n"
                + "    LEFT JOIN SalePrice SP ON P.id = SP.productID\n"
                + "        AND GETDATE() BETWEEN SP.startDate AND SP.endDate\n"
                + "    -- Thay thế 3 bằng ID của sản phẩm bạn muốn tính toán\n"
                + ")\n"
                + "SELECT\n"
                + "    ProductID,\n"
                + "    ProductName,\n"
                + "    categoryID,\n"
                + "    OriginPrice,\n"
                + "    SalePrice,\n"
                + "    description,\n"
                + "    image,\n"
                + "    TotalStockQuantity - TotalOrderQuantity AS Quantity,\n"
                + "    status,\n"
                + "    season\n"
                + "FROM\n"
                + "    ProductInfo where status =1 \n"
                + "	ORDER BY\n"
                + "    CASE\n"
                + "         WHEN season = 'spring' AND MONTH(GETDATE()) BETWEEN 1 AND 3 THEN 1\n"
                + "        WHEN season = 'summer' AND MONTH(GETDATE()) BETWEEN 4 AND 6 THEN 1\n"
                + "        WHEN season = 'autumn' AND MONTH(GETDATE()) BETWEEN 7 AND 9 THEN 1\n"
                + "        WHEN season = 'winter' AND MONTH(GETDATE()) NOT BETWEEN 1 AND 9 THEN 1\n"
                + "        ELSE 2\n"
                + "    END,\n"
                + "    season;";// sale price = origin price if not have
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return vector;
    }

    //DuyAnh
    //Sort by Name
    public Vector<Product> SortByName() {
        Vector<Product> vector = new Vector<>();
        String query = "SELECT p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	order by p.name;";// sale price = origin price if not have
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3), rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getDate(9),
                        rs.getInt(10)));
            }
        } catch (Exception e) {
        }
        return vector;
    }

    //DuyAnh
    //Sort By Category
    public Vector<Product> SortByCategory() {
        Vector<Product> vector = new Vector<>();
        String query = "SELECT p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	order by p.categoryID;";// sale price = origin price if not have
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3), rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getDate(9),
                        rs.getInt(10)));
            }
        } catch (Exception e) {
        }
        return vector;
    }

    //Duy Anh
    //Sort By Origin Price
    public Vector<Product> SortByOriginPrice() {
        Vector<Product> vector = new Vector<>();
        String query = "SELECT p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	order by p.price;";// sale price = origin price if not have
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3), rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getDate(9),
                        rs.getInt(10)));
            }
        } catch (Exception e) {
        }
        return vector;
    }

    //Duy Anh
    //Sort By Sale Price
    public Vector<Product> SortBySalePrice() {
        Vector<Product> vector = new Vector<>();
        String query = "SELECT p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	order by sp.price;";// sale price = origin price if not have
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3), rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getDate(9),
                        rs.getInt(10)));
            }
        } catch (Exception e) {
        }
        return vector;
    }

    //DuyAnh
    //Sort by Status
    public Vector<Product> SortByStatus() {
        Vector<Product> vector = new Vector<>();
        String query = "SELECT p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	order by p.status;";// sale price = origin price if not have
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3), rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getDate(9),
                        rs.getInt(10)));
            }
        } catch (Exception e) {
        }
        return vector;
    }

    //DuyAnh
    //get data for category
    public Vector<Category> getAllCategory() {
        Vector<Category> vector = new Vector<>();
        String query = "select * from Category";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new Category(rs.getInt(1), rs.getString(2), rs.getString(3)));
            }
        } catch (Exception e) {
        }
        return vector;
    }

    //DuyAnh
    //search product by category
    public Vector<Product> getProductByCID(String cid) {
        Vector<Product> vector = new Vector<>();
        String query = "WITH ProductInfo AS (\n"
                + "    SELECT\n"
                + "        P.id AS ProductID,\n"
                + "        P.name AS ProductName,\n"
                + "        P.categoryID,\n"
                + "        ISNULL(SP.price, P.price) AS SalePrice,\n"
                + "        P.price AS OriginPrice,\n"
                + "        P.description,\n"
                + "        P.image,\n"
                + "        (\n"
                + "            SELECT ISNULL(SUM(ps.quantity), 0)\n"
                + "            FROM ProductStock ps\n"
                + "            WHERE ps.productID = P.id\n"
                + "        ) AS TotalStockQuantity,\n"
                + "        (\n"
                + "            SELECT ISNULL(SUM(od.quantity), 0)\n"
                + "            FROM OrderDetail od\n"
                + "            WHERE od.productID = P.id\n"
                + "        ) AS TotalOrderQuantity,\n"
                + "        P.status,\n"
                + "        P.season\n"
                + "    FROM\n"
                + "        Product P\n"
                + "    LEFT JOIN SalePrice SP ON P.id = SP.productID\n"
                + "        AND GETDATE() BETWEEN SP.startDate AND SP.endDate\n"
                + "    -- Thay thế 3 bằng ID của sản phẩm bạn muốn tính toán\n"
                + ")\n"
                + "SELECT\n"
                + "    ProductID,\n"
                + "    ProductName,\n"
                + "    categoryID,\n"
                + "    OriginPrice,\n"
                + "    SalePrice,\n"
                + "    description,\n"
                + "    image,\n"
                + "    TotalStockQuantity - TotalOrderQuantity AS Quantity,\n"
                + "    status,\n"
                + "    season\n"
                + "FROM\n"
                + "    ProductInfo\n"
                + "	where categoryID = ? and status =  1 \n "
                + "	ORDER BY\n"
                + "    CASE\n"
                + "         WHEN season = 'spring' AND MONTH(GETDATE()) BETWEEN 1 AND 3 THEN 1\n"
                + "        WHEN season = 'summer' AND MONTH(GETDATE()) BETWEEN 4 AND 6 THEN 1\n"
                + "        WHEN season = 'autumn' AND MONTH(GETDATE()) BETWEEN 7 AND 9 THEN 1\n"
                + "        WHEN season = 'winter' AND MONTH(GETDATE()) NOT BETWEEN 1 AND 9 THEN 1\n"
                + "        ELSE 2\n"
                + "    END,\n"
                + "    season;";
        try {
            PreparedStatement st = connection.prepareStatement(query);

            st.setString(1, cid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return vector;
    }

    //DuyAnh
    //show the last product
    public Product getLast() {
        String query = "SELECT top 1 p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	order by p.id desc;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3), rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getDate(9),
                        rs.getInt(10));
            }
        } catch (Exception e) {
        }
        return null;
    }

    //DuyAnh
    //get lastest by id
    public Product getLastByID() {
        String query = "SELECT top 1 p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	order by id desc;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3), rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getDate(9),
                        rs.getInt(10));
            }
        } catch (Exception e) {
        }
        return null;
    }

    //DuyAnh 
    //Search by product name
    public Vector<Product> searchByProductName(String txtSearch) {
        Vector<Product> vector = new Vector<>();
        String query = "WITH ProductInfo AS (\n"
                + "    SELECT\n"
                + "        P.id AS ProductID,\n"
                + "        P.name AS ProductName,\n"
                + "        P.categoryID,\n"
                + "        ISNULL(SP.price, P.price) AS SalePrice,\n"
                + "        P.price AS OriginPrice,\n"
                + "        P.description,\n"
                + "        P.image,\n"
                + "        (\n"
                + "            SELECT ISNULL(SUM(ps.quantity), 0)\n"
                + "            FROM ProductStock ps\n"
                + "            WHERE ps.productID = P.id\n"
                + "        ) AS TotalStockQuantity,\n"
                + "        (\n"
                + "            SELECT ISNULL(SUM(od.quantity), 0)\n"
                + "            FROM OrderDetail od\n"
                + "            WHERE od.productID = P.id\n"
                + "        ) AS TotalOrderQuantity,\n"
                + "        P.status,\n"
                + "        P.season\n"
                + "    FROM\n"
                + "        Product P\n"
                + "    LEFT JOIN SalePrice SP ON P.id = SP.productID\n"
                + "        AND GETDATE() BETWEEN SP.startDate AND SP.endDate\n"
                + "    -- Thay thế 3 bằng ID của sản phẩm bạn muốn tính toán\n"
                + ")\n"
                + "SELECT\n"
                + "    ProductID,\n"
                + "    ProductName,\n"
                + "    categoryID,\n"
                + "    OriginPrice,\n"
                + "    SalePrice,\n"
                + "    description,\n"
                + "    image,\n"
                + "    TotalStockQuantity - TotalOrderQuantity AS Quantity,\n"
                + "    status,\n"
                + "    season\n"
                + "FROM\n"
                + "    ProductInfo\n"
                + "	where ProductName like ?\n"
                + "	ORDER BY\n"
                + "    CASE\n"
                + "         WHEN season = 'spring' AND MONTH(GETDATE()) BETWEEN 1 AND 3 THEN 1\n"
                + "        WHEN season = 'summer' AND MONTH(GETDATE()) BETWEEN 4 AND 6 THEN 1\n"
                + "        WHEN season = 'autumn' AND MONTH(GETDATE()) BETWEEN 7 AND 9 THEN 1\n"
                + "        WHEN season = 'winter' AND MONTH(GETDATE()) NOT BETWEEN 1 AND 9 THEN 1\n"
                + "        ELSE 2\n"
                + "    END,\n"
                + "    season;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, "%" + txtSearch + "%");
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                vector.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return vector;
    }

    public Vector<Product> getListByPage(Vector<Product> list, int start, int end) {
        Vector<Product> vector = new Vector<>();
        for (int i = start; i < end; i++) {
            vector.add(list.get(i));
        }
        return vector;
    }

    // DuyAnh
    //active product
    public void ActiveProduct(String pid) {
        String query = "UPDATE Product\n"
                + "SET status = 1\n"
                + "WHERE id = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, pid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    //DuyAnh
    //Update Sale
    public void UpdateSale(String sale_price, String pid) {
        String query = "UPDATE SalePrice\n"
                + "SET price = ?\n"
                + "WHERE productID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, sale_price);
            st.setString(2, pid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    // DuyAnh
    //active product
    public void InactiveProduct(String pid) {
        String query = "UPDATE Product\n"
                + "SET status = 0\n"
                + "WHERE id = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, pid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    //DuyAnh
    //InsertProduct
    public void insertProduct(String name, String cateID, String origin_price,
            String description, String image, String status, String season) {

        String query = "INSERT INTO [dbo].[Product]\n"
                + "           ([name]\n"
                + "           ,[categoryID]\n"
                + "           ,[price]\n"
                + "           ,[description]\n"
                + "           ,[image]\n"
                + "           ,[status]\n"
                + "           ,[season])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, name);
            st.setString(2, cateID);
            st.setString(3, origin_price);
            st.setString(4, description);
            st.setString(5, image);
            st.setString(6, status);
            st.setString(7, season);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    //DuyAnh
    //Insert Product
    public void insertProduct(String pid, String sale_price) {
        String query = "INSERT INTO SalePrice (productID, startDate, endDate, price)\n"
                + "VALUES (?, '2023-12-01', '2024-12-31', ?);";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, pid);
            st.setString(2, sale_price);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    //Duy Anh
    //insert quantity
    public void insertQuantity(String pid, String quantity, String date) {
        String query = "INSERT INTO [dbo].[ProductStock]\n"
                + "           ([productID]\n"
                + "           ,[userID]\n"
                + "           ,[quantity]\n"
                + "           ,[date])\n"
                + "     VALUES\n"
                + "           (?,1,?,?);";

        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, pid);
            st.setString(2, quantity);
            st.setString(3, date);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    //DuyAnh
    //EdittProduct
//    public void editProduct(String name, String cateID, String origin_price,
//            String description, String image, String status, String id) {
//        String query = "UPDATE [dbo].[Product]\n"
//                + "   SET [name] = ?\n"
//                + "      ,[categoryID] = ?\n"
//                + "      ,[price] = ?\n"
//                + "      ,[description] = ?\n"
//                + "      ,[image] = ?\n"
//                + "      ,[status] = ?\n"
//                + " WHERE id =?";
//        try {
//            PreparedStatement st = connection.prepareStatement(query);
//            st.setString(1, name);
//            st.setString(2, cateID);
//            st.setString(3, origin_price);
//            st.setString(4, description);
//            st.setString(5, image);
//            st.setString(6, status);
//            st.setString(7, id);
//            int rowsAffected = st.executeUpdate();
//            if (rowsAffected > 0) {
//                System.out.println("Product updated successfully!");
//            } else {
//                System.out.println("No rows affected. Product may not exist with the provided ID.");
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
    public void editProduct(String name, String cateID, String origin_price,
        String description, String image, String status, String id) {
    String query = "UPDATE [dbo].[Product]\n"
            + "   SET [name] = ?\n"
            + "      ,[categoryID] = ?\n"
            + "      ,[price] = ?\n"
            + "      ,[description] = ?\n"
            + "      ,[image] = ?\n"
            + "      ,[status] = ?\n"
            + " WHERE id =?";
    try {
        PreparedStatement st = connection.prepareStatement(query);
        st.setString(1, name);
        st.setString(2, cateID);
        st.setString(3, origin_price);
        st.setString(4, description);
        st.setString(5, image);
        st.setString(6, status);
        st.setString(7, id);
        int rowsAffected = st.executeUpdate();
        if (rowsAffected > 0) {
            System.out.println("Product updated successfully!");
        } else {
            System.out.println("No rows affected. Product may not exist with the provided ID.");
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
}

    //duy anh
    //update quantity and date
    public void UpdateQuantity(int quantity, String date, String pid) {
        String query = "UPDATE ProductStock\n"
                + "SET quantity = ?"
                + ",[date] = ?\n"
                + "WHERE productID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, quantity);
            st.setString(2, date);
            st.setString(3, pid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    //DuyAnh
    //get Product by ID
    public Product getProductByID(String id) {
        String query = "	WITH ProductInfo AS (\n"
                + "    SELECT\n"
                + "        P.id AS ProductID,\n"
                + "        P.name AS ProductName,\n"
                + "        P.categoryID,\n"
                + "        ISNULL(SP.price, P.price) AS SalePrice,\n"
                + "        P.price AS OriginPrice,\n"
                + "        P.description,\n"
                + "        P.image,\n"
                + "        (\n"
                + "            SELECT ISNULL(SUM(ps.quantity), 0)\n"
                + "            FROM ProductStock ps\n"
                + "            WHERE ps.productID = P.id\n"
                + "        ) AS TotalStockQuantity,\n"
                + "        (\n"
                + "            SELECT ISNULL(SUM(od.quantity), 0)\n"
                + "            FROM OrderDetail od\n"
                + "            WHERE od.productID = P.id\n"
                + "        ) AS TotalOrderQuantity,\n"
                + "        P.status,\n"
                + "        P.season\n"
                + "    FROM\n"
                + "        Product P\n"
                + "    LEFT JOIN SalePrice SP ON P.id = SP.productID\n"
                + "        AND GETDATE() BETWEEN SP.startDate AND SP.endDate\n"
                + ")\n"
                + "SELECT\n"
                + "    ProductID,\n"
                + "    ProductName,\n"
                + "    categoryID,\n"
                + "    OriginPrice,\n"
                + "    SalePrice,\n"
                + "    description,\n"
                + "    image,\n"
                + "    TotalStockQuantity - TotalOrderQuantity AS Quantity,\n"
                + "    status,\n"
                + "    season\n"
                + "FROM\n"
                + "    ProductInfo\n"
                + "WHERE\n"
                + "    ProductID = ?;\n";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10));
            }
        } catch (Exception e) {
        }
        return null;
    }

    //DuyAnh
    // Filter for product
    public Vector<Product> getProductByFilter(String cid, String status) {
        Vector<Product> vector = new Vector<>();
        String query = "SELECT  p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "       p.description,\n"
                + "       p.image,\n"
                + "       isnull(ps.quantity,0) ,\n"
                + "       ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "WHERE ";

        if (cid != null && !cid.isEmpty() && status != null && !status.isEmpty()) { //nếu có data của điều kiện nào thì thêm yếu tố cho điều kiện 
            query += "p.categoryID = ? AND p.status = ? ";
        } else if (cid != null && !cid.isEmpty()) {
            query += "p.categoryID = ? ";
        } else if (status != null && !status.isEmpty()) {
            query += "p.status = ? ";
        } else {
            query += "1=1 ";
        }

        query += "ORDER BY ps.date;";

        try {
            PreparedStatement st = connection.prepareStatement(query);

            int paramIndex = 1;

            if (cid != null && !cid.isEmpty() && status != null && !status.isEmpty()) {
                st.setString(paramIndex, cid);
                paramIndex++;
                st.setString(paramIndex, status);
            } else if (cid != null && !cid.isEmpty()) {
                st.setString(paramIndex, cid);
            } else if (status != null && !status.isEmpty()) {
                st.setString(paramIndex, status);
            }

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3), rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getDate(9),
                        rs.getInt(10)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vector;
    }

    public List<Product> getTop4Product() {
        List<Product> products = new ArrayList<>();
        String sql = "SELECT top 4 p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	order by ps.date;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product product = new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3), rs.getDouble(4),
                        rs.getDouble(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getDate(9),
                        rs.getInt(10));
                products.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }

    public Product getByID(int id) {
        String sql = "SELECT  p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	where p.id = ?\n";

        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Product product = new Product();
                product.setId(id);
                product.setName(rs.getString(2));
                product.setCateID(rs.getInt(3));
                product.setOrigin_price(rs.getDouble(4));
                product.setSale_price(rs.getDouble(5));
                product.setDescription(rs.getString(6));
                product.setImage(rs.getString(7));
                product.setQuantity(rs.getInt(8));
                product.setDate(rs.getDate(9));
                product.setStatus(rs.getInt(10));
                return product;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Product getInfo(Product product) {
        String sql = "SELECT  p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	where p.id= ?\n";

        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, product.getId());
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                product.setName(rs.getString(2));
                product.setCateID(rs.getInt(3));
                product.setOrigin_price(rs.getDouble(4));
                product.setSale_price(rs.getDouble(5));
                product.setDescription(rs.getString(6));
                product.setImage(rs.getString(7));
                product.setQuantity(rs.getInt(8));
                product.setDate(rs.getDate(9));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    public Vector<Product> getRelated(Product product) {
        Vector products = new Vector();
        String sql = "SELECT top 4 p.id, p.name, p.categoryID, p.price, \n"
                + "       CASE \n"
                + "          WHEN sp.price IS NOT NULL AND GETDATE() BETWEEN sp.startDate AND sp.endDate THEN sp.price\n"
                + "          ELSE p.price\n"
                + "       END AS salePrice,\n"
                + "	   p.description,\n"
                + "	   p.image,\n"
                + "	   isnull(ps.quantity,0),\n"
                + "	   ps.date,p.status\n"
                + "FROM Product p\n"
                + "LEFT JOIN SalePrice sp ON p.id = sp.productID\n"
                + "LEFT JOIN \n"
                + "    ProductStock ps ON p.id = ps.productID\n"
                + "	where p.categoryID = ? and p.id != ?\n"
                + "	order by newid();";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, product.getCateID());
            stm.setInt(2, product.getId());
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product item = new Product();
                item.setId(rs.getInt(1));
                item.setName(rs.getString(2));
                item.setCateID(rs.getInt(3));
                item.setOrigin_price(rs.getDouble(4));
                item.setSale_price(rs.getDouble(5));
                item.setDescription(rs.getString(6));
                item.setImage(rs.getString(7));
                item.setQuantity(rs.getInt(8));
                item.setDate(rs.getDate(9));
                products.add(item);
            }
            return products;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }

    //DuyAnh
    //get static for product chart 
    public Vector<ProductChart> getProductBySeason() {
        Vector<ProductChart> vector = new Vector<>();
        String query = "SELECT season, COUNT(*) AS NumberOfProducts\n"
                + "FROM Product\n"
                + "GROUP BY season;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new ProductChart(rs.getString(1), rs.getInt(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vector;
    }

    public Vector<ProductChart> getProductByFavourite() {
        Vector<ProductChart> vector = new Vector<>();
        String query = "SELECT TOP 7\n"
                + "    COUNT(pf.productID) AS FavoriteCount,"
                + "p.name AS ProductName\n"
                + "FROM\n"
                + "    Product p\n"
                + "JOIN\n"
                + "    Product_Favorite pf ON p.id = pf.productID\n"
                + "GROUP BY\n"
                + "    p.id, p.name\n"
                + "ORDER BY\n"
                + "    FavoriteCount ;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new ProductChart(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vector;
    }

//    //Hoang Anh
//    //Test method
//    public static void main(String[] args) {
//        ProductDAO productDAO = new ProductDAO();
//
////        productDAO.editProduct("Iphone 14 Pro", "1", "3000", "small", "iphone_14pro.jpg", "0", "1");
////        Product product = productDAO.getProductByID("1");
////        System.out.println(product);
////        Vector<Product> topProducts = productDAO.getProductByFilter("","");
////        Product product = productDAO.getProductByID("1");
////        System.out.println(product);
////        productDAO.insertProduct("test", "1", "2000", "test", "iphone_14pro.jpg", "1", "spring");
////        productDAO.insertProduct("26", sale_price);
////        productDAO.insertQuantity(pid, quantity, date);
//        Vector<Product> topProducts = productDAO.getAllProduct();
//
//        for (Product product : topProducts) {
//            System.out.println(product);
//        }
//    }

}
