/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.AfterChange;
import entity.BeforeChange;
import entity.ContactInfo;
import entity.CustomerChart;
import entity.Favorite_Product;
import entity.History;
import entity.OrderDetail;
import entity.OrderList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import entity.User;
import entity.UserContactInfo;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

/**
 *
 * @author Phuc Lam
 */
public class UserDAO extends DBContext {

    //LamTP
    // This function use to check account exist. 
    // If account exist, system accept it access
    public User checkAccount(String email, String password) {
        String sql = "SELECT [id]\n"
                + "      ,[email]\n"
                + "      ,[password]\n"
                + "      ,[roleID]\n"
                + "      ,[confirmation]\n"
                + "      ,[status]\n"
                + "      ,[fullName]\n"
                + "      ,[gender]\n"
                + "      ,[imageURL]\n"
                + "      ,[Date]\n"
                + "  FROM [dbo].[User]"
                + "WHERE [email] = ? AND [password] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("id"), rs.getString("email"),
                        rs.getString("password"),
                        rs.getInt("roleID"), rs.getInt("confirmation"),
                        rs.getInt("status"), rs.getString("fullName"),
                        rs.getInt("gender"), rs.getString("imageURL"), rs.getDate("Date"));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    //LamTP
    // This function use to get account information by email
    public User getUserByEmail(String email) {
        String sql = "SELECT [id]\n"
                + "      ,[email]\n"
                + "      ,[password]\n"
                + "      ,[roleID]\n"
                + "      ,[confirmation]\n"
                + "      ,[status]\n"
                + "      ,[fullName]\n"
                + "      ,[gender]\n"
                + "      ,[imageURL]\n"
                + "      ,[Date]\n"
                + "  FROM [dbo].[User]"
                + "WHERE [email] = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("id"), rs.getString("email"),
                        rs.getString("password"),
                        rs.getInt("roleID"), rs.getInt("confirmation"),
                        rs.getInt("status"), rs.getString("fullName"),
                        rs.getInt("gender"), rs.getString("imageURL"), rs.getDate("Date"));
            }
        } catch (SQLException e) {
        }
        return null;
    }
    
    
        public User getUserByUserID(int id) {
        String sql = "SELECT [id]\n"
                + "      ,[email]\n"
                + "      ,[password]\n"
                + "      ,[roleID]\n"
                + "      ,[confirmation]\n"
                + "      ,[status]\n"
                + "      ,[fullName]\n"
                + "      ,[gender]\n"
                + "      ,[imageURL]\n"
                + "      ,[Date]\n"
                + "  FROM [dbo].[User]"
                + "WHERE id = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("id"), rs.getString("email"),
                        rs.getString("password"),
                        rs.getInt("roleID"), rs.getInt("confirmation"),
                        rs.getInt("status"), rs.getString("fullName"),
                        rs.getInt("gender"), rs.getString("imageURL"), rs.getDate("Date"));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public int getUserID(String email, String password) {
        String sql = "SELECT [id] FROM [dbo].[User] WHERE [email] = ? AND [password] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException e) {
        }
        return -1; // Return -1 if no user is found with the provided email and password
    }

    //LamTP
    // This function use to update pass 
    // When user login successfully 
    // In case of  user forgot password and want to reset it
    public void updatePass(User u) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET [password] =? \n"
                + " WHERE email = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u.getPassword());
            st.setString(2, u.getEmail());
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }


    
    //LamTP
    // This function use to confirm that user account is activated
    public void ActiveAccount(String email) {
                String sql = "UPDATE [dbo].[User]\n"
                + "   SET [confirmation] =1 \n"
                + " WHERE email = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //LamTP
    // This function use to create a new account
    public void insertUser(User user) {
        String sql = "INSERT INTO [dbo].[User] ([id], [email], [password],"
                + " [roleID], [confirmation], [status], [fullName], [gender], "
                + "[imageURL]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, user.getId());
            st.setString(2, user.getEmail());
            st.setString(3, user.getPassword());
            st.setInt(4, user.getRoleID());
            st.setInt(5, user.getConfirmation());
            st.setInt(6, user.getStatus());
            st.setString(7, user.getFullName());
            st.setInt(8, user.getGender());
            st.setString(9, user.getImageURL());

            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //LamTP
    // This function is use to set user contact info
    public void insertContactInfo(ContactInfo contactInfo) {
        String sql = "INSERT INTO [dbo].[ContactInfo] ([id], [fullName],"
                + " [phone], [note], [userID], [address]) "
                + "VALUES (?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, contactInfo.getId());
            st.setString(2, contactInfo.getFullName());
            st.setString(3, contactInfo.getPhone());
            st.setString(4, contactInfo.getNote());
            st.setInt(5, contactInfo.getUserID());
            st.setString(6, contactInfo.getAddress());

            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //LamTP
    // This fucntion use to get last user ID to insert new user which id ascending
    public User getLastUser() {
        String sql = "SELECT TOP 1 * FROM [dbo].[User] ORDER BY [id] DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("id"), rs.getString("email"),
                        rs.getString("password"), rs.getInt("roleID"),
                        rs.getInt("confirmation"), rs.getInt("status"),
                        rs.getString("fullName"), rs.getInt("gender"),
                        rs.getString("imageURL"), rs.getDate("Date"));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    //LamTP
    // This fucntion use to get last user contact ID to insert new contact which 
    // id ascending
    public ContactInfo getLastContact() {
        String sql = "SELECT TOP 1 * FROM [dbo].[ContactInfo] ORDER BY [id] DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new ContactInfo(rs.getInt("id"), rs.getString("fullName"),
                        rs.getString("phone"), rs.getString("note"),
                        rs.getInt("userID"), rs.getString("Address"), rs.getString("email"));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    //LamTP
    // This function use to get user contact information
    public ContactInfo getContactInfoByUserId(int id) {
        String sql = "SELECT * FROM [dbo].[ContactInfo] where userID = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new ContactInfo(rs.getInt("id"), rs.getString("fullName"),
                        rs.getString("phone"), rs.getString("note"),
                        rs.getInt("userID"), rs.getString("Address"),rs.getString("email"));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    //LamTP
    // This function use to change infor of user in home page
    public void updateUser(User u) {
        String sql = "UPDATE [dbo].[User] SET [fullName] =? , [gender] =? , "
                + "[imageURL] =? , [roleID]=? WHERE email = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u.getFullName());
            st.setInt(2, u.getGender());
            st.setString(3, u.getImageURL());
            st.setInt(4, u.getRoleID());
            st.setString(5, u.getEmail());
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //LamTP
    // This function use to change contact info of user
    public void updateContact(ContactInfo ci) {
        String sql = "UPDATE [dbo].[ContactInfo] SET [fullName] =? , "
                + "[phone] =? , [note] =?, [Address] =?, [email]=?  WHERE userID = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, ci.getFullName());
            st.setString(2, ci.getPhone());
            st.setString(3, ci.getNote());
            st.setString(4, ci.getAddress());
            st.setString(5, ci.getEmail());
            st.setInt(6, ci.getUserID());
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //LamTP
    // This function use to get favorite product of each user
    public Vector<Favorite_Product> getFavoriteProductsByUser(int id) {
        Vector<Favorite_Product> favoriteProducts = new Vector<>();
        String sql = "SELECT [userID], [productID] FROM [dbo].[Product_Favorite] WHERE [userID] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Favorite_Product favoriteProduct = new Favorite_Product(rs.getInt(1), rs.getInt(2));
                favoriteProducts.add(favoriteProduct);
            }
        } catch (SQLException e) {
        }
        return favoriteProducts;
    }

    //LamTP
    // This function use to add new product for favorite product list
    public void addFavoriteProduct(int userID, int productID) {
        String sql = "Insert into [dbo].[Product_Favorite] ([userID], [productID]) VALUES (?, ?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            st.setInt(2, productID);

            st.executeUpdate();
        } catch (SQLException e) {
        }
    }


    //LamTP
    // This function use to get customer with each contact infor
    public Vector<UserContactInfo> getAllCustomerWithContactInfo() {
        Vector<UserContactInfo> vector = new Vector<>();
        String sql = "SELECT u.id AS userId, u.email, u.password, u.roleID, u.confirmation, "
                + "u.status, u.fullName, u.gender, u.imageURL, u.Date,c.id AS contactId, "
                + "c.fullName AS contactFullName, c.phone, c.note, c.address,c.email "
                + "FROM [dbo].[User] u "
                + "INNER JOIN [dbo].[ContactInfo] c ON u.id = c.userID "
                + "WHERE u.roleID = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("email"),
                        rs.getString("password"), rs.getInt("roleID"),
                        rs.getInt("confirmation"), rs.getInt("status"),
                        rs.getString("fullName"), rs.getInt("gender"),
                        rs.getString("imageURL"), rs.getDate("Date"));
                ContactInfo contactInfo = new ContactInfo(rs.getInt("contactId"),
                        rs.getString("contactFullName"),
                        rs.getString("phone"), rs.getString("note"),
                        rs.getInt("userId"), rs.getString("address"),rs.getString("email"));
                vector.add(new UserContactInfo(user, contactInfo));
            }
        } catch (SQLException e) {
        }
        return vector;
    }

    //LamTP
    // This function use to get user with each contact infor
    public Vector<UserContactInfo> getAllUserWithContactInfo() {
        Vector<UserContactInfo> vector = new Vector<>();
        String sql = "SELECT u.id AS userId, u.email, u.password, u.roleID, u.confirmation, "
                + "u.status, u.fullName, u.gender, u.imageURL, u.Date, c.id AS contactId, "
                + "c.fullName AS contactFullName, c.phone, c.note, c.address, c.email "
                + "FROM [dbo].[User] u "
                + "INNER JOIN [dbo].[ContactInfo] c ON u.id = c.userID ";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("email"),
                        rs.getString("password"), rs.getInt("roleID"),
                        rs.getInt("confirmation"), rs.getInt("status"),
                        rs.getString("fullName"), rs.getInt("gender"),
                        rs.getString("imageURL"), rs.getDate("Date"));
                ContactInfo contactInfo = new ContactInfo(rs.getInt("contactId"),
                        rs.getString("contactFullName"),
                        rs.getString("phone"), rs.getString("note"),
                        rs.getInt("userId"), rs.getString("address"),rs.getString("email"));
                vector.add(new UserContactInfo(user, contactInfo));
            }
        } catch (SQLException e) {
        }
        return vector;
    }

    //LamTP
    // This function use to get all user of infor and its contact by user id
    public UserContactInfo getCustomerDetailByUserId(int userId) {
        UserContactInfo userContactInfo = null;
        String sql = "SELECT u.id AS userId, u.email, u.password, u.roleID, u.confirmation, "
                + "u.status, u.fullName, u.gender, u.imageURL, u.Date, c.id AS contactId, "
                + "c.fullName AS contactFullName, c.phone, c.note, c.address, c.email as contactEmail "
                + "FROM [dbo].[User] u "
                + "INNER JOIN [dbo].[ContactInfo] c ON u.id = c.userID "
                + "WHERE  u.id = ?"; // Lọc theo userId

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("email"),
                        rs.getString("password"), rs.getInt("roleID"),
                        rs.getInt("confirmation"), rs.getInt("status"),
                        rs.getString("fullName"), rs.getInt("gender"),
                        rs.getString("imageURL"),rs.getDate("Date"));
                ContactInfo contactInfo = new ContactInfo(rs.getInt("contactId"),
                        rs.getString("contactFullName"),
                        rs.getString("phone"), rs.getString("note"),
                        rs.getInt("userId"), rs.getString("address"),rs.getString("contactEmail"));
                userContactInfo = new UserContactInfo(user, contactInfo);
            }
        } catch (SQLException e) {
        }
        return userContactInfo;
    }

    //LamTP
    // This function use to filter user by status
    public Vector<UserContactInfo> filterUserContactInfoByStatus(int status) {
        Vector<UserContactInfo> filteredList = new Vector<>();
        String sql = "SELECT u.id AS userId, u.email, u.password, u.roleID, u.confirmation, "
                + "u.status, u.fullName, u.gender, u.imageURL, u.Date, c.id AS contactId, "
                + "c.fullName AS contactFullName, c.phone, c.note, c.address,c.email "
                + "FROM [dbo].[User] u "
                + "INNER JOIN [dbo].[ContactInfo] c ON u.id = c.userID "
                + "WHERE u.roleID = 1 AND u.status = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("email"),
                        rs.getString("password"), rs.getInt("roleID"),
                        rs.getInt("confirmation"), rs.getInt("status"),
                        rs.getString("fullName"), rs.getInt("gender"),
                        rs.getString("imageURL"), rs.getDate("Date"));
                ContactInfo contactInfo = new ContactInfo(rs.getInt("contactId"),
                        rs.getString("contactFullName"),
                        rs.getString("phone"), rs.getString("note"),
                        rs.getInt("userId"), rs.getString("address"),rs.getString("email"));
                filteredList.add(new UserContactInfo(user, contactInfo));
            }
        } catch (SQLException e) {
        }
        return filteredList;
    }

    //LamTP
    // This function get all history of changes for a specific user ID
    public ArrayList<History> getAllHistoryByUserId(int userId) {
        ArrayList<History> historyList = new ArrayList<>();
        String sql = "SELECT \n"
                + "    H.dateChange AS Change_Time,\n"
                + "    U.email AS UserID_Email,\n"
                + "    E.fullName AS Editor_Name,\n"
                + "    AC.fullName AS AfterChange_Name,\n"
                + "    AC.gender AS AfterChange_Gender,\n"
                + "    AC.phone AS AfterChange_Phone,\n"
                + "    AC.Address AS AfterChange_Address,\n"
                + "    BC.fullName AS BeforeChange_Name,\n"
                + "    BC.gender AS BeforeChange_Gender,\n"
                + "    BC.phone AS BeforeChange_Phone,\n"
                + "    BC.Address AS BeforeChange_Address\n"
                + "FROM \n"
                + "    History H\n"
                + "JOIN \n"
                + "    [dbo].[User] U ON H.userID = U.id\n"
                + "JOIN \n"
                + "    [dbo].[User] E ON H.editerID = E.id\n"
                + "JOIN \n"
                + "    AfterChange AC ON H.afterID = AC.aID\n"
                + "JOIN \n"
                + "    BeforeChange BC ON H.beforeID = BC.bID"
                + "	WHERE U.id =?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                History history = new History();
                history.setDate(rs.getDate("Change_Time"));

                User changeUser = new User();
                changeUser.setEmail(rs.getString("UserID_Email"));

                User editUser = new User();
                editUser.setFullName(rs.getString("Editor_Name"));

                AfterChange afterChange = new AfterChange();

                int genderAfter = rs.getInt("AfterChange_Gender");
                //If you check that the data is null in the database, 
                //assign it to -1 in the entiry
                if (rs.wasNull()) {
                    afterChange.setGender(-1);
                } else {
                    afterChange.setGender(genderAfter);
                }

                afterChange.setFullName(rs.getString("AfterChange_Name"));
                afterChange.setPhone(rs.getString("AfterChange_Phone"));
                afterChange.setAddress(rs.getString("AfterChange_Address"));

                BeforeChange beforeChange = new BeforeChange();

                int genderBefore = rs.getInt("BeforeChange_Gender");
                //If you check that the data is null in the database,
                //assign it to -1 in the entiry
                if (rs.wasNull()) {
                    beforeChange.setGender(-1);
                } else {
                    beforeChange.setGender(genderBefore);
                }

                beforeChange.setFullName(rs.getString("BeforeChange_Name"));
                beforeChange.setPhone(rs.getString("BeforeChange_Phone"));
                beforeChange.setAddress(rs.getString("BeforeChange_Address"));

                history.setChange(changeUser);
                history.setEdit(editUser);
                history.setAfter(afterChange);
                history.setBefore(beforeChange);

                historyList.add(history);
            }
        } catch (SQLException e) {
        }
        return historyList;
    }

    //LamTP
    //This function use to save the new field info of user has changed
    public void insertAfterChange(AfterChange afterChange) {
        String sql = "INSERT INTO [dbo].[AfterChange] ( [fullName], [gender], "
                + "[phone], [address]) VALUES ( ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, afterChange.getFullName());
            st.setInt(2, afterChange.getGender());
            st.setString(3, afterChange.getPhone());
            st.setString(4, afterChange.getAddress());

            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //LamTP
    //This function use to save the old field info of user before changing
    public void insertBeforeChange(BeforeChange beforeChange) {
        String sql = "INSERT INTO [dbo].[BeforeChange] ( [fullName], [gender], "
                + "[phone], [address]) VALUES ( ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, beforeChange.getFullName());
            st.setInt(2, beforeChange.getGender());
            st.setString(3, beforeChange.getPhone());
            st.setString(4, beforeChange.getAddress());

            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //LamTP
    //This function use to insert the user history change 
    public void insertHistory(int userID, int editerID, int beforeID,
            int afterID, Date dateChange) {

        String sql = "INSERT INTO [dbo].[History] ([userID], [editerID], "
                + "[beforeID], [afterID], [dateChange]) VALUES (?, ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            st.setInt(2, editerID);
            st.setInt(3, beforeID);
            st.setInt(4, afterID);
            st.setDate(5, dateChange);

            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //LamTP
    // This function retrieves the last BeforeChange record
    public BeforeChange getLastBeforeChange() {
        String sql = "SELECT TOP 1 * FROM [dbo].[BeforeChange] ORDER BY [bID] DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new BeforeChange(
                        rs.getInt("bID"),
                        rs.getString("fullName"),
                        rs.getInt("gender"),
                        rs.getString("phone"),
                        rs.getString("address")
                );
            }
        } catch (SQLException e) {
        }
        return null;
    }

    //LamTP
    // This function retrieves the last AfterChange record
    public AfterChange getLastAfterChange() {
        String sql = "SELECT TOP 1 * FROM [dbo].[AfterChange] ORDER BY [aID] DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new AfterChange(
                        rs.getInt("aID"),
                        rs.getString("fullName"),
                        rs.getInt("gender"),
                        rs.getString("phone"),
                        rs.getString("address")
                );
            }
        } catch (SQLException e) {
        }
        return null;
    }

    //LamTP
    //This function use to get Order History of each customer
    public Vector<OrderList> getAllOrderByUserID(int userID) {
        Vector<OrderList> vector = new Vector<>();
        String query = "SELECT \n"
                + "    o.id AS order_id,\n"
                + "    ci.fullName AS full_name,\n"
                + "    u.email,\n"
                + "    ci.phone,\n"
                + "    ci.Address AS address,\n"
                + "    o.date AS order_date,\n"
                + "    (SELECT SUM(od.quantity * od.price) FROM OrderDetail od WHERE od.orderID = o.id) AS total,\n"
                + "    os.name AS status\n"
                + "FROM \n"
                + "    [dbo].[Order] o\n"
                + "JOIN \n"
                + "    [dbo].[ContactInfo] ci ON o.contactInfoID = ci.id\n"
                + "JOIN \n"
                + "    [dbo].[User] u ON o.userID = u.id\n"
                + "JOIN \n"
                + "    [dbo].[OrderStatus] os ON o.statusID = os.id Where o.userID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, userID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new OrderList(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        rs.getDouble(7),
                        rs.getString(8)));
            }
        } catch (SQLException e) {
        }
        return vector;
    }

    //LamTP
    //This function use to get common info of each order that user choose
    //including customer contact and total, status of order 
    public OrderList getOrderByID(String id) {
        String query = "SELECT \n"
                + "    o.id AS order_id,\n"
                + "    ci.fullName AS full_name,\n"
                + "    u.email,\n"
                + "    ci.phone,\n"
                + "    ci.Address AS address,\n"
                + "    o.date AS order_date,\n"
                + "    (SELECT SUM(od.quantity * od.price) FROM OrderDetail od WHERE od.orderID = o.id) AS total,\n"
                + "    os.name AS status\n"
                + "FROM \n"
                + "    [dbo].[Order] o\n"
                + "JOIN \n"
                + "    [dbo].[ContactInfo] ci ON o.contactInfoID = ci.id\n"
                + "JOIN \n"
                + "    [dbo].[User] u ON o.userID = u.id\n"
                + "JOIN \n"
                + "    [dbo].[OrderStatus] os ON o.statusID = os.id\n"
                + "WHERE o.id =?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new OrderList(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        rs.getDouble(7),
                        rs.getString(8));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    //LamTP
    //This function use to get detail each product of from order that user choose
    public Vector<OrderDetail> getDetailByOID(String oid) {
        Vector<OrderDetail> vector = new Vector<>();
        String query = "SELECT \n"
                + "   p.id, p.image,\n"
                + "    p.name AS productName,\n"
                + "    c.name AS categoryName,\n"
                + "    od.price,\n"
                + "    od.quantity,\n"
                + "    (od.price * od.quantity) AS amount\n"
                + "FROM \n"
                + "    OrderDetail od\n"
                + "JOIN \n"
                + "    Product p ON od.productID = p.id\n"
                + "JOIN \n"
                + "    Category c ON p.categoryID = c.id\n"
                + "where od.orderID =?";
        try {
            PreparedStatement st = connection.prepareStatement(query);

            st.setString(1, oid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new OrderDetail(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getInt(6), rs.getDouble(7)));
            }
        } catch (SQLException e) {
        }
        return vector;
    }

    //LamTP
    //This function use to filter User by role, gender, status
    public Vector<UserContactInfo> filterUser(int[] roles, int[] genders, int[] statuses) {
        Vector<UserContactInfo> filteredUsers = new Vector<>();
        String sql = "SELECT u.id AS userId, u.email, u.password, u.roleID, u.confirmation, "
                + "u.status, u.fullName, u.gender, u.imageURL, u.Date, c.id AS contactId, "
                + "c.fullName AS contactFullName, c.phone, c.note, c.address, c.email "
                + "FROM [dbo].[User] u "
                + "INNER JOIN [dbo].[ContactInfo] c ON u.id = c.userID "
                + "WHERE 1=1 ";  // This is default that ensure WHERE is always correct

        //Add conditions to the role if there is data
        if (roles != null && roles.length > 0) {
            sql += "AND u.roleID IN (" + String.join(",", Collections.nCopies(roles.length, "?")) + ") ";
        }

        //Add conditions to the gender if there is data
        if (genders != null && genders.length > 0) {
            sql += "AND u.gender IN (" + String.join(",", Collections.nCopies(genders.length, "?")) + ") ";
        }

        //Add conditions to the status if there is data
        if (statuses != null && statuses.length > 0) {
            sql += "AND u.status IN (" + String.join(",", Collections.nCopies(statuses.length, "?")) + ") ";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            // Set parameters for roles, genders, and statuses if applicable
            int paramIndex = 1;
            if (roles != null) {
                for (int role : roles) {
                    st.setInt(paramIndex++, role);
                }
            }

            if (genders != null) {
                for (int gender : genders) {
                    st.setInt(paramIndex++, gender);
                }
            }

            if (statuses != null) {
                for (int status : statuses) {
                    st.setInt(paramIndex++, status);
                }
            }

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("email"),
                        rs.getString("password"), rs.getInt("roleID"),
                        rs.getInt("confirmation"), rs.getInt("status"),
                        rs.getString("fullName"), rs.getInt("gender"),
                        rs.getString("imageURL"), rs.getDate("Date"));
                ContactInfo contactInfo = new ContactInfo(rs.getInt("contactId"),
                        rs.getString("contactFullName"),
                        rs.getString("phone"), rs.getString("note"),
                        rs.getInt("userId"), rs.getString("address"), rs.getString("email"));
                filteredUsers.add(new UserContactInfo(user, contactInfo));
            }
        } catch (SQLException e) {
        }
        return filteredUsers;
    }

    //LamTP
    //This function use to remove product from favorite list
    public void deleteFavoriteProduct(int userID, int productID) {
        String sql = "DELETE FROM Product_Favorite WHERE userID = ? AND productID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            st.setInt(2, productID);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //LamTP
    //This function user to change account status.
    public void updateStatus(int userID, int newStatus) {
        String sql = "UPDATE [dbo].[User] SET [status] = ? WHERE [id] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, newStatus);
            st.setInt(2, userID);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //DuyAnh
    //get data for cutomer chart 
    public Vector<CustomerChart> get7CustomerChartByDay() {
        Vector<CustomerChart> vector = new Vector<>();
        String query = "	SELECT COUNT(*) AS CustomerCount,\n"
                + "    [Date]\n"
                + "FROM \n"
                + "    [dbo].[User]\n"
                + "WHERE\n"
                + "    [Date] >= DATEADD(DAY, -7, GETDATE()) and roleID =1\n"
                + "GROUP BY \n"
                + "    [Date]\n"
                + "ORDER BY \n"
                + "    [Date] ;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new CustomerChart(rs.getInt(1), rs.getDate(2)));
            }
        } catch (SQLException e) {
        }
        return vector;
    }

    public Vector<CustomerChart> get7CustomerChartByWeek() {
        Vector<CustomerChart> vector = new Vector<>();
        String query = "SELECT \n"
                + "    COUNT(*) AS BlogCount,\n "
                + "    DATEADD(WEEK, DATEDIFF(WEEK, 0, [Date]), 0) AS WeekStart\n"
                + "FROM \n"
                + "    [dbo].[User]\n"
                + "WHERE\n"
                + "    [Date] >= DATEADD(WEEK, -7, GETDATE()) AND roleID = 1\n"
                + "GROUP BY \n"
                + "    DATEADD(WEEK, DATEDIFF(WEEK, 0, [Date]), 0)\n"
                + "ORDER BY \n"
                + "    WeekStart;";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                vector.add(new CustomerChart(rs.getInt(1), rs.getDate(2)));
            }
        } catch (SQLException e) {
        }
        return vector;
    }

}
