/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.User;

import entity.ContactInfo;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import model.UserDAO;

/**
 *
 * @author Phuc Lam
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegisterServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegisterServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private String md5Hash(String input) {
        try {
            // Khởi tạo đối tượng MessageDigest với thuật toán MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            // Chuyển đổi chuỗi đầu vào thành mảng byte
            byte[] inputBytes = input.getBytes();
            // Băm mảng byte và lưu kết quả vào hashedBytes
            byte[] hashedBytes = md.digest(inputBytes);
            // Khởi tạo đối tượng StringBuilder để xây dựng chuỗi hexa của băm
            StringBuilder stringBuilder = new StringBuilder();
            for (byte b : hashedBytes) {
                // Chuyển đổi byte thành chuỗi hexa và nối vào stringBuilder
                stringBuilder.append(String.format("%02x", b));
            }
            // Trả về chuỗi hexa của băm
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    // This function use register for guest
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String pass = request.getParameter("pass");
        String cpass = request.getParameter("cpass");
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String gender = request.getParameter("gender");
        UserDAO cbd = new UserDAO();

        // Check email we use to register invalid or not
        User u = cbd.getUserByEmail(email);
        int uNewID = cbd.getLastUser().getId() + 1;
        int cNewID = cbd.getLastContact().getId() + 1;
        // Set up name condition
        String checkName = "^[a-zA-Z]+(?:\\s+[a-zA-Z]+)*$";
        name = name.replaceAll("\\s+", " ");
        // Set up password conditions
        String uppercaseRegex = ".*[A-Z].*";
        String lowercaseRegex = ".*[a-z].*";
        String specialCharacterRegex = ".*[@#$%^&+=!].*";
        String minLengthRegex = ".{8,}";

        // Initialize an empty error message
        String error = "";

        if (!name.matches(checkName)) {
            error += "Name must be word.<br>";
        }
        // If the email is existed from database, we send a notification for them
        if (u != null) {
            error += "Username already exists.<br>";
        }

        // Check password conditions and append error messages       
        //Check password include at least 1 uppercase letter
        if (!pass.matches(uppercaseRegex)) {
            error += "Password lack uppercase letter.<br>";
        }
        //Check password include at least 1 lowercase letter
        if (!pass.matches(lowercaseRegex)) {
            error += "Password lack lowercase letter. <br>";
        }
        //Check password include at least 1 speacial charater
        if (!pass.matches(specialCharacterRegex)) {
            error += "Password need one special character.<br> ";
        }

        //Check password have length must be >=8
        if (!pass.matches(minLengthRegex)) {
            error += "Password must length >= 8. <br>";
        }

        //Check pass and confirm pass must be pass
        if (!pass.equals(cpass)) {
            error += "Passwords do not match.<br> ";
        }

        // Check if any errors occurred
        if (!error.isEmpty()) {
            // Set the accumulated error message
            request.setAttribute("error", error);
            request.getRequestDispatcher("SignUp.jsp").forward(request, response);
        } else {
            try {
                int gen = Integer.parseInt(gender);
                String img = "avatar.jpg";
                String note = "Use Email";
                String pass_Md5 = md5Hash(pass);
                User uNew = new User(uNewID, email, pass_Md5, 1, 0, 1, name, gen, img, null);
                ContactInfo cNew = new ContactInfo(cNewID, name, phone, note, uNewID, address, email);

                cbd.insertUser(uNew);
                cbd.insertContactInfo(cNew);

                // Set an email you must confirm to activate the account
                String link = generateLink("http://localhost:8080/SWP391_Project/confirm.jsp", email);
                sendConfirmationEmail(email, link);

                request.setAttribute("error", "Please check email to confirm account");
                request.getRequestDispatcher("SignUp.jsp").forward(request, response);

            } catch (ServletException | IOException | NumberFormatException e) {

            }
        }

    }

    // This function allow Admin and Marketing create account 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String pass = request.getParameter("pass");
        String cpass = request.getParameter("cpass");
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String gender = request.getParameter("gender");
        String confirmAccount = request.getParameter("confirm");
        String role_raw = request.getParameter("role");
        UserDAO cbd = new UserDAO();

        // Check email we use to register invalid or not
        User u = cbd.getUserByEmail(email);
        int uNewID = cbd.getLastUser().getId() + 1;
        int cNewID = cbd.getLastContact().getId() + 1;
        
        // Set up name condition
        String checkName = "^[a-zA-Z]+(?:\\s+[a-zA-Z]+)*$";
        name = name.replaceAll("\\s+", " ");
        // Set up address condition
        address = address.replaceAll("\\s+", " ");
        // Set up password conditions
        String uppercaseRegex = ".*[A-Z].*";
        String lowercaseRegex = ".*[a-z].*";
        String specialCharacterRegex = ".*[@#$%^&+=!].*";
        String minLengthRegex = ".{8,}";

        String error = "";
        
        if (!name.matches(checkName)) {
            error += "Name must be word.<br>";
        }
        // If the email is existed from database, we send a notification for them
        if (u != null) {
            error += "Username already exists.";
        }

        // Check password conditions and append error messages
        if (!pass.matches(uppercaseRegex)) {
            error += " Password lack uppercase letter.";
        }

        if (!pass.matches(lowercaseRegex)) {
            error += " Password lack lowercase letter. ";
        }

        if (!pass.matches(specialCharacterRegex)) {
            error += " Password need one special character. ";
        }

        if (!pass.matches(minLengthRegex)) {
            error += " Password must length >= 8.";
        }

        if (!pass.equals(cpass)) {
            error += " Passwords do not match. ";
        }

        // Check if any errors occurred
        if (!error.isEmpty()) {
            // Set the accumulated error message
            request.setAttribute("error", error);
            request.getRequestDispatcher("addNewCustomer.jsp").forward(request, response);
        } else {
            try {
                int gen = Integer.parseInt(gender);
                String img = "avatar.jpg";
                String note = "Use Email";
                int role;

                // check role value exist or not. 
                // If it is null, we set role value = 1(customer)
                if (role_raw == null) {
                    role = 1;

                    //If exist, we get from request
                } else {
                    role = Integer.parseInt(role_raw);
                }
                int confirm = Integer.parseInt(confirmAccount);
                User uNew = new User(uNewID, email, pass, role, confirm, 1, name, gen, img, null);
                ContactInfo cNew = new ContactInfo(cNewID, name, phone, note, uNewID, address, email);

                cbd.insertUser(uNew);
                cbd.insertContactInfo(cNew);
                String msg = " Here is your password for new account: " + pass;
                sendConfirmationEmail(email, msg);

                request.setAttribute("error", "Create successful");

                request.getRequestDispatcher("manageCustomer").forward(request, response);

            } catch (ServletException | IOException | NumberFormatException e) {

            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void sendConfirmationEmail(String email, String message) {
        // This is host email
        final String from = "lamtp2810@gmail.com";
        final String password = "rbxtkzwzapslqjef";

        // Set attribute for email
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        //TLS 587 and SSL 465. You should you 587 for more security
        props.put("mail.smtp.port", "587");
        // Command means when you use an email server to send each message. 
        // You must identify the email server you are logging into
        props.put("mail.smtp.auth", "true");

        props.put("mail.smtp.starttls.enable", "true");

        // This is authen what accpeted we connect host email to sent message
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };

        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);

        try {
            // Set type of title and content of email, we sent
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            // The email reccied the message, we sent
            msg.setFrom(from);
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            // Title of email
            msg.setSubject("Click link to confirm your account!");
            // Content of email
            msg.setText(message, "UTF-8");
            // Send email
            Transport.send(msg);
        } catch (MessagingException e) {
            // Handle exceptions
        }
    }

    private static String generateLink(String originalLink, String userEmail) {
        try {
            long currentTime = System.currentTimeMillis();
            // Set time the link exist
            long expirationTime = currentTime + TimeUnit.MINUTES.toMillis(2);

            // Add the param email and the param link to email.            
            String signedLink = originalLink + "?email=" + userEmail
                    + "&expiration=" + expirationTime;
            return signedLink;
        } catch (Exception e) {
            return null;
        }
    }

}
