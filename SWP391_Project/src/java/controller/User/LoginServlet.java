/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.User;

import model.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import entity.User;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Phuc Lam
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String e = request.getParameter("email");
        String p = request.getParameter("pass");
        String r = request.getParameter("rem");

        UserDAO d = new UserDAO();

        // Check if account exists
        User a = d.getUserByEmail(e);

        if (a == null) {
            // If account doesn't exist, return an error message to the user
            request.setAttribute("error", "Email not exist");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else if (!checkPassword(p, a.getPassword())) {
            // If password is incorrect, return an error message to the user
            request.setAttribute("error", "Password not correct");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else if (a.getConfirmation() == 0) {
            // If account hasn't been confirmed, return an error message to the user
            request.setAttribute("error", "Please check email to confirm account");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("account", a);

            Cookie cu = new Cookie("cuser", e);
            Cookie cp = new Cookie("cpass", p);
            Cookie cr = new Cookie("crem", r);

            if (r != null) {
                cu.setMaxAge(60 * 60 * 24);
                cp.setMaxAge(60 * 60 * 24);
                cr.setMaxAge(60 * 60 * 24);
            } else {
                cu.setMaxAge(0);
                cp.setMaxAge(0);
                cr.setMaxAge(0);
            }

            response.addCookie(cu);
            response.addCookie(cp);
            response.addCookie(cr);

            switch (a.getRoleID()) {
                case 4:
                    response.sendRedirect("manageCustomer");
                    break;
                case 3:
                    response.sendRedirect("UserChartByDay");
                    break;
                case 2:
                    response.sendRedirect("ShowChart");
                    break;
                default:
                    response.sendRedirect("home");
                    break;
            }
        }
    }

    private boolean checkPassword(String inputPassword, String hashedPassword) {
        // Băm mật khẩu đầu vào thành MD5
        String inputHash = md5Hash(inputPassword);
            // So sánh băm đầu vào với mật khẩu đã được băm
        return inputHash.equals(hashedPassword);
    }

    private String md5Hash(String input) {
        try {
            // Khởi tạo đối tượng MessageDigest với thuật toán MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            // Chuyển đổi chuỗi đầu vào thành mảng byte
            byte[] inputBytes = input.getBytes();
            // Băm mảng byte và lưu kết quả vào hashedBytes
            byte[] hashedBytes = md.digest(inputBytes);
            // Khởi tạo đối tượng StringBuilder để xây dựng chuỗi hexa của băm
            StringBuilder stringBuilder = new StringBuilder();
            for (byte b : hashedBytes) {
                // Chuyển đổi byte thành chuỗi hexa và nối vào stringBuilder
                stringBuilder.append(String.format("%02x", b));
            }
             // Trả về chuỗi hexa của băm
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
