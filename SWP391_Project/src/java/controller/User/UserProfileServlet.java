/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.User;

import entity.ContactInfo;
import entity.Favorite_Product;
import entity.Product;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Vector;
import model.ProductDAO;
import model.UserDAO;

/**
 *
 * @author Phuc Lam
 */
@WebServlet(name = "UserProfileServlet", urlPatterns = {"/userProfile"})
public class UserProfileServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserProfileServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserProfileServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        // Get account session from the page
        User account = (User) session.getAttribute("account");

        String email = account.getEmail();
        int id = account.getId();

        UserDAO d = new UserDAO();

        // Get account infor
        User user = d.getUserByEmail(email);
        ContactInfo contact = d.getContactInfoByUserId(id);

        // Get favorite product id
        int productID;
        Product product;
        ProductDAO dao = new ProductDAO();

        Vector<Favorite_Product> favoriteProducts = d.getFavoriteProductsByUser(id);

        Vector<Product> listProduct = new Vector<>();
        if (favoriteProducts != null) {
            for (Favorite_Product favoriteProduct : favoriteProducts) {
                productID = favoriteProduct.getProductID();
                product = dao.getProductByID(Integer.toString(productID));
                listProduct.add(product); // Thêm sản phẩm vào Vector
            }
        } else {
            listProduct = null;
        }
        
        String error = request.getParameter("msg");


        request.setAttribute("product", listProduct);
        request.setAttribute("user", user);
        request.setAttribute("contact", contact);
        request.setAttribute("error", error);
        request.getRequestDispatcher("Profile.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
