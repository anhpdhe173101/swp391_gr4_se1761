/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.User;

import entity.ContactInfo;
import entity.User;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.sql.Date;
import model.UserDAO;

/**
 *
 * @author Phuc Lam
 */
@WebServlet(name = "ProfileServlet", urlPatterns = {"/changeProfile"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 50, // 50MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB
public class ChangeProfileServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Atribute for user
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        int gender = Integer.parseInt(request.getParameter("gender"));

        // Atribute for user contact
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");
        String note = request.getParameter("note");
        String emailContact = request.getParameter("emailContact");
        String nameContact = request.getParameter("nameContact");
        // Get infor of user in database that is not change
        UserDAO d = new UserDAO();
        int id = d.getUserByEmail(email).getId();
        String pass = d.getUserByEmail(email).getPassword();
        int roleID = d.getUserByEmail(email).getRoleID();
        int confirmation = d.getUserByEmail(email).getConfirmation();
        int status = d.getUserByEmail(email).getStatus();
        String imageURL = d.getUserByEmail(email).getImageURL();
        Date date = d.getUserByEmail(email).getDate();
        // Retrieves the file part
        Part filePart = request.getPart("image");

        // Obtains the file name
        String fileName = extractFileName(filePart);

        // Refines the file name
        fileName = new File(fileName).getName();

        // Update user
        if (!fileName.isEmpty()) {
            // Writes the file to disk
            String uploadPath = getFolderUpload().getAbsolutePath();
            filePart.write(uploadPath + File.separator + fileName);
            User unew = new User(id, email, pass, roleID, confirmation, status, name, gender, fileName, date);
            d.updateUser(unew);
        } else {
            User unew = new User(id, email, pass, roleID, confirmation, status, name, gender, imageURL, date);
            d.updateUser(unew);
        }

        // Update contact
        int contactID = d.getContactInfoByUserId(id).getId();
        ContactInfo cnew = new ContactInfo(contactID, nameContact, phone, note, roleID, address, emailContact);
        d.updateContact(cnew);
        String msg = "Change profile successfully";
        response.sendRedirect("userProfile?msg=" + msg);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    // Extracts file name from HTTP header content-disposition
    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    // Defines the upload directory
    public File getFolderUpload() {
        File folderUpload = new File("D:\\SWP391\\SWP_Shop\\SWP391_Project\\web\\img\\user");
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
