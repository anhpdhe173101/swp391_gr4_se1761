/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.User;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import model.UserDAO;

/**
 *
 * @author Phuc Lam
 */
@WebServlet(name="ConfirmPasswordServlet", urlPatterns={"/ConfirmPassword"})
public class ConfirmPasswordServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConfirmPasswordServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConfirmPasswordServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        UserDAO d = new UserDAO();
        String pass = request.getParameter("passnow");
        String email = request.getParameter("email");
        String pass_Md5 = md5Hash(pass);
        // Get current password
        String current_pass = d.getUserByEmail(email).getPassword();
        try {
            
            // Test the pass and current are match
            if (!pass_Md5.equals(current_pass)) {
                request.setAttribute("fault", "Passwords do not match.");
                request.getRequestDispatcher("change_password.jsp").forward(request, response);
                
            // If pass is match, we generate session for current pass    
            } else {
                HttpSession session = request.getSession();
                session.setAttribute("confirm", "Succesfuuly");
                request.getRequestDispatcher("change_password.jsp").forward(request, response);
            }
        } catch (Exception e) {

        }
    } 
    


    private String md5Hash(String input) {
        try {
            // Khởi tạo đối tượng MessageDigest với thuật toán MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            // Chuyển đổi chuỗi đầu vào thành mảng byte
            byte[] inputBytes = input.getBytes();
            // Băm mảng byte và lưu kết quả vào hashedBytes
            byte[] hashedBytes = md.digest(inputBytes);
            // Khởi tạo đối tượng StringBuilder để xây dựng chuỗi hexa của băm
            StringBuilder stringBuilder = new StringBuilder();
            for (byte b : hashedBytes) {
                // Chuyển đổi byte thành chuỗi hexa và nối vào stringBuilder
                stringBuilder.append(String.format("%02x", b));
            }
             // Trả về chuỗi hexa của băm
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
