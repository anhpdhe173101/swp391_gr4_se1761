/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.User;

import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import model.UserDAO;

/**
 *
 * @author Phuc Lam
 */
@WebServlet(name = "ChangePasswordServlet", urlPatterns = {"/change"})
public class ChangePasswordServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangePasswordServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangePasswordServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO d = new UserDAO();
        String pass = request.getParameter("pass");
        String cpass = request.getParameter("cpass");
        String email = request.getParameter("email");
        
        // Set up password conditions
        String uppercaseRegex = ".*[A-Z].*";
        String lowercaseRegex = ".*[a-z].*";
        String specialCharacterRegex = ".*[@#$%^&+=!].*";
        String minLengthRegex = ".{8,}";
        
        // Get old password
        String pre_pass = d.getUserByEmail(email).getPassword();
        try {
            
            // Check the password has at least one uppercase character
            if (!pass.matches(uppercaseRegex)) {
                request.setAttribute("error", "Password must contain at least one uppercase letter.");
                request.getRequestDispatcher("change_password.jsp").forward(request, response);
            
            // Chec the password has at least one lowercase character        
            } else if (!pass.matches(lowercaseRegex)) {
                request.setAttribute("error", "Password must contain at least one lowercase letter.");
                request.getRequestDispatcher("change_password.jsp").forward(request, response);

            // Check the password has at least one special character                 
            } else if (!pass.matches(specialCharacterRegex)) {
                request.setAttribute("error", "Password must contain at least one special character.");
                request.getRequestDispatcher("change_password.jsp").forward(request, response);

            // Check the password has at least 8 characters long
            } else if (!pass.matches(minLengthRegex)) {
                request.setAttribute("error", "Password must be at least 8 characters long.");
                request.getRequestDispatcher("change_password.jsp").forward(request, response);

            // Check the new password don't match with old password
            } else if (pass.equals(pre_pass)) {
                request.setAttribute("error", "Don't use the previous pass.");
                request.getRequestDispatcher("change_password.jsp").forward(request, response);

            // Check password and confinrm password are match
            } else if (!pass.equals(cpass)) {
                request.setAttribute("error", "Passwords do not match.");
                request.getRequestDispatcher("userProfile").forward(request, response);

            } else {
                
                int id = d.getUserByEmail(email).getId();
                int roleID = d.getUserByEmail(email).getRoleID();
                int confirmation = d.getUserByEmail(email).getConfirmation();
                int status = d.getUserByEmail(email).getStatus();
                String fullName = d.getUserByEmail(email).getFullName();
                int gender = d.getUserByEmail(email).getGender();
                String imageURL = d.getUserByEmail(email).getImageURL();
                Date date = d.getUserByEmail(email).getDate();
                User unew = new User(id, email, cpass, roleID, confirmation, status, fullName, gender, imageURL,date);
                
                d.updatePass(unew);
                request.setAttribute("error", "change password successfully");
//                request.getRequestDispatcher("userProfile").forward(request, response);
                request.getRequestDispatcher("change_password.jsp").forward(request, response);
            }
        } catch (Exception e) {

        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
