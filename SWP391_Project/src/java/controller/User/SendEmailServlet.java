package controller.User;
/**
 *
 * @author Phuc Lam
 */
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.UserDAO;

@WebServlet(name = "SendServlet", urlPatterns = {"/send"})
public class SendEmailServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SendServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SendServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("ForgotPassword.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setCharacterEncoding("UTF-8");
        String toEmail = request.getParameter("toEmail");
        
        UserDAO d = new UserDAO();
        User u = d.getUserByEmail(toEmail);
        
        // Check account exist
        if (u == null) {
            
            request.setAttribute("error", "invalid email");
            request.getRequestDispatcher("ForgotPassword.jsp").forward(request, response);
        } else {
            String title = "Do you want to reset password";
            String link = generateLink("http://localhost:8080/SWP391_Project/ResetPassword.jsp", toEmail);

            final String from = "lamtp2810@gmail.com";
            final String password = "rbxtkzwzapslqjef";

            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");

            Authenticator auth = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(from, password);
                }
            };

            Session session = Session.getInstance(props, auth);
            MimeMessage msg = new MimeMessage(session);
            try {
                msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
                msg.setFrom(from);
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
                msg.setSubject(title);
                msg.setText(link, "UTF-8");
                Transport.send(msg);
            } catch (MessagingException e) {
            }
            request.setAttribute("successful", "Check your email to reset password");
            request.getRequestDispatcher("ForgotPassword.jsp").forward(request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private static String generateLink(String originalLink, String userEmail) {
        try {
            long currentTime = System.currentTimeMillis();
            long expirationTime = currentTime + TimeUnit.MINUTES.toMillis(2);

            String signedLink = originalLink + "?email=" + userEmail
                    + "&expiration=" + expirationTime;
            return signedLink;
        } catch (Exception e) {
            return null;
        }
    }
}
