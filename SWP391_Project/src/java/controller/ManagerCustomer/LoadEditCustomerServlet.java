/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.ManagerCustomer;

import entity.AfterChange;
import entity.BeforeChange;
import entity.ContactInfo;
import entity.User;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.sql.Date;
import model.UserDAO;

/**
 *
 * @author Phuc Lam
 */
@WebServlet(name = "LoadEditCustomerServlet", urlPatterns = {"/loadEditCustomer"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 50, // 50MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB
public class LoadEditCustomerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //1.1: Atribute for User
        String id_raw = request.getParameter("id");

        String name_raw = request.getParameter("name");
        int gender_raw = Integer.parseInt(request.getParameter("gender"));

        String role_raw = request.getParameter("role");
        int role;
        if (role_raw != null) {
            role = Integer.parseInt(role_raw);
        } else {
            role = 1;
        }

        //1.2: Atribute for user Contact Infor
        String nameContact = request.getParameter("nameContact");
        String address_raw = request.getParameter("address");
        String phone_raw = request.getParameter("phone");
        String emailContact = request.getParameter("email");
        //1.3: Get EditerID  from session
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        int editer_ID = account.getId();

        //1.4: Get current Customer Infor By Email 
        int id = Integer.parseInt(id_raw);
        UserDAO dao = new UserDAO();
        User user = dao.getUserByUserID(id);

        String email = user.getEmail();
        String pass = user.getPassword();
        int confirmation = user.getConfirmation();
        int status = user.getStatus();
        String name = user.getFullName();
        int gender = user.getGender();
        String imageURL = user.getImageURL();
        Date date = user.getDate();
        //1.5: Get all Contact Infor by UserID
        int user_id = user.getId();
        ContactInfo contactInfo = dao.getContactInfoByUserId(user_id);
        int contact_ID = contactInfo.getId();
        String phone = contactInfo.getPhone();
        String note = contactInfo.getNote();
        String address = contactInfo.getAddress();
//        String emailContact = contactInfo.getEmail();

        //2.1:Variables for History Record
        String afterName, beforeName;
        String afterPhone, beforePhone;
        String afterAddress, beforeAddress;
        int afterGender, beforeGender;

        //2.2: Variables store the current value
        String currentName, currentPhone, currentAddress;
        int currentGender;

        //2.3: Check the parameters from request what is diffirent with their in database
        //2.3.1: If there is a change for name
        if (!name_raw.equals(name)) {
            afterName = name_raw;   // Get value from the request
            beforeName = name;      // Get value form the current name from database
            currentName = name_raw; //Update the new value
        } else {
            afterName = null;
            beforeName = null;
            currentName = name;
        }

        if (gender_raw != gender) {
            afterGender = gender_raw;
            beforeGender = gender;
            currentGender = gender_raw;
        } else {
            afterGender = 0;
            beforeGender = 0;
            currentGender = gender;
        }

        if (!phone_raw.equals(phone)) {
            afterPhone = phone_raw;
            beforePhone = phone;
            currentPhone = phone_raw;
        } else {
            afterPhone = null;
            beforePhone = null;
            currentPhone = phone;
        }

        if (!address_raw.equals(address)) {
            afterAddress = address_raw;
            beforeAddress = address;
            currentAddress = address_raw;
        } else {
            afterAddress = null;
            beforeAddress = null;
            currentAddress = address;
        }
        //Check at least one attribute change
        if (!name_raw.equals(name) || gender_raw != gender || !phone_raw.equals(phone) || !address_raw.equals(address)) {
            //3.1: Insert the old feild in Before Change
            BeforeChange before = new BeforeChange(0, beforeName, beforeGender, beforePhone, beforeAddress);
            dao.insertBeforeChange(before);

            //3.2: Insert the new feild in After Change
            AfterChange after = new AfterChange(0, afterName, afterGender, afterPhone, afterAddress);
            dao.insertAfterChange(after);

            //3.3: Get the latest id of BeforeChange and AfterChange record
            int beforeID = dao.getLastBeforeChange().getbID();
            int afterID = dao.getLastAfterChange().getaID();

            //3.4: Insert History table
            dao.insertHistory(user_id, editer_ID, afterID, beforeID, null);
        }

        // Retrieves the file part
        Part filePart = request.getPart("image");

        // Obtains the file name
        String fileName = extractFileName(filePart);

        // Refines the file name
        fileName = new File(fileName).getName();

        if (!fileName.isEmpty()) {
            // Writes the file to disk
            String uploadPath = getFolderUpload().getAbsolutePath();
            filePart.write(uploadPath + File.separator + fileName);

            //3.5: Update User table 
            User unew = new User(id, email, pass, role, confirmation, status, currentName, currentGender, fileName, date);
            dao.updateUser(unew);
        } else {
            //3.5: Update User table 
            User unew = new User(id, email, pass, role, confirmation, status, currentName, currentGender, imageURL, date);
            dao.updateUser(unew);
        }

        //3.6: Update Contact Infor table
        ContactInfo cnew = new ContactInfo(contact_ID, nameContact, currentPhone, note, user_id, currentAddress, emailContact);
        dao.updateContact(cnew);

        //Navigate to editCustomer
//        request.setAttribute("msg", "Edit successfully");
        String msg = "Edit successfully";
        response.sendRedirect("editCustomer?cid=" + id + "&msg=" + msg);
    }

    // Extracts file name from HTTP header content-disposition
    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    // Defines the upload directory
    public File getFolderUpload() {
        File folderUpload = new File("D:\\SWP391\\SWP_Shop\\SWP391_Project\\web\\img\\user");
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
