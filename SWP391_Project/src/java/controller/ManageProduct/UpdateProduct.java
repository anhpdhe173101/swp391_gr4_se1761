    /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.ManageProduct;

import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import model.ProductDAO;

/**
 *
 * @author Duy Anh
 */
@WebServlet(name = "UpdateProduct", urlPatterns = {"/UpdateProduct"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
                 maxFileSize = 1024 * 1024 * 50,      // 50MB
                 maxRequestSize = 1024 * 1024 * 50)   // 50MB
public class UpdateProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String name = request.getParameter("name");
        String cateID = request.getParameter("cateID");
        String origin_price = request.getParameter("origin_price");
        String description = request.getParameter("description");
        String original_image = request.getParameter("original_image");
        String status = request.getParameter("status");
        String id = request.getParameter("id");
        String sale_price = request.getParameter("sale_price");
        String quantity = request.getParameter("quantity");
        String date = request.getParameter("date");
       
        
        ProductDAO dao = new ProductDAO();
        // Retrieves the file part
        Part filePart = request.getPart("image");
        System.out.println(filePart);

// Obtains the file name
        String fileName = "";

// If filePart is not null, extract the file name
       
            fileName = extractFileName(filePart);

            // Refines the file name
            fileName = new File(fileName).getName();

            // Writes the file to disk
            if (!fileName.isEmpty()) {
                // Ghi file ảnh tải lên vào thư mục upload
                String uploadPath = getFolderUpload().getAbsolutePath();
                filePart.write(uploadPath + File.separator + fileName);
                 dao.editProduct(name, cateID, origin_price, description, fileName, status, id);
            }
           
         else {
            // If filePart is null, use the original image name
            
            dao.editProduct(name, cateID, origin_price, description, original_image, status, id);
        }
        
        
        
       
        
        dao.UpdateSale(sale_price, id);
        if (quantity.compareTo("0") > 0) {
            dao.insertQuantity(id, quantity, null);
        }

        System.out.println("Value: " + cateID);

//        response.sendRedirect("ManageProduct");
        String msg="Edit product have id "+ id+ " successfully" ;
        response.sendRedirect("ManageProduct?msg=" + msg);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    // Defines the upload directory
    public File getFolderUpload() {
        File folderUpload = new File("D:\\SWP_PROJECT\\swp\\SWP391_Project\\web\\img\\product_image");
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }
}

    
