/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.Blog;

import entity.Blog;
import entity.BlogCategory;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.BlogCategoryDAO;
import model.BlogDAO;

/**
 *
 * @author Acer
 */
@WebServlet(name="BlogSearchController", urlPatterns={"/blogSearch"})
public class BlogSearchController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String key = request.getParameter("key");
        if(key==null){
            key="";
        }
        BlogDAO bd = new BlogDAO();
        List<Blog> list = bd.searchByTitle(key);
        BlogCategoryDAO bcd = new BlogCategoryDAO();
        List<Blog> newBlog = bd.getTop3Newest();
        List<BlogCategory> all = bcd.getAll();
        
        int page, numperpage=4;
        int size = list.size();
        int num = (size%4==0?(size/4):((size/4)+1));
        String xpage = request.getParameter("page");
        if(xpage==null) {
            page=1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page-1)*4;
        end = Math.min(page*numperpage, size);
        List<Blog> list1 = bd.getListByPage(list, start, end);
        
        request.setAttribute("data", list1);
        request.setAttribute("page", page);
        request.setAttribute("num", num);
        request.setAttribute("key", key);
        request.setAttribute("newblog", newBlog);
        request.setAttribute("allcate", all);
        request.getRequestDispatcher("blog.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}