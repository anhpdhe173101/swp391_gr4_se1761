/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Cart;

import entity.Cart;
import entity.Item;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.ProductDAO;

/**
 *
 * @author Phuc Lam
 */
@WebServlet(name="ChangeQuantityServlet", urlPatterns={"/ChangeQuantity"})
public class ChangeQuantityServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangeQuantityServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangeQuantityServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        ProductDAO dal = new ProductDAO();
        List<Product> list = dal.getAllProduct();
        Cookie[] arr = request.getCookies();
        //Get Value from cart cookies
        String txt = "";
        if (arr != null) {
            for (Cookie o : arr) {
                if (o.getName().equals("cart")) {
                    txt += o.getValue();
                    o.setMaxAge(0);
                    response.addCookie(o);
                }
            }
        }
        
        Cart cart = new Cart(txt, list);
        String number_raw = request.getParameter("num");
        String id_raw = request.getParameter("id");
        int id, num;
        
        try {
            id = Integer.parseInt(id_raw);
            Product p = dal.getProductByID(id_raw);
            int numStore = p.getQuantity();
            num = Integer.parseInt(number_raw);
            //If product quanity in cart is 1,
            //when we reduce amount, we remove that product from cart
            if (num == -1 && (cart.getquantityById(id) <= 1)) {
                cart.removeItem(id);
            } else {
                //Check if quanity in cart larger than Quanity in Store we set num = 0
                if (num == 1 && cart.getquantityById(id) >= numStore) {
                    num = 0;
                }
                double price = p.getSale_price();
                Item t = new Item(p, num, price);
                //Update quanity of product in cart
                cart.addItem(t);
            }
        } catch (NumberFormatException e) {
        }
        List<Item> items = cart.getItems();
        txt = "";
        //check item exist in Cart or not
        if (!items.isEmpty()) {
            //get the first product store in cart
            txt = items.get(0).getProduct().getId() + ":"
                    + items.get(0).getQuantity();
            // repeat from the second item because "-" are 
            //used to separate the item comes first
            for (int i = 1; i < items.size(); i++) {
                txt += "-"+items.get(i).getProduct().getId() + ":"
                        + items.get(i).getQuantity();
            }
        }
        Cookie c = new Cookie("cart", txt);
        c.setMaxAge(2 * 24 * 60 * 60);
        response.addCookie(c);
        request.setAttribute("cart", cart);
        request.getRequestDispatcher("shopping_cart.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
