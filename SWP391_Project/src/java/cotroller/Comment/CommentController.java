/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package cotroller.Comment;

import entity.Comment;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.CommentDAO;
import model.UserDAO;

/**
 *
 * @author Acer
 */
@WebServlet(name="CommentController", urlPatterns={"/comment"})
public class CommentController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        CommentDAO cd = new CommentDAO();

        HttpSession session = request.getSession();
        UserDAO ud = new UserDAO();
        User account = (User) session.getAttribute("account");
        if (account == null) {
            //if account don't exist or password wrong, return a message for user
            request.setAttribute("Error", "Please login to comment!");
            request.getRequestDispatcher("home").forward(request, response);
        } else {
            //Add new comment:
            String em = account.getEmail();
            String pa = account.getPassword();
            int postUserID = (int) ud.getUserID(em, pa);
            String commentContent = request.getParameter("commentContent");
            String blogID_raw = request.getParameter("blogID");
            int blogID;
            try {
                blogID = Integer.parseInt(blogID_raw);
                cd.addNewComment(postUserID, blogID, commentContent);
                request.getRequestDispatcher("blogDetail?bid="+blogID).forward(request, response);
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
