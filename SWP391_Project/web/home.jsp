<%-- 
    Document   : home
    Created on : Jan 13, 2024, 4:46:23 PM
    Author     : Acer
--%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.Product" %>
<%@page import="entity.Category" %>
<%@page import="java.util.Vector" %>
<!DOCTYPE html>
<html lang="zxx">
    <!-- For home page: 
    - Show sliders, hot posts, featured products + the sider with the latest posts, static contacts/links
    - Shown slider information includes its image and title; the user is redirected to the slider's backlink on his/her clicking
    - Shown post information includes its thumbnail, title, brief-info; the user is redirected to the post's details on his/her clicking
    - Shown product information includes its thumbnail, title, brief information; the user is redirected to the product's details on his/her clicking -->
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Home</title> 
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <jsp:useBean id="i" class="model.SliderDAO" scope="request"/>
        <jsp:useBean id="t" class="model.ProductDAO" scope="request"/>
    </head>

    <body>

        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Header Section Begin -->
        <%@ include file="header.jsp" %>
        <!-- Header Section End -->


        <section class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="hero__categories">
                            <div class="hero__categories__all">
                                <i class="fa fa-bars"></i>
                                <span>All categories</span>
                            </div>
                            <!--Duy Anh-->
                            <ul>
                                <c:forEach items="${requestScope.listC}" var="c">
                                    <li><a href="category?id=${c.cateID}">${c.name}</a></li>
                                </c:forEach>
                            </ul>
                            <!--Duy Anh-->
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="search">
                                    <input type="text" name="txt" placeholder="What do you need?">
                                    <button type="submit" class="site-btn">SEARCH</button>
                                </form>
                            </div>
                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>+84 81 661 3231</h5>
                                    <span>Support 24/7 time</span>
                                </div>
                            </div>
                        </div>
                        <div class="hero__item" style="background-image: url('img/background.jpg');">
                            <div class="slide-container" style="width: 100%;height: 100%;object-fit: cover;">
                                <c:forEach items="${requestScope.slider}" var="s">
                                    <c:set var="id" value="${s.id}"/> 
                                    <div class="slide">
                                        <a href="shop">
                                            <img  src="img/${s.image}">
                                        </a>
                                    </div>
                                </c:forEach>
                                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                                <a class="next" onclick="plusSlides(1)">&#10095;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Hero Section End -->
        <!--Hoang Anh-->

        <!--Hoang Anh-->
        <!-- Latest Product Section Begin -->
        <section class="latest-product spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title from-blog__title">
                            <h2>Latest product</h2>
                            
                        </div>
                        <h4 style="color: red; text-align: center">${requestScope.msg}</h4>
                    </div>
                </div>
                <div class="row">
                    <c:forEach items="${requestScope.product}" var="p">
                        <c:set var="id" value="${p.id}"/>
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="img/product_image/${p.image}">
                                    <ul class="product__item__pic__hover">
                                        <c:if test="${sessionScope.account.roleID eq 1}">
                                            <li onclick="doAdd('${p.id}')"><a ><i class="fa fa-heart"></i></a></li>
                                        </c:if>
                                        <li><a href="addToCart?id=${p.id}&num=1" ><i class="fa fa-shopping-cart" ></i></a></li>
                                    </ul>
                                </div>
                                <div class="product__item__text">
                                    <h6><a href="./product_detail.jsp">${p.name}</a></h6>
                                    <h5>${p.origin_price} USD</h5>
                                    <h5>${p.sale_price} USD</h5>
                                </div>
                            </div>
                        </div>
                    </c:forEach> 
                </div>
                <div class="text-center">
                    <a href="shop" class="see">
                        <span style="background-color: #6B8E23; color: white; padding: 0 5px;">See more</span>
                    </a>
                </div>
            </div>
        </section>
        <!-- Latest Product Section End -->
        <!--Hoang Anh-->

        <!--Hoang Anh-->
        <!-- Blog Section Begin -->
        <section class="from-blog spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title from-blog__title">
                            <h2>From The Blog</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <c:forEach items="${requestScope.blog}" var="n">
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="blog__item">
                                <div class="blog__item__pic">
                                    <img style="width: 200px; height: 200px" src="img/blog/${n.image}" alt="">
                                </div>
                                <div class="blog__item__text">
                                    <ul>
                                        <li ><i class="fa fa-calendar-o"></i>${n.datePosted}</li>
                                    </ul>
                                    <h5><a href="blogDetail?bid=${n.id}">${n.title}</a></h5>
                                    <p>${n.opening} </p>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <div class="text-center">
                    <a href="blog" class="see">
                        <span style="background-color: #6B8E23; color: white; padding: 0 5px;">See more</span>
                    </a>
                </div>
            </div>
        </section>
        <!-- Blog Section End -->
        <!--Hoang Anh-->

        <!--Hoang Anh-->
        <!-- Footer Section Begin -->
        <%@ include file="footer.jsp" %>
        <!-- Footer Section End -->
        <!--Hoang Anh-->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
        <script>
                                        $(document).ready(function () {
                                            $('.slider').slick({
                                                autoplay: true,
                                                autoplaySpeed: 3000
                                            });
                                        });
        </script>

        <!--Hoang Anh-->
        <!-- This script controls a slideshow -->
        <script>
            var slideIndex = 1;
            showSlides(slideIndex);

            function plusSlides(n) {
                showSlides(slideIndex += n);
            }

            function showSlides(n) {
                var i;
                var slides = document.getElementsByClassName("slide");
                if (n > slides.length) {
                    slideIndex = 1
                }
                if (n < 1) {
                    slideIndex = slides.length
                }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                    slides[i].style.opacity = 0;
                }
                slides[slideIndex - 1].style.display = "block";
                setTimeout(function () {
                    slides[slideIndex - 1].style.opacity = 1;
                }, 50);
            }
        </script>
        <!--Hoang Anh-->
        
       <script type="text/javascript">
            function  doAdd(id) {
                if (confirm('ban chac co muon add product nay vao danh sach yeu thich khong')) {
                    window.location = 'addFavoriteProduct?id=' + id;
                }
            }
        </script>

    </body>

</html>