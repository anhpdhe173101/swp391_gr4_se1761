<%-- 
    Document   : ManageProduct
    Created on : Mar 12, 2024, 10:06:55 AM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Tables - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
        <style>
            /* Custom CSS for table image alignment */
            #datatablesSimple tbody td img {
                max-width: 100px; /* Set a maximum width for the image */
                height: auto; /* Maintain the aspect ratio of the image */
                display: block; /* Ensure the image is displayed as a block element */
                margin: 0 auto; /* Center the image horizontally within the cell */
            }
        </style>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="manageCustomer">Marketing</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">

                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i> ${sessionScope.account.fullName}</a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="addNewCustomer.jsp">Add New Customer</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Menu</div>
                            <a class="nav-link" href="UserChartByWeek">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Customer 
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="manageCustomer">Customer Tables</a>
                                    <a class="nav-link" href="addNewCustomer.jsp">Add New Customer</a>
                                </nav>
                            </div>



                            <a class="nav-link" href="postList">
                                <div class="sb-nav-link-icon"><i class="fa-regular fa-address-card"></i></div>
                                Post Management

                            </a>

                            <a class="nav-link" href="manageSlider">
                                <div class="sb-nav-link-icon"><i class="fa-solid fa-sliders"></i></div>
                                Slider Management

                            </a>
                            <a class="nav-link" href="ManageProduct">
                                <div class="sb-nav-link-icon"><i class="fa-brands fa-product-hunt"></i></div>
                                Product Management

                            </a>

                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Marketing
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Edit Product</h1>
                        <ol class="breadcrumb mb-4">

                        </ol>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Edit Product
                            </div>
                            <div class="card-body">
                                <form action="UpdateProduct" method="post" enctype="multipart/form-data" onsubmit="return validatePrices()">
                                    <div class="modal-body">
                                        <div class="row"> 
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ID</label>
                                                    <input value="${detail.id}" name="id" type="text" class="form-control" readonly required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input value="${detail.name}" name="name" type="text" class="form-control" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Origin Price</label>
                                                    <input id="origin_price" value="${detail.origin_price}" name="origin_price" type="number" step="0.01" class="form-control" required min="0">
                                                </div>
                                                <div class="form-group">
                                                    <label>Sale Price</label>
                                                    <input id="sale_price" value="${detail.sale_price}" name="sale_price" type="number" step="0.01" class="form-control" required min="0">
                                                </div>
                                                <div class="form-group">
                                                    <label>Category</label>
                                                    <select name="cateID" class="form-select" aria-label="Default select example">
                                                        <c:forEach items="${listCC}" var="category">
                                                            <option value="${category.cateID}" ${category.cateID eq detail.cateID ? 'selected' : ''}>
                                                                ${category.name}
                                                            </option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Image</label>

                                                    <input type="hidden" name="original_image" value="${detail.image}">

                                                    <input name="image" type="file" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Quantity(Current in Stock now is ${detail.quantity} )</label>
                                                    <input name="quantity" type="number" class="form-control"  min="0">
                                                </div>

                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <input value="${detail.description}" name="description" type="text" class="form-control" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <select name="status" class="form-select" aria-label="Default select example">
                                                        <option value="1" ${detail.status eq 1 ? 'selected' : ''}>Show</option>
                                                        <option value="0" ${detail.status eq 0 ? 'selected' : ''}>Hide</option>
                                                    </select>
                                                    <br>
                                                    <div class="modal-footer">
                                                        <input type="submit" name="submit" class="btn btn-success" value="Save">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                                </main>
                                <footer class="py-4 bg-light mt-auto">
                                    <div class="container-fluid px-4">
                                        <div class="d-flex align-items-center justify-content-between small">
                                            <div class="text-muted">Copyright &copy; Your Website 2023</div>
                                            <div>
                                                <a href="#">Privacy Policy</a>
                                                &middot;
                                                <a href="#">Terms &amp; Conditions</a>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
                        <script src="js/scripts.js"></script>
                        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
                        <script src="js/datatables-simple-demo.js"></script>
                        <script>
                                    function validatePrices() {
                                        var originPrice = parseFloat(document.getElementById('origin_price').value);
                                        var salePrice = parseFloat(document.getElementById('sale_price').value);

                                        if (salePrice > originPrice) {
                                            alert("Sale price not higher than Origin Price");
                                            return false; // Ngăn không cho form được gửi đi
                                        }
                                        return true; // Cho phép form được gửi đi
                                    }
                        </script>
                        </body>

                        </html>

