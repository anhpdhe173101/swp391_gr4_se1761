<%-- 
   Document   : shop
   Created on : Jan 13, 2024, 4:46:38 PM
   Author     : Acer
--%>
<%@page import="java.util.Vector" %>
<%@page import="entity.Product" %>
<%@page import="entity.Category" %>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Shop</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>

    <body>

        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>


        <!-- Header Section Begin -->
        <%@ include file="header.jsp" %>
        <!-- Header Section End -->



        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-section set-bg" data-setbg="img/shop.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="breadcrumb__text">
                            <h2>SWP391.S</h2>
                            <div class="breadcrumb__option">
                                <span>Shop with us today</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- DuyAnh -->
        <!-- Product Section Begin -->
        <section class="product spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-5">
                        <div class="sidebar">
                            <!-- search box -->
                            <form action="searching" class="form-inline my-2 my-lg-0">
                                <div class="input-group input-group-sm">
                                    <input  name="txt" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Search...">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-secondary btn-number">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <!-- list category -->
                            <div class="hero__categories">
                                <div class="hero__categories__all">
                                    <i class="fa fa-bars"></i>
                                    <span>All categories</span>
                                </div>
                                <!--Duy Anh-->
                                <ul>
                                    <c:forEach items="${requestScope.listC}" var="c">
                                        <li><a href="category?id=${c.cateID}">${c.name}</a></li>
                                        </c:forEach>
                                </ul>
                                <!--Duy Anh-->
                            </div>
                            <br>

                            <!-- latest product -->
                            <div class="sidebar__item">
                                <div class="latest-product__text">
                                    <h4>Latest Products</h4>
                                    <div class="product__item">
                                        <div class="product__item__pic set-bg" data-setbg="img/product_image/${latest.image}">

                                        </div>
                                        <div class="product__item__text">
                                            <div class="product__discount__item__text">
                                                <h5><a href="./product_detail.jsp">${latest.name}</a></h5>
                                                <div class="product__item__price">${latest.sale_price} <span>${latest.origin_price}</span></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <ul>
                                <li>Phone: +84 81 661 3231</li>
                                <li>Email: hello@project.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-7">
                        <!-- list product -->
                        <div class="row">
                            <c:forEach var="o" items="${requestScope.listP}">
                                <div class="col-lg-4 col-md-6 col-sm-6">
                                    <div class="product__item">
                                        <div class="product__item__pic set-bg" data-setbg="img/product_image/${o.image}">

                                        </div>
                                        <div class="product__item__text">
                                            <div class="product__discount__item__text">
                                                <h5><a href="productDetail?id=${o.id}">${o.name}</a></h5>
                                                <p class="card-text show_txt">${o.description}</p>
                                                <div class="product__item__price">${o.sale_price} <span>${o.origin_price}</span></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <c:choose>
                                                <c:when test="${o.quantity != 0}">
                                                    <div class="col">
                                                        <a href="addToCart?id=${o.id}&num=1"" class="btn btn-success btn-block">Add to cart</a>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="col">
                                                        <a href="#" class="btn btn-success btn-block">Sold Out</a>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="product__pagination blog__pagination" style="display: flex; justify-content: center;">
                            <c:set var="page" value="${requestScope.page}" />
                            <div class="pagination">
                                <c:if test="${page gt 1}">
                                    <li class="page-item" style="margin-right: 16px;">
                                        <a class="page-link" href="shopp?page=${page-1}" style="display: flex; justify-content: center; align-items: center;">
                                            <i class="fa fa-long-arrow-left"></i>
                                        </a>
                                    </li>
                                </c:if>
                                <c:forEach begin="${1}" end="${requestScope.num}" var="i">
                                    <a href="shopp?page=${i}" class="${i==page?"active":""}">${i}</a>
                                </c:forEach>
                                <c:if test="${page lt requestScope.num}">
                                    <li class="page-item">
                                        <a class="page-link" href="shopp?page=${page+1}" style="display: flex; justify-content: center; align-items: center;">
                                            <i class="fa fa-long-arrow-right"></i>
                                        </a>
                                    </li>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Product Section End -->

        <!-- DuyAnh -->
        <!-- Footer Section Begin -->
        <%@ include file="footer.jsp" %>
        <!-- Footer Section End -->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>

        <!--LamTP-->
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                document.querySelector("#login-popup").addEventListener("click", function () {
                    document.querySelector(".popup").classList.add("active");
                });

                document.querySelector(".popup .close-btn").addEventListener("click", function () {
                    document.querySelector(".popup").classList.remove("active");
                });
            });
        </script>
        <!--LamTP-->

    </body>

</html>
