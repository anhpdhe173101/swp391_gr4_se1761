<%-- 
    Document   : CustomerTable
    Created on : Feb 28, 2024, 2:28:47 PM
    Author     : Phuc Lam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Tables - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="userProfile">Profile</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">

                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i> ${sessionScope.account.fullName}</a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="home">Home</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">

                            <div class="sb-sidenav-menu-heading">Menu</div>
                            <a class="nav-link" href="userProfile">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Profile

                            </a>                         
                            <a class="nav-link" href="change_password.jsp">
                                <div class="sb-nav-link-icon">
                                    <i class="fa-solid fa-pen-to-square"></i>                           
                                </div>
                                Change Password
                            </a>
                            <a class="nav-link" href="orderHistory">
                                <div class="sb-nav-link-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                Order History
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Customer
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                                                <h1 class="mt-4">Profile</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="orderHistory">Order History</a> </li>
                            <li class="breadcrumb-item active">Order Information</li>

                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">

                              <table class="table">
                                    <thead>
                                        <tr>
                                            <td colspan="4" style="text-align: center; font-weight: bold; font-size: larger; border: 1px solid black; color: red"> Order Info</td>
                                        </tr>
                                        <tr>
                                            <th style="border: 1px solid black;">Order ID</th>
                                            <th style="border: 1px solid black;">Date</th>
                                            <th style="border: 1px solid black;">Total</th>
                                            <th style="border: 1px solid black;">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="border: 1px solid black;">${o.id}</td>
                                            <td style="border: 1px solid black;">${o.order_date}</td>
                                            <td style="border: 1px solid black;">${o.total}</td>
                                            <td style="border: 1px solid black;">${o.status}</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table" style="margin-top: 50px;">
                                    <thead>
                                        <tr>
                                            <td colspan="4" style="text-align: center; font-weight: bold; font-size: larger; border: 1px solid black; color: blue"> Contact Info</td>
                                        </tr>
                                        <tr>
                                            <th style="border: 1px solid black;">Customer Name</th>
                                            <th style="border: 1px solid black;">Email</th>
                                            <th style="border: 1px solid black;">Phone</th>
                                            <th style="border: 1px solid black;">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="border: 1px solid black;">${o.name}</td>
                                            <td style="border: 1px solid black;">${o.email}</td>
                                            <td style="border: 1px solid black;">${o.phone}</td>
                                            <td style="border: 1px solid black;">${o.address}</td>
                                        </tr>
                                    </tbody>    
                                </table>
                                .
                            </div>
                        </div>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Order Information Table
                            </div>
                            <div class="card-body">
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Image</th>
                                            <th>Product</th>
                                            <th>Category</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Image</th>
                                            <th>Product</th>
                                            <th>Category</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <c:forEach items="${list}" var="o">
                                            <tr>                                
                                                <td>
                                                   ${o.id}
                                                </td>
                                                <td>
                                                    <img src="img/product_image/${o.image}"style=" height: 150px; width: 150px;">
                                                </td>
                                                <td>${o.name}</td>
                                                <td>${o.category}</td>
                                                <td>${o.price}</td>
                                                <td>${o.quantity}</td>
                                                <td>${o.amount}</td>
                                                <td  > <div onclick="doAdd('${o.id}')"><i class="fa-solid fa-cart-shopping"></i> </div></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Shopping &copy; Website 2024</div>

                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>

    </body>
    
           <script type="text/javascript">
            function  doAdd(id) {
                if (confirm('Do you want to buy this product again')) {
                    window.location = 'addToCart?id=' + id +'&num=1';
                }
            }
        </script>
</html>

