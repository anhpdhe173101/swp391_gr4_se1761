<%-- 
    Document   : ManageProduct
    Created on : Mar 12, 2024, 10:06:55 AM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Tables - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
        <style>
            /* Custom CSS for table image alignment */
            #datatablesSimple tbody td img {
                max-width: 100px; /* Set a maximum width for the image */
                height: auto; /* Maintain the aspect ratio of the image */
                display: block; /* Ensure the image is displayed as a block element */
                margin: 0 auto; /* Center the image horizontally within the cell */
            }


            .gallery-wrap .img-big-wrap img {
                height: 400px;
                width: auto;
                display: inline-block;
                cursor: zoom-in;
            }


            .gallery-wrap .img-small-wrap .item-gallery {
                width: 60px;
                height: 60px;
                border: 1px solid #ddd;
                margin: 7px 2px;
                display: inline-block;
                overflow: hidden;
            }

            .gallery-wrap .img-small-wrap {
                text-align: center;
            }
            .gallery-wrap .img-small-wrap img {
                max-width: 70%;
                max-height: 70%;
                object-fit: cover;
                border-radius: 4px;
                cursor: zoom-in;
            }
            .img-big-wrap img{
                width: 70% !important;
                height: auto !important;
            }
            .filter-form,
            .form-inline {
                display: inline-block;
                vertical-align: middle;
            }
        </style>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="manageCustomer">Marketing</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">

                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i> ${sessionScope.account.fullName}</a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="userProfile">Profile</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="logout">Logout</a></li>>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Menu</div>
                            <a class="nav-link" href="UserChartByWeek">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Customer 
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="manageCustomer">Customer Tables</a>
                                    <a class="nav-link" href="addNewCustomer.jsp">Add New Customer</a>
                                </nav>
                            </div>



                            <a class="nav-link" href="postList">
                                <div class="sb-nav-link-icon"><i class="fa-regular fa-address-card"></i></div>
                                Post Management

                            </a>

                            <a class="nav-link" href="manageSlider">
                                <div class="sb-nav-link-icon"><i class="fa-solid fa-sliders"></i></div>
                                Slider Management

                            </a>
                            <a class="nav-link" href="ManageProduct">
                                <div class="sb-nav-link-icon"><i class="fa-brands fa-product-hunt"></i></div>
                                Product Management

                            </a>

                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Marketing
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <br>
                        <div>
                            <form action="FilterProduct" method="post" class="filter-form">
                                <label for="category-filter" style="color: black;">Category:</label>
                                <select id="category-filter" name="cid">
                                    <option value="">All</option>
                                    <c:forEach items="${listCC}" var="category">
                                        <option value="${category.cateID}">${category.name}</option>
                                    </c:forEach>
                                </select>
                                <label for="status-filter" style="color: black;">Status:</label>
                                <select id="status-filter" name="status">
                                    <option value="">All</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                                <button type="submit" class="btn btn-secondary btn-number">Filter</button>
                            </form>

                            <form action="searchName" method="post" class="form-inline my-2 my-lg-0">
                                <div class="input-group input-group-sm">
                                    <input name="txt" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Search...">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-secondary btn-number">
                                            Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        </form>
                    </div>
                    <ol class="breadcrumb mb-4">

                    </ol>

                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table me-1"></i>
                            Product Detail
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="row">
                                    <aside class="col-sm-5 border-right">
                                        <article class="gallery-wrap"> 
                                            <div class="img-big-wrap">
                                                <div> <a href="#"><img src="img/product_image/${detail.image}"></a></div>
                                            </div> 
                                            <div class="img-small-wrap">

                                            </div> 
                                        </article> 
                                    </aside>
                                    <aside class="col-sm-7">
                                        <article class="card-body p-5">
                                            <h3 class="title mb-3">${detail.name}</h3>

                                            <p class="price-detail-wrap"> 
                                                <span class="price h3 text-warning"> 
                                                    <span class="currency">$</span><span class="num">${detail.sale_price}   </span><del style="color: black;"><span class="currency"style="color: black;">          $</span><span class="num" style="color: black;">${detail.origin_price}</span></del>
                                                </span> 

                                            </p> 
                                            <dl class="item-property">
                                                <dt>Description</dt>
                                                <dd><p>${detail.description}</p></dd>
                                            </dl>


                                            <hr>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <dl class="param param-inline">
                                                        <dt>Quantity: ${detail.quantity}</dt>

                                                    </dl>  
                                                </div>

                                            </div> 
                                            <hr>
                                            <!-- link to load data for update -->
                                            <a href="LoadProduct?pid=${detail.id}" class="btn btn-lg btn-primary text-uppercase"> Update Detail </a>

                                        </article> 
                                    </aside> 
                                </div>
                            </div>
                            </main>
                            <footer class="py-4 bg-light mt-auto">
                                <div class="container-fluid px-4">
                                    <div class="d-flex align-items-center justify-content-between small">
                                        <div class="text-muted">Copyright &copy; Your Website 2023</div>
                                        <div>
                                            <a href="#">Privacy Policy</a>
                                            &middot;
                                            <a href="#">Terms &amp; Conditions</a>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
                    <script src="js/scripts.js"></script>
                    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
                    <script src="js/datatables-simple-demo.js"></script>
                    </body>
                    </html>

