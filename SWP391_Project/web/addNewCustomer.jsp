<%-- 
    Document   : CustomerTable
    Created on : Feb 28, 2024, 2:28:47 PM
    Author     : Phuc Lam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Tables - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->

            <!-- Check if role=4 show Admin, role = 3 show Marketing-->
            <c:if test="${sessionScope.account.roleID eq 4}">
                <a class="navbar-brand ps-3" href="manageCustomer">Admin</a>
            </c:if>
            <c:if test="${sessionScope.account.roleID eq 3}">
                <a class="navbar-brand ps-3" href="manageCustomer">Marketing</a>
            </c:if>

            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">

                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i> ${sessionScope.account.fullName}</a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="userProfile">Profile</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Menu</div>


                            <c:if test="${sessionScope.account.roleID eq 3}">
                                <a class="nav-link" href="UserChartByWeek">
                                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                    Dashboard
                                </a>
                            </c:if>



                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                    <c:if test="${sessionScope.account.roleID eq 3}">
                                    Customer 
                                </c:if>

                                <c:if test="${sessionScope.account.roleID eq 4}">
                                    User 
                                </c:if>
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <c:if test="${sessionScope.account.roleID eq 3}">
                                        <a class="nav-link" href="manageCustomer">                                                                            
                                            Customer Tables                                                                              
                                        </a>                                
                                        <a class="nav-link" href="addNewCustomer.jsp">Add New Customer</a>
                                    </c:if>

                                    <c:if test="${sessionScope.account.roleID eq 4}">
                                        <a class="nav-link" href="manageCustomer">
                                            User Tables
                                        </a>
                                        <a class="nav-link" href="addNewCustomer.jsp">Add New User</a>
                                    </c:if>
                                </nav>
                            </div>

                            <c:if test="${sessionScope.account.roleID eq 3}">
                                <a class="nav-link" href="postList">
                                    <div class="sb-nav-link-icon"><i class="fa-regular fa-address-card"></i></div>
                                    Post Management
                                </a>
                                <a class="nav-link" href="manageSlider">
                                    <div class="sb-nav-link-icon"><i class="fa-solid fa-sliders"></i></div>
                                    Slider Management
                                </a>
                                <a class="nav-link" href="ManageProduct">
                                    <div class="sb-nav-link-icon"><i class="fa-brands fa-product-hunt"></i></div>
                                    Product Management
                                </a>
                            </c:if>

                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        <c:if test="${sessionScope.account.roleID eq 4}">
                            Admin
                        </c:if>

                        <c:if test="${sessionScope.account.roleID eq 3}">
                            Marketing
                        </c:if>

                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <c:if test="${sessionScope.account.roleID eq 4}">
                            <h1 class="mt-4">Admin</h1> 
                        </c:if>
                        <c:if test="${sessionScope.account.roleID eq 3}">
                            <h1 class="mt-4">Marketing</h1> 
                        </c:if>  

                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item">
                                <c:if test="${sessionScope.account.roleID eq 4}">
                                    <a href="manageCustomer">User</a>
                                </c:if>
                                <c:if test="${sessionScope.account.roleID eq 3}">
                                    <a href="UserChartByDay">Dashboard</a>
                                </c:if>
                            </li>
                            <li class="breadcrumb-item active">Add New</li>
                        </ol>

                        <div class="card mb-4">
                            <div class="card-body">
                                <form action="register" method="post">
                                    <h5 id="errorMessage" style="color: red"> ${requestScope.error}</h5>
                                    <c:if test="${sessionScope.account.roleID eq 3}">
                                        <div class="modal-header">						
                                            <h4 class="modal-title">Add New Customer</h4>  
                                        </div>
                                    </c:if>
                                    <c:if test="${sessionScope.account.roleID eq 4}">                                                                         
                                        <div class="modal-header">						
                                            <h4 class="modal-title">Add New User</h4>  
                                        </div>
                                    </c:if>
                                    <div class="form-group" style=" display: none">
                                        <label>Confirmation</label>
                                        <input name="confirm" type="text" class="form-control" value="1" required>
                                    </div>
                                    <div class="modal-body">					
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input name="name" type="text" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input name="email" type="text" class="form-control" required>
                                        </div>


                                        <c:if test="${sessionScope.account.roleID eq 4}">
                                            <div class="form-group">
                                                <label>Role</label>
                                                <select name="role" class="form-select" aria-label="Default select example">
                                                    <option value="1" >Customer</option>
                                                    <option value="2" >Sale</option>
                                                    <option value="3" >Marketing</option>
                                                </select>
                                            </div>
                                        </c:if>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <input name="pass" type="password" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm password</label>
                                            <input name="cpass" type="password" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <input name="phone" type="text" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input name="address" type="text" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <section>
                                                <input type="radio" id="maleCheck" name="gender" value="1" checked=""> Male
                                                <input type="radio" id="femaleCheck" name="gender" value="0"> Female                    
                                            </section>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-success" value="Add">
                                    </div>
                                </form>                                .
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Shopping &copy; Website 2024</div>

                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>


        <script>
            // Hàm ẩn thông báo sau một khoảng thời gian đã cho
            function hideErrorMessage() {
                var errorMessage = document.getElementById("errorMessage");
                if (errorMessage) {
                    errorMessage.style.display = "none";
                }
            }

            // Gọi hàm ẩn thông báo sau 30 giây
            setTimeout(hideErrorMessage, 30000); // 30 giây = 30000 mili giây
        </script>
    </body>
</html>

