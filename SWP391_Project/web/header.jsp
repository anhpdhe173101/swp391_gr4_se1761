<%-- 
    Document   : header
    Created on : Jan 30, 2024, 10:02:03 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Header</title> 
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    </head>
    <body>
        <!--Hoang Anh-->
        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="header__top__left">
                                <ul>
                                    <li><i class="fa fa-envelope"></i> hello@project.com</li>
                                    <li>Free Shipping for all Order of $50</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <!--LamTP-->  
                            <div class="header__top__right">
                                <!--                                Check the account is logged in-->
                                <c:if test="${sessionScope.account != null}">                                     
                                    <div class="header__top__right__auth" >
                                        <a href="userProfile"><i class="fa fa-user"></i> ${sessionScope.account.fullName}</a>
                                    </div>
                                    <div class="header__top__right__auth" style="margin-left: 20px;" >
                                        <a href="logout"> Logout</a>
                                    </div>
                                </c:if> 
                                <c:if test="${sessionScope.account == null}">  
                                    <div class="header__top__right__social"></div>
                                    <div class="header__top__right__auth">
                                        <a href="login.jsp" ><i class="fa fa-user"></i> Login</a>
                                    </div>
                                    <div class="header__top__right__auth" style="margin-left: 20px;" >
                                        <a href="SignUp.jsp"> Register</a>
                                    </div>
                                </c:if>
                                <!--LamTP-->     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="header__logo">
                            <a href="./home.jsp"><img src="img/p_logo.png" alt="" style="width: 60%"></a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <nav class="header__menu">
                            <ul>
                                <li class="active"><a href="home">Home</a></li>
                                <li><a href="shopp">Shop</a></li>
                                <li><a href="blog">Blog</a></li>
                                <li><a href="./contact.jsp">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3">
                        <div class="header__cart">
                            <ul>
                                <li><a href="showCart"><i class="fa fa-shopping-bag">                                
                                        </i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="humberger__open">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
        </header>
        <!-- Header Section End -->
        <!--Hoang Anh-->
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                document.querySelector("#login-popup").addEventListener("click", function () {
                    document.querySelector(".popup").classList.add("active");
                });

                document.querySelector(".popup .close-btn").addEventListener("click", function () {
                    document.querySelector(".popup").classList.remove("active");
                });
            });
        </script>
    </body>
</html>
