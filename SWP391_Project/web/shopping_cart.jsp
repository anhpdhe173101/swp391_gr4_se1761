<%-- 
    Document   : shopping_cart
    Created on : Jan 13, 2024, 4:47:30 PM
    Author     : Acer
--%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Cart</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <style>
            .pro-qty button {
                border: none; /* Ẩn viền của các nút button */
                background: #f5f5f5;

            }
        </style>
    </head>

    <body>
        <!-- Page Preloder -->


        <!-- Header Section Begin -->
        <%@ include file="header.jsp" %>
        <!-- Header Section End -->

        <!-- Hero Section Begin -->
        <section class="hero hero-normal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="hero__categories">
                            <div class="hero__categories__all">
                                <i class="fa fa-bars"></i>
                                <span>All categories</span>
                            </div>
                            <ul>
                                <li><a href="#">Smartphone</a></li>
                                <li><a href="#">Laptop</a></li>
                                <li><a href="#">Tablet</a></li>
                                <li><a href="#">Smartwatch</a></li>
                                <li><a href="#">Accessories</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="#">
                                    <input type="text" placeholder="What do you need?">
                                    <button type="submit" class="site-btn">SEARCH</button>
                                </form>
                            </div>
                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>+84 81 661 3231</h5>
                                    <span>Support 24/7 time</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-section set-bg" data-setbg="img/cart.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="breadcrumb__text">
                            <h2>Shopping Cart</h2>
                            <div class="breadcrumb__option">
                                <a href="./index.html">Home</a>
                                <span>Shopping Cart</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Shoping Cart Section Begin -->
        <section class="shoping-cart spad">
            <div class="container">
                <div class="row">
                    <h2 style="color: red;"> ${requestScope.msg}</h2>
                    <div class="col-lg-12">
                        <div class="shoping__cart__table">
                            <c:if test="${not empty requestScope.cart.items}">
                            <table>
                                
                                <thead>
                                    <tr>
                                        <th class="shoping__product">Products</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <c:set var="cart" value="${requestScope.cart}"/>
                                    <c:forEach items="${cart.items}" var="i">
                                        <tr>
                                            <td class="shoping__cart__item">
                                                <img src="img/product_image/${i.product.image}" alt="" style=" height: 150px; width: 150px;">
                                                <h5>${i.product.name}</h5>
                                            </td>
                                            <td class="shoping__cart__price">
                                                ${i.price}
                                            </td>
                                            <td class="shoping__cart__quantity">
                                                <div class="quantity">
                                                    <div class="pro-qty">
                                                        <button><a href="ChangeQuantity?num=-1&id=${i.product.id}">-</a></button>
                                                        <input type="text" value="${i.quantity}" readonly="">
                                                        <button><a href="ChangeQuantity?num=1&id=${i.product.id}">+</a></button>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="shoping__cart__total">
                                                ${(i.price*i.quantity)}
                                            </td>

                                            <td class="shoping__cart__item__close">
                                                <a href="deleteItem?id=${i.product.id}"><span class="icon_close" ></span></a>
                                            </td>

                                        </tr>
                                    </c:forEach>

                                </tbody>
                            </table>
                                    </c:if>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="shoping__cart__btns">
                            <a href="shopp" class="primary-btn cart-btn">CONTINUE SHOPPING</a>

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="shoping__continue">

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="shoping__checkout">
                            <h5>Cart Total</h5>
                            <ul>

                                <li>Total <span>${cart.totalMoney}$</span></li>
                            </ul>

                            <a href="checkOut" class="primary-btn">PROCEED TO CHECKOUT</a>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Shoping Cart Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="./index.html"><img src="img/p_logo.png" alt="" style="width: 60%"></a>
                            </div>
                            <ul>
                                <li>Address: Hoa Lac High-Tech Park, Km29 Thang Long Avenue, Thach That District, Hanoi, Vietnam</li>
                                <li>Phone: +84 81 661 3231</li>
                                <li>Email: hello@project.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                        <div class="footer__widget">
                            <h6>Informations and policies</h6>
                            <ul>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Warranty Policy</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="footer__widget">
                            <h6>Join Our Newsletter Now</h6>
                            <p>Get E-mail updates about our latest shop and special offers.</p>
                            <form action="#">
                                <input type="text" placeholder="Enter your mail">
                                <button type="submit" class="site-btn">Subscribe</button>
                            </form>
                            <div class="footer__widget__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment">
                                <h6>Payment methods</h6>
                                <img src="img/payment.png" alt="" style="width: 30%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
        <script>
                                        document.addEventListener("DOMContentLoaded", function () {
                                            // Lấy tất cả các thẻ span cần xóa
                                            var spanElements = document.querySelectorAll('.pro-qty .dec.qtybtn, .pro-qty .inc.qtybtn');

                                            // Duyệt qua từng thẻ span được lấy
                                            spanElements.forEach(function (spanElement) {
                                                // Nếu thẻ span được tìm thấy
                                                if (spanElement) {
                                                    // Xóa thẻ span khỏi DOM
                                                    spanElement.remove();
                                                }
                                            });
                                        });

        </script>
    </body>

</html>
