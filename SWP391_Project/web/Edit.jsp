<%-- 
    Document   : ManagerProduct
    Author     : Duy Anh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manager.css" rel="stylesheet" type="text/css"/>
        <style>
            img{
                width: 200px;
                height: 120px;
            }
        </style>
    <body>

        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Edit <b>Product</b></h2>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
            <div id="editEmployeeModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="UpdateProduct" method="post">
                            
                            <div class="modal-body">
                                <div class="form-group">
                                <label>ID</label>
                                <input value="${detail.id}" name="id" type="text" class="form-control" readonly required>
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <input value="${detail.name}" name="name" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Image</label>
                                <input value="${detail.image}" name="image" type="num" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Origin Price</label>
                                <input value="${detail.origin_price}" name="origin_price" type="num" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Sale Price</label>
                                <input value="${detail.sale_price}" name="sale_price" type="num" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input value="${detail.quantity}" name="quantity" type="num" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Date</label>
                                <input value="${detail.date}" name="date" type="date" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <input value="${detail.description}" name="description" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select value="${detail.status}" name="status" class="form-select" aria-label="Default select example">
                                        <option  value="1">Hide</option>
                                        <option  value="0">Show</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Category</label>
                                <select value="${detail.cateID}" name="cateID" class="form-select" aria-label="Default select example">
                                    <c:forEach items="${listCC}" var="category">
                                        <option  value="${category.cateID}">${category.name}</option>
                                    </c:forEach>
                                </select>
                            </div>

                        </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                <input type="submit" name="submit" class="btn btn-success" value="Edit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>


        <script src="js/manager.js" type="text/javascript"></script>
    </body>
</html>