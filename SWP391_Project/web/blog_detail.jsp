<%-- 
    Document   : blog
    Created on : Jan 13, 2024, 4:47:39 PM
    Author     : Acer
--%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="entity.Product" %>
<%@page import="entity.Category" %>
<%@page import="java.util.Vector" %>

<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Blog Detail</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <jsp:useBean id="b" class="model.BlogDAO" scope="request"/>
        <jsp:useBean id="bc" class="model.BlogCategoryDAO" scope="request"/>

        <style>
            .comment-section {
                border: 1px solid #ccc;
                padding: 10px;
                margin-top: 20px;
            }

            .comment-input {
                display: flex;
                justify-content: space-between;
                margin-bottom: 20px;
                height: 50px;
            }

            .comment-input textarea {
                width: 90%;
                padding: 10px;
                height: 50px;
            }

            .comment-input button {
                width: 8%;
                height: 50px;
                padding: 10px;
                background-color: #4CAF50;
                color: white;
                border: none;
                cursor: pointer;
            }

            .existing-comments {
                margin-top: 20px;
            }

            .comment {
                margin-bottom: 20px;
                padding: 15px;
                border: 1px solid #ddd;
                border-radius: 5px;
                background-color: #f9f9f9;
            }

            .comment .username {
                color: #888;
                font-size: 12px;
                margin-bottom: 3px;
            }

            .comment .comment-date {
                color: #888;
                font-size: 12px;
            }

            .comment .comment-content {
                margin-top: 10px;
                color: #555;
            }
            .add-blog-button {
                display: inline-block;
                padding: 10px 20px;
                background-color: #007bff;
                color: #fff;
                text-decoration: none;
                border-radius: 4px;
                margin-top: 10px; /* Adjust as needed */
            }

            .add-blog-button:hover {
                background-color: #0056b3;
            }
            .blog__content__info {
                display: flex;
                justify-content: space-between;
            }

            .blog__content__info .date {
                text-align: left;
            }

            .blog__content__info .author {
                text-align: right;
            }
        </style>


    </head>

    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <%@include file='header.jsp'%>

        <!-- Hero Section Begin -->
        <section class="hero hero-normal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="hero__categories">
                            <div class="hero__categories__all">
                                <i class="fa fa-bars"></i>
                                <span>All categories</span>
                            </div>
                            <!--Duy Anh-->
                            <%--                            <ul>
                                                            <% for (Category o : listCC) { %>
                                                            <li><a href="category?cid=<%= o.getCateID() %>"><%=o.getName()%></a></li>
                                                                <%}%>
                                                        </ul>--%>
                            <!--Duy Anh-->
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="#">
                                    <input type="text" placeholder="What do you need?">
                                    <button type="submit" class="site-btn">SEARCH</button>
                                </form>
                            </div>
                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>+84 81 661 3231</h5>
                                    <span>Support 24/7 time</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-section set-bg" data-setbg="img/blog.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="breadcrumb__text">
                            <h2 style="color: black">Blog</h2>
                            <div class="breadcrumb__option">
                                <span style="color: black">Discover the unknown</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Blog Section Begin -->
        <section class="blog spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        <div class="blog__sidebar">
                            <div class="blog__sidebar__search">
                                <form action="blogSearch">
                                    <input type="text" placeholder="Search..." name="key">
                                    <button type="submit"><span class="icon_search"></span></button>
                                </form>
                            </div>
                            <div class="blog__sidebar__item">
                                <h4>Categories</h4>
                                <ul>
                                    <li><a href="blog">All</a></li>
                                        <c:forEach items="${requestScope.allcate}" var="c">
                                        <li><a href="blogCate?cid=${c.categoryID}">${c.categoryName}</a></li>
                                        </c:forEach>
                                </ul>
                            </div>

                            <div class="blog__sidebar__item">
                                <h4>Recent News</h4>
                                <c:forEach items="${requestScope.newblog}" var="n">
                                    <div class="blog__sidebar__recent" style="margin-bottom: 20px;">
                                        <a type="submit" href="blogDetail?bid=${n.id}" class="blog__sidebar__recent__item">
                                            <div class="blog__sidebar__recent__item__pic">
                                                <img src="img/blog/${n.image}" style="width: 70px; height: 45px">
                                            </div>
                                            <div class="blog__sidebar__recent__item__text">
                                                <c:set var="words" value="${fn:split(n.title, ' ')}"/>
                                                <h6>
                                                    <c:forEach items="${words}" var="word" varStatus="loop">
                                                        ${word}<c:if test="${loop.index % 4 == 3 && !loop.last}"><br></c:if>
                                                    </c:forEach>
                                                </h6>
                                                <span>${n.datePosted}</span>
                                            </div>
                                        </a>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="blog__content">
                            <div class="blog__content__title">
                                <h2>${requestScope.blog.title}</h2>
                                <p>${requestScope.blog.opening}</p>
                            </div>
                            <div class="blog__content__image">
                                <img src="img/blog/${requestScope.blog.image}" alt="${requestScope.blog.title}">
                            </div>
                            <div class="blog__content__info">
                                <span class="date">Date Posted: ${requestScope.blog.datePosted}</span>
                                <span class="author">Author: ${requestScope.blog.authorName}</span> 
                            </div>
                            <hr class="cross-border">
                            <div class="blog__content__text">
                                <p>${requestScope.blog.content}</p>
                            </div>
                        </div>
                        <div class="comment-section">
                            <c:if test="${sessionScope.account != null}">
                                <div style="margin-bottom: 10px">
                                    <a href="AddBlog.jsp" class="add-blog-button">Do you want to write a blog? Click Here!</a>
                                </div>
                                <form action="comment">
                                    <div class="comment-input">
                                        <input value="${requestScope.blog.id}" type="hidden" name="blogID">
                                        <textarea name="commentContent" type="text" placeholder="Enter your thought here..."></textarea>
                                        <button type="submit">Send</button>
                                    </div>
                                </form>
                            </c:if>        
                            <hr class="cross-border">
                            <div class="existing-comments">
                                <!-- Existing comments will be displayed here -->
                                <c:forEach items="${requestScope.allcom}" var="n">
                                    <div class="comment">
                                        <div class="comment-date" style="text-align: left;">
                                            ${n.commentDate}
                                        </div>
                                        <div class="username" style="text-align: left;">
                                            ${n.postUserName}
                                        </div>
                                        <div class="comment-content">
                                            ${n.commentContent}
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <!-- Blog Section End -->

        <%@include file='footer.jsp'%>

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>


        <!--LamTP-->
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                document.querySelector("#login-popup").addEventListener("click", function () {
                    document.querySelector(".popup").classList.add("active");
                });

                document.querySelector(".popup .close-btn").addEventListener("click", function () {
                    document.querySelector(".popup").classList.remove("active");
                });
            });
        </script>
        <!--LamTP-->
    </body>

</html>


