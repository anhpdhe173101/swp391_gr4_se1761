<%-- 
    Document   : CustomerTable
    Created on : Feb 28, 2024, 2:28:47 PM
    Author     : Phuc Lam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Tables - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="userProfile">Profile</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">

                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i> ${sessionScope.account.fullName}</a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="home">Home</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">

                            <div class="sb-sidenav-menu-heading">Menu</div>
                            <a class="nav-link" href="userProfile">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Profile

                            </a>                         
                            <a class="nav-link" href="change_password.jsp">
                                <div class="sb-nav-link-icon">
                                    <i class="fa-solid fa-pen-to-square"></i>                           
                                </div>
                                Change Password
                            </a>
                            <c:if test="${sessionScope.account.roleID eq 1}">
                                <a class="nav-link" href="orderHistory">
                                    <div class="sb-nav-link-icon">
                                        <i class="fas fa-table"></i>
                                    </div>
                                    Order History
                                </a>
                            </c:if>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>

                        <c:if test="${sessionScope.account.roleID eq 1}">
                            Customer
                        </c:if>

                        <c:if test="${sessionScope.account.roleID eq 2}">
                            Sale
                        </c:if>

                        <c:if test="${sessionScope.account.roleID eq 3}">
                            Marketing
                        </c:if>

                        <c:if test="${sessionScope.account.roleID eq 4}">
                            Admin
                        </c:if>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Profile</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="userProfile">Profile </a></li>
                            <li class="breadcrumb-item active">Detail</li>

                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <form action="changeProfile" enctype="multipart/form-data" method="post">
                                    <div class="row">

                                        <aside class="col-sm-5 border-right">
                                            <article class="gallery-wrap">
                                                <div class="img-big-wrap">
                                                    <div>
                                                        <a href="#"><img id="previewImage" src="img/user/${requestScope.user.imageURL}" style="height: 500px; width: 500px;"></a>
                                                    </div>
                                                </div>
                                                <div class="img-small-wrap d-flex justify-content-center"> 
                                                    <input name="image" type="file" size="60" class="form-control" >
                                                </div>
                                            </article>
                                        </aside>
                                        <aside class="col-sm-7">
                                            <article class="card-body p-5">
                                                <h3 class="title mb-3" id="errorMessage" style="color: red">${requestScope.error}</h3>

                                                <dl class="item-property">
                                                    <dt>User Info</dt>
                                                    <dd>
                                                        <div class="form-group">
                                                            <label class="form-label">Name</label>
                                                            <input type="text" class="form-control" name="name" value="${requestScope.user.fullName}">
                                                        </div>                                      
                                                        <div class="form-group">
                                                            <label class="form-label">Gender</label>
                                                            <select class="form-select" name="gender">
                                                                <option value="1" ${requestScope.user.gender == 1 ? 'selected' : ''}>Male</option>
                                                                <option value="0" ${requestScope.user.gender == 0 ? 'selected' : ''}>Female</option>
                                                            </select>
                                                        </div> 
                                                        <div class="form-group">
                                                            <label class="form-label">Email</label>
                                                            <input type="text" class="form-control" name="email" value="${requestScope.user.email}" readonly>
                                                        </div>

                                                    </dd>
                                                </dl>

                                                <dl class="param param-inline">
                                                    <dt>Contact Info </dt>
                                                    <dd>
                                                        <div class="form-group">
                                                            <label class="form-label">Name of Consignee</label>
                                                            <input type="text" class="form-control" name="nameContact" value="${requestScope.contact.fullName}">
                                                        </div>   
                                                        <div class="form-group">
                                                            <label class="form-label">Address</label>
                                                            <input type="text" class="form-control" name="address" value="${requestScope.contact.address}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label">Phone</label>
                                                            <input type="text" class="form-control" name="phone" value="${requestScope.contact.phone}">
                                                        </div>                                 
                                                        <div class="form-group">
                                                            <label class="form-label">Email Contact</label>
                                                            <input type="text" class="form-control" name="emailContact" value="${requestScope.contact.email}">
                                                        </div>                                    
                                                        <div class="form-group">
                                                            <label class="form-label">Method used to contact me</label>
                                                            <select class="form-select" name="note">
                                                                <option value="Use Phone" ${requestScope.contact.note == 'Use Phone' ? 'selected' : ''}>Use Phone</option>
                                                                <option value="Use Email" ${requestScope.contact.note == 'Use Email' ? 'selected' : ''}>Use Email</option>
                                                            </select>
                                                        </div>
                                                    </dd>

                                                </dl>  

                                                <hr>
                                                <button type="submit" class="btn btn-lg btn-primary text-uppercase">Update Detail</button>

                                            </article> 
                                        </aside> 
                                    </div>
                                    .</form>
                            </div>


                        </div>
                        <c:if test="${sessionScope.account.roleID eq 1}">                                  
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-table me-1"></i>
                                    Favorite Product Table
                                </div>
                                <div class="card-body">
                                    <table id="datatablesSimple">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Image</th>
                                                <th>Category</th>
                                                <th>Price</th>                            
                                                <th>Description</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Image</th>
                                                <th>Category</th>
                                                <th>Origin Price</th>                            
                                                <th>Description</th>
                                                <th>Delete</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <c:forEach items="${product}" var="c">
                                                <tr>                                
                                                    <td>${c.id}</td>                                
                                                    <td>${c.name}</td>
                                                    <td><img style=" height: 150px; width: 150px;" src="img/product_image/${c.image}" ></td>
                                                    <td>${c.cateID}</td>
                                                    <td >${c.origin_price}</td>                     
                                                    <td>${c.description}</td>  
                                                    <td id="delete_col">
                                                        <a onclick="doDelete('${c.id}')">
                                                            <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </c:forEach> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </c:if>  

                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Shopping &copy; Website 2024</div>

                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>
        <script type="text/javascript">
                                                            function doDelete(id) {
                                                                if (confirm("Do you really want to delete this product?")) {
                                                                    window.location.href = "deleteFavoriteProduct?id=" + id;
                                                                }
                                                            }
        </script>

        <script>
            // Hàm ẩn thông báo sau một khoảng thời gian đã cho
            function hideErrorMessage() {
                var errorMessage = document.getElementById("errorMessage");
                if (errorMessage) {
                    errorMessage.style.display = "none";
                }
            }

            // Gọi hàm ẩn thông báo sau 30 giây
            setTimeout(hideErrorMessage, 30000); // 30 giây = 30000 mili giây
        </script>
    </body>
</html>

