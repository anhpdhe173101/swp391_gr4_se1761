<%-- 
    Document   : Detail
    Author     : Duy Anh
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
         <link href="css/style.css" rel="stylesheet" type="text/css"/>
         
        <style>
            .gallery-wrap .img-big-wrap img {
                height: 450px;
                width: auto;
                display: inline-block;
                cursor: zoom-in;
            }


            .gallery-wrap .img-small-wrap .item-gallery {
                width: 60px;
                height: 60px;
                border: 1px solid #ddd;
                margin: 7px 2px;
                display: inline-block;
                overflow: hidden;
            }

            .gallery-wrap .img-small-wrap {
                text-align: center;
            }
            .gallery-wrap .img-small-wrap img {
                max-width: 100%;
                max-height: 100%;
                object-fit: cover;
                border-radius: 4px;
                cursor: zoom-in;
            }
            .img-big-wrap img{
                width: 100% !important;
                height: auto !important;
            }
        </style>
    </head>
    <body>


           
                <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="ManageProduct">Manage Product</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">
                    
                    <!-- Filter by category and status -->
                    <form action="FilterProduct" method="post" class="filter-form">
                    <label for="category-filter" style="color: white;">Category:</label>
                    <select id="category-filter" name="cid">
                        <option value="">All</option>
                        <c:forEach items="${listCC}" var="category">
                            <option  value="${category.cateID}">${category.name}</option>
                        </c:forEach>
                        
                    </select>
                    <label for="status-filter" style="color: white;" >Status:</label>
                    <select id="status-filter" name="status">
                        <option value="">All</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                    <button type="submit" class="btn btn-secondary btn-number">Filter</button>

                </form>
                    <!-- Search by name -->
                <form action="searchName" method="post" class="form-inline my-2 my-lg-0">
                    <div class="input-group input-group-sm">
                        <input name="txt" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Search...">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary btn-number">
                                Search
                            </button>
                        </div>
                    </div>

                </form>
                </div>
            </div>
        </nav>
                <br>
                 <div class="container">
                <div border="1" class="row">
                    
                
                    <div class="container">
                        <div class="card">
                            <div class="row">
                                <aside class="col-sm-5 border-right">
                                    <article class="gallery-wrap"> 
                                        <div class="img-big-wrap">
                                            <div> <a href="#"><img src="img/product_image/${detail.image}"></a></div>
                                        </div> 
                                        <div class="img-small-wrap">
                                            
                                        </div> 
                                    </article> 
                                </aside>
                                <aside class="col-sm-7">
                                    <article class="card-body p-5">
                                        <h3 class="title mb-3">${detail.name}</h3>

                                        <p class="price-detail-wrap"> 
                                            <span class="price h3 text-warning"> 
                                                <span class="currency">$</span><span class="num">${detail.sale_price}   </span><del style="color: black;"><span class="currency"style="color: black;">          $</span><span class="num" style="color: black;">${detail.origin_price}</span></del>
                                            </span> 
                                             
                                        </p> 
                                        <dl class="item-property">
                                            <dt>Description</dt>
                                            <dd><p>${detail.description}</p></dd>
                                        </dl>


                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <dl class="param param-inline">
                                                    <dt>Quantity: ${detail.quantity}</dt>
                                                    
                                                </dl>  
                                            </div>
                                            
                                        </div> 
                                        <hr>
                                        <!-- link to load data for update -->
                                        <a href="LoadProduct?pid=${detail.id}" class="btn btn-lg btn-primary text-uppercase"> Update Detail </a>
                                        
                                    </article> 
                                </aside> 
                            </div>
                        </div>


                    </div>
                
            </div>
        </div>
       
        
    </body>
</html>
