<%-- 
    Document   : PageTemplate
    Created on : Feb 29, 2024, 4:39:41 PM
    Author     : Acer
--%>

<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Tables - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
        <style>
            /* ... */

            /* Thêm CSS cho phần email, phone, address nằm trên cùng một hàng ngang */
            .filter-container {
                display: flex;
                gap: 10px;
                align-items: center;
            }

            .filter-container label {
                margin-bottom: 0;
                margin-right: 10px; /* Khoảng cách giữa label và input/select */
            }

            .filter-container input {
                margin-bottom: 0;
            }

            .table {
                width: 100%;
                margin-bottom: 1rem;
                color: #212529;
                border-collapse: collapse;
            }

            .table th,
            .table td {
                padding: 0.75rem;
                vertical-align: top;
                border-top: 2px solid #333; /* Độ đậm của đường kẻ đã được tăng lên */
            }

            .table thead th {
                vertical-align: bottom;
                border-bottom: 2px solid #333; /* Độ đậm của đường kẻ đã được tăng lên */
            }

            .table tbody + tbody {
                border-top: 2px solid #333; /* Độ đậm của đường kẻ đã được tăng lên */
            }
            .img-container {
                max-width: 100px; /* Đặt kích thước tối đa cho container ảnh */
                text-align: center; /* Canh giữa ảnh trong container */
            }

            .product-image {
                max-width: 50%; /* Đảm bảo ảnh không vượt quá kích thước container */
                height: auto; /* Cho phép ảnh tự điều chỉnh chiều cao */
            }
        </style>


    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="index.html">Start Bootstrap</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">
                    
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="userProfile">Profile</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Main page</div>
                            <a class="nav-link" href="ShowChart">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                MKT Dashboard
                            </a>
                            <div class="sb-sidenav-menu-heading"> All List Pages</div>
                            <!-- 1 -->
                            <a class="nav-link" href="ManageOrder">
                                <div class="sb-nav-link-icon"><i class="fas fa-table me-1"></i></div>
                                Order List
                            </a>
                            <!--                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                                            <nav class="sb-sidenav-menu-nested nav">
                                                                <a class="nav-link" href="layout-static.html">Product List</a>
                                                                <a class="nav-link" href="layout-sidenav-light.html">Product Detail</a>
                                                            </nav>
                                                        </div>-->
                            <!--                             2 
                                                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts2" aria-expanded="false" aria-controls="collapseLayouts2">
                                                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                                            Slider Management
                                                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                                        </a>
                                                        <div class="collapse" id="collapseLayouts2" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                                            <nav class="sb-sidenav-menu-nested nav">
                                                                <a class="nav-link" href="layout-static.html">Slider List</a>
                                                                <a class="nav-link" href="layout-sidenav-light.html">Slider Detail</a>
                                                            </nav>
                                                        </div>
                                                         3 
                                                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts3" aria-expanded="false" aria-controls="collapseLayouts3">
                                                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                                            Customer Management
                                                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                                        </a>
                                                        <div class="collapse" id="collapseLayouts3" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                                            <nav class="sb-sidenav-menu-nested nav">
                                                                <a class="nav-link" href="layout-static.html">Customer List</a>
                                                                <a class="nav-link" href="layout-sidenav-light.html">Customer Detail</a>
                                                            </nav>
                                                        </div>
                                                         4 
                                                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts4" aria-expanded="false" aria-controls="collapseLayouts4">
                                                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                                            Post/Blog Management
                                                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                                        </a>-->
                            <!--                            <div class="collapse" id="collapseLayouts4" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                                            <nav class="sb-sidenav-menu-nested nav">
                                                                <a class="nav-link" href="layout-static.html">Post List</a>
                                                                <a class="nav-link" href="layout-sidenav-light.html">Post Detail</a>
                                                            </nav>
                                                        </div>-->
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        User name put here
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4"></h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Tables</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Order Detail
                            </div>
                            <div class="card-body">
                                <form action="EditOrder" method="post">
                                    <div class="modal-body">					
                                        <div class="modal-body">					
                                            <div class="form-group filter-container">
                                                <label>Order id</label>
                                                <input value="${o.id}" name="oid" type="num" class="form-control" readonly required>
                                            

                                                <label>Customer Name</label>
                                                <input value="${o.name}" name="name" type="text" class="form-control" readonly required>
                                            </div>
                                            <br>
                                            <div class="form-group filter-container">
                                                <label>Email</label>
                                                <input value="${o.email}" name="email" type="text" class="form-control" readonly required>

                                                <label>Phone</label>
                                                <input value="${o.phone}" name="phone" type="num" class="form-control" readonly required>

                                                <label>Address</label>
                                                <input value="${o.address}" name="address" type="text" class="form-control" readonly required>
                                            </div>
                                            <br>
                                            <div>

                                                <table class="table">
                                                    <thead>
                                                        <tr>

                                                            <th scope="col">Image</th>
                                                            <th scope="col">Product</th>
                                                            <th scope="col">Category</th>
                                                            <th scope="col">Price</th>
                                                            <th scope="col">Quantity</th>
                                                            <th scope="col">Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${list}" var="o">
                                                            <tr>

                                                                <td class="img-container">
                                                                    <img src="img/product_image/${o.image}" alt="${o.name}" class="product-image">
                                                                </td>
                                                                <td>${o.name}</td>
                                                                <td>${o.category}</td>
                                                                <td>${o.price}</td>
                                                                <td>${o.quantity}</td>
                                                                <td>${o.amount}</td>

                                                            </tr>
                                                        </c:forEach>
                                                        <!-- Add more rows as needed -->
                                                    </tbody>
                                                </table>

                                                <br>
                                                <div class="form-group filter-container">
                                                    <label>Date</label>
                                                    <input value="${o.order_date}" name="order_date" type="datetime" class="form-control" readonly required>
                                                
                                                    <label>Total</label>
                                                    <input value="${o.total}" name="total" type="num" class="form-control" readonly required>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <select name="status" class="form-control" ${o.status.equals("done") ? "disabled" : ""}>
                                                        <option value="wait" ${o.status.equals("wait") ? "selected" : ""}>Wait</option>
                                                        <option value="process" ${o.status.equals("processing") ? "selected" : ""}>Process</option>
                                                        <option value="done" ${o.status.equals("done") ? "selected" : ""}>Done</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" name="submit" class="btn btn-success" value="Edit">
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2023</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>


        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>
    </body>
</html>

