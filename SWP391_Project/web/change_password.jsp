<%-- 
    Document   : CustomerTable
    Created on : Feb 28, 2024, 2:28:47 PM
    Author     : Phuc Lam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Tables - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="userProfile">Profile</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">

                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i> ${sessionScope.account.fullName}</a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="home">Home</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">

                            <div class="sb-sidenav-menu-heading">Menu</div>
                            <a class="nav-link" href="userProfile">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Profile

                            </a>                         
                            <a class="nav-link" href="change_password.jsp">
                                <div class="sb-nav-link-icon">
                                    <i class="fa-solid fa-pen-to-square"></i>                           
                                </div>
                                Change Password
                            </a>
                            <c:if test="${sessionScope.account.roleID eq 1}">    
                            <a class="nav-link" href="orderHistory">
                                <div class="sb-nav-link-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                Order History
                            </a>
                            </c:if>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Customer
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Profile</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="userProfile">Profile </a></li>
                            <li class="breadcrumb-item active">Change Password</li>

                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">

                                <c:if test="${sessionScope.confirm == null}">  
                                    <div class="card-body pb-2">
                                        <form action="ConfirmPassword" >
                                            <input type="hidden" name="email" value="${sessionScope.account.email}"/>
                                            <h5 style="color: red ">${requestScope.fault}</h5>
                                            <div class="form-group">
                                                <label class="form-label">Confirm Password</label>
                                                <input type="password" name="passnow" class="form-control">
                                            </div>
                                            <div class="text-right mt-3">
                                                <button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                                            </div>
                                        </form>
                                    </div>
                                </c:if>        

                                <c:if test="${sessionScope.confirm != null}">  
                                    <div class="card-body pb-2">
                                        <form action="change">
                                            <input type="hidden" name="email" value="${sessionScope.account.email}"/>
                                            <h5 style="color: red ">${requestScope.error}</h5>
                                            <div class="form-group">
                                                <label class="form-label">New password</label>
                                                <input type="password" name="pass" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Repeat new password</label>
                                                <input type="password" name="cpass" class="form-control">
                                            </div>
                                            <div class="text-right mt-3">
                                                <button type="submit" class="btn btn-primary">Save changes</button>&nbsp;
                                            </div>
                                        </form>
                                    </div>
                                </c:if>    

                            </div>                                  .
                        </div>
                    </div>


                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Shopping &copy; Website 2024</div>

                        </div>
                    </div>
                </footer>
            </div>

            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
            <script src="js/scripts.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
            <script src="js/datatables-simple-demo.js"></script>


            <script>
                // Hàm ẩn thông báo sau một khoảng thời gian đã cho
                function hideErrorMessage() {
                    var errorMessage = document.getElementById("errorMessage");
                    if (errorMessage) {
                        errorMessage.style.display = "none";
                    }
                }

                // Gọi hàm ẩn thông báo sau 30 giây
                setTimeout(hideErrorMessage, 30000); // 30 giây = 30000 mili giây
            </script>
    </body>
</html>

