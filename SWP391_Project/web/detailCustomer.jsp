<%-- 
    Document   : CustomerTable
    Created on : Feb 28, 2024, 2:28:47 PM
    Author     : Phuc Lam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Tables - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>

    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="index.html">Start Bootstrap</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">

                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i> ${sessionScope.account.fullName}</a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="userProfile">Profile</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Menu</div>


                            <c:if test="${sessionScope.account.roleID eq 3}">
                                <a class="nav-link" href="UserChartByWeek">
                                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                    Dashboard
                                </a>
                            </c:if>



                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                    <c:if test="${sessionScope.account.roleID eq 3}">
                                    Customer 
                                </c:if>

                                <c:if test="${sessionScope.account.roleID eq 4}">
                                    User 
                                </c:if>
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <c:if test="${sessionScope.account.roleID eq 3}">
                                        <a class="nav-link" href="manageCustomer">                                                                            
                                            Customer Tables                                                                              
                                        </a>                                
                                        <a class="nav-link" href="addNewCustomer.jsp">Add New Customer</a>
                                    </c:if>

                                    <c:if test="${sessionScope.account.roleID eq 4}">
                                        <a class="nav-link" href="manageCustomer">
                                            User Tables

                                        </a>
                                        <a class="nav-link" href="addNewCustomer.jsp">Add New User</a>
                                    </c:if>
                                </nav>
                            </div>

                            <c:if test="${sessionScope.account.roleID eq 3}">
                                <a class="nav-link" href="postList">
                                    <div class="sb-nav-link-icon"><i class="fa-regular fa-address-card"></i></div>
                                    Post Management

                                </a>

                                <a class="nav-link" href="manageSlider">
                                    <div class="sb-nav-link-icon"><i class="fa-solid fa-sliders"></i></div>
                                    Slider Management

                                </a>
                                <a class="nav-link" href="ManageProduct">
                                    <div class="sb-nav-link-icon"><i class="fa-brands fa-product-hunt"></i></div>
                                    Product Management

                                </a>
                            </c:if>

                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        <c:if test="${sessionScope.account.roleID eq 4}">
                            Admin
                        </c:if>

                        <c:if test="${sessionScope.account.roleID eq 3}">
                            Marketing
                        </c:if>

                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Tables</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="UserChartByWeek">Dashboard</a></li>
                            <li class="breadcrumb-item active">Tables</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">

                                <div class="row">
                                    <aside class="col-sm-5 border-right">
                                        <article class="gallery-wrap"> 
                                            <div class="img-big-wrap">
                                                <div> <a href="#"><img src="img/user/${detail.user.imageURL}" style=" height: 500px; width: 500px;"></a></div>
                                            </div> 
                                            <div class="img-small-wrap">

                                            </div> 
                                        </article> 
                                    </aside>
                                    <aside class="col-sm-7">
                                        <article class="card-body p-5">
                                            <h3 class="title mb-3">${detail.user.fullName}</h3>
                                            <dl class="item-property">
                                                <dt>User Info</dt>
                                                <dd>
                                                    <p>Email: ${detail.user.email}</p>
                                                    <p>Gender: ${detail.user.gender == 1 ? 'Male' : 'Female'}</p>
                                                    <p>Status: ${detail.user.status == 1 ? 'Active' : 'Inactive'}</p>
                                                    <c:if test="${sessionScope.account.roleID eq 4}">
                                                        <p style="color: red">
                                                            Role:
                                                            <c:choose>
                                                                <c:when test="${detail.user.roleID == 1}">Customer</c:when>
                                                                <c:when test="${detail.user.roleID == 2}">Sale</c:when>
                                                                <c:when test="${detail.user.roleID == 3}">Marketing</c:when>
                                                                <c:when test="${detail.user.roleID == 4}">Admin</c:when>
                                                            </c:choose>
                                                        </p> 
                                                    </c:if>
                                                     <p>Date created: ${detail.user.date}</p>
                                                </dd>
                                            </dl>
                                            <hr>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <dl class="param param-inline">
                                                        <dt>Contact Info </dt>
                                                        <dd>
                                                            <p>Name Contact: ${detail.contactInfo.fullName}</p>
                                                            <p>Phone: ${detail.contactInfo.phone}</p>
                                                            <p>Address: ${detail.contactInfo.address}</p>
                                                            <p>Contact Method: ${detail.contactInfo.note}</p>
                                                        </dd>

                                                    </dl>  
                                                </div>

                                            </div> 
                                            <hr>
                                            <a href="editCustomer?cid=${detail.user.id}" class="btn btn-lg btn-primary text-uppercase"> Update Detail </a>
                                        </article> 
                                    </aside> 
                                </div>
                                .
                            </div>
                        </div>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                History Change Table
                            </div>
                            <div class="card-body">
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>Name Before Change</th>
                                            <th>Name After Change</th>
                                            <th>Phone Before Change</th>
                                            <th >Phone After Change</th>                            
                                            <th>Gender Before Change</th>
                                            <th >Gender After Change </th>
                                            <th >Address Before Change </th>
                                            <th >Address After Change </th>
                                            <th>Change Time</th>
                                            <th>Editor Name</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name Before Change</th>
                                            <th >Name After Change</th>
                                            <th>Phone Before Change</th>
                                            <th>Phone After Change</th>                            
                                            <th>Gender Before Change</th>
                                            <th>Gender After Change </th>
                                            <th>Address Before Change </th>
                                            <th>Address After Change </th>
                                            <th>Change Time</th>
                                            <th>Editor Name</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <c:forEach items="${requestScope.history}" var="c">
                                            <tr>                                                               
                                                <td>${c.before.fullName}</td>
                                                <td style="color:red">${c.after.fullName}</td>
                                                <td>${c.before.phone}</td>
                                                <td  style="color:red">${c.after.phone}</td>
                                                <td >${c.before.gender == 1 ? 'Male' : c.before.gender == 0 ? 'Female' : ''}</td>
                                                <td style="color:red">${c.after.gender == 1 ? 'Male' : c.after.gender ==0 ? 'Female' : ''}</td>
                                                <td>${c.before.address}</td>
                                                <td style="color:red">${c.after.address}</td> 
                                                <td>${c.date}</td> 
                                                <td>${c.edit.fullName}</td>                                
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Shopping &copy; Website 2024</div>

                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>
    </body>
</html>

