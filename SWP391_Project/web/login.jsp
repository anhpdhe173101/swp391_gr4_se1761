<%-- 
    Document   : login
    Created on : Feb 25, 2024, 7:13:18 PM
    Author     : Phuc Lam
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Login | Ludiflex</title>

        <style>
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500&display=swap');
            *{
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: 'Poppins',sans-serif;
            }
            body{
                display: flex;
                justify-content: center;
                align-items: center;
                min-height: 100vh;
                background:#f8fafb;
            }
            .login-box{
                display: flex;
                justify-content: center;
                flex-direction: column;
                width: 440px;
                height: 480px;
                padding: 30px;
            }
            .login-header{
                text-align: center;
                margin: 20px 0 40px 0;
            }
            .login-header header{
                color: #333;
                font-size: 30px;
                font-weight: 600;
            }
            .input-box .input-field{
                width: 100%;
                height: 60px;
                font-size: 17px;
                padding: 0 25px;
                margin-bottom: 15px;
                border-radius: 30px;
                border: none;
                box-shadow: 0px 5px 10px 1px rgba(0,0,0, 0.05);
                outline: none;
                transition: .3s;
            }
            ::placeholder{
                font-weight: 500;
                color: #222;
            }
            .input-field:focus{
                width: 105%;
            }
            .forgot{
                display: flex;
                margin-bottom: 25px;
            }
            section{
                display: flex;
                align-items: center;
                font-size: 14px;
                color: #555;
            }

            #check{
                margin-right: 10px;
            }
            a{
                text-decoration: none;
            }
            a:hover{
                text-decoration: underline;
            }
            section a{
                color: #555;
            }
            .input-submit{
                position: relative;
            }
            .submit-btn{
                width: 100%;
                height: 60px;
                background: #18ad51;
                border: none;
                border-radius: 30px;
                cursor: pointer;
                transition: .3s;
            }
            .input-submit label{
                position: absolute;
                top: 45%;
                left: 50%;
                color: #fff;
                -webkit-transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                cursor: pointer;
            }
            .submit-btn:hover{
                background: #000;
                transform: scale(1.05,1);
            }
            .sign-up-link{
                text-align: center;
                font-size: 15px;
                margin-top: 10px;
            }
            .sign-up-link a{
                color: #000;
                font-weight: 600;
            }

        </style>
    </head>
    <body>
        <div class="login-box">
            <form action="login" > 
                <div class="login-header">
                    <header>Sign In</header>
                    <h5 style="color: red " id="errorMessage">${requestScope.error}</h5>
                </div>
                <div class="input-box">
                    <input type="text" class="input-field" placeholder="Email" name="email" value="${cookie.cuser.value}" required>
                </div>
                <div class="input-box">
                    <input type="password" class="input-field" placeholder="Password" name="pass" value="${cookie.cpass.value}"required>
                </div>
                <div class="forgot">       
                    <input type="checkbox"  name="rem" value="on" 
                           ${cookie.crem.value eq 'on'?'checked':''} > 
                    Remember me      
                    <a href="ForgotPassword.jsp" style="margin-left: 100px;">Forgot Password</a>
                </div>
                <div class="input-submit">
                    <button class="submit-btn" id="submit" type="submit"></button>
                    <label for="submit">Sign In</label>
                </div>
            </form>         
            <div class="sign-up-link">
                <p>Do you have account? <a href="SignUp.jsp">Register</a></p>
                <p>Do you want back to home page? <a href="home">Home</a></p>
            </div>
        </div>
        
        <script>
            function hideErrorMessage() {
                var errorMessage = document.getElementById("errorMessage");
                if (errorMessage) {
                    errorMessage.style.display = "none";
                }
            }     
            setTimeout(hideErrorMessage, 30000); 
        </script>
    </body>
    

</html>

