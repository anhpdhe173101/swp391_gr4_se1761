<%-- 
    Document   : CustomerTable
    Created on : Feb 28, 2024, 2:28:47 PM
    Author     : Phuc Lam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Tables - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>

        <style>
            img{
                width: 200px;
                height: 150px;
            }
            .checkbox-option {
                display: inline-block;
                margin-right: 10px;
            }
            .hidden {
                display: none;
            }

            .filter-container {
                display: flex;
                gap: 10px;
                align-items: center;
            }

            .filter-container label {
                margin-bottom: 0;
                margin-right: 10px; /* Khoảng cách giữa label và input/select */
            }

            .filter-container input,
            .filter-container select {
                margin-bottom: 0;
            }

            .filter-container button {
                margin-top: 8px;
            }

            /* Thêm CSS để nằm trên cùng một hàng ngang */
            .filter-container div {
                display: flex;
                align-items: center;
            }

            .filter-container button {
                margin-top: 0; /* Đặt lại margin-top về 0 để nằm trên cùng hàng ngang */
            }
            
        </style>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="manageCustomer">Admin</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">

                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i> ${sessionScope.account.fullName}</a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="userProfile">Profile</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Menu</div>

                            <a class="nav-link" href="manageCustomer">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                User Tables
                            </a>

                            <a class="nav-link" href="addNewCustomer.jsp">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Add New User

                            </a>
                        </div>
                    </div>

                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Admin
                    </div>


                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Admin</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="manageCustomer">User</a></li>
                            <li class="breadcrumb-item active">Table</li>
                        </ol>


                        <div id="checkbox_div" >
                            <div style="color: blue; font-weight: bold">Show Hide Column:

                            </div>
                            <div class="checkbox-option"><input type="checkbox" onchange="toggleColumn(0);">ID</div>
                            <div class="checkbox-option"><input type="checkbox" onchange="toggleColumn(1);">Image</div>
                            <div class="checkbox-option"><input type="checkbox" onchange="toggleColumn(2);">Name</div>
                            <div class="checkbox-option"><input type="checkbox" onchange="toggleColumn(3);">Gender</div>
                            <div class="checkbox-option"><input type="checkbox" onchange="toggleColumn(4);">Email</div>
                            <div class="checkbox-option"><input type="checkbox" onchange="toggleColumn(5);">Phone</div>
                            <div class="checkbox-option"><input type="checkbox" onchange="toggleColumn(6);">Address</div>
                            <div class="checkbox-option"><input type="checkbox" onchange="toggleColumn(7);">Role</div>
                            <div class="checkbox-option"><input type="checkbox" onchange="toggleColumn(8);">Status</div>
                            <div class="checkbox-option"><input type="checkbox" onchange="toggleColumn(9);">Update</div>
                        </div>

                        <div class="card mb-4">
                            <div class="card-header" style="display: flex; justify-content: space-between; align-items: center;">
                                <div>
                                    <i class="fas fa-table me-1"></i>
                                    <span>User Table</span>
                                </div>

                                <h5 style="color: red; margin: 0;">${requestScope.error}</h5>

                                <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal">
                                    <a href="addNewCustomer.jsp" style="text-decoration: none; color: white;">Add New User</a>
                                </button>


                            </div>

                            <div class="card-body">

                                <form action="filterUserByStatus" method="post" class="form-inline my-2 my-lg-0 d-flex align-items-center">
                                    <div class="filter-container">
                                        <div>
                                            <label class="form-label" style="color: red; font-weight: bold">Gender:</label>
                                            <div>
                                                <input type="checkbox" name="gender" value="1" id="male">
                                                <label for="male">Male</label>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="gender" value="0" id="female">
                                                <label for="female">Female</label>
                                            </div>
                                        </div>
                                        <div>
                                            <label class="form-label" style="color: red; font-weight: bold">Role:</label>
                                            <div>
                                                <input type="checkbox" name="role" value="1" id="customer">
                                                <label for="customer">Customer</label>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="role" value="2" id="sale">
                                                <label for="sale">Sale</label>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="role" value="3" id="marketing">
                                                <label for="marketing">Marketing</label>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="role" value="4" id="admin">
                                                <label for="admin">Admin</label>
                                            </div>
                                        </div>
                                        <div>
                                            <label class="form-label"style="color: red; font-weight: bold">Status:</label>
                                            <div>
                                                <input type="checkbox" name="status" value="1" id="active">
                                                <label for="active">Active</label>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="status" value="0" id="inactive">
                                                <label for="inactive">Inactive</label>
                                            </div>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-primary" >
                                                <i class="fa fa-search"></i> Filter
                                            </button>
                                        </div>
                                    </div>
                                </form>




                                <table id="datatablesSimple">

                                    <thead>

                                        <tr>

                                            <th>ID</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Email</th>                            
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Role</th>
                                            <th>Status</th>
                                            <th>Update</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Email</th>                            
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Role</th>
                                            <th>Status</th>
                                            <th>Update</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <c:forEach items="${list}" var="c">    
                                            <tr>
                                                <td>${c.user.id}</td> 
                                                <td>
                                                    <img style=" height: 150px; width: 150px;" src="img/user/${c.user.imageURL}">
                                                </td>
                                                <td>
                                                    <a href="customerDetail?cid=${c.user.id}" style="text-decoration: none">
                                                        ${c.user.fullName}
                                                    </a>                               
                                                </td>
                                                <td> ${c.user.gender == 1 ? 'Male' : 'Female'}</td>
                                                <td> ${c.user.email}</td>
                                                <td> ${c.contactInfo.phone}</td>
                                                <td> ${c.contactInfo.address}</td>
                                                <td style="font-weight: bold;" >    
                                                    <c:choose>
                                                        <c:when test="${c.user.roleID == 1}">Customer</c:when>
                                                        <c:when test="${c.user.roleID == 2}">Sale</c:when>
                                                        <c:when test="${c.user.roleID == 3}">Marketing</c:when>
                                                        <c:when test="${c.user.roleID == 4}">Admin</c:when>

                                                    </c:choose>
                                                </td>
                                                <td>
                                                    <c:if test="${c.user.status == 1}">
                                                        <div class="col">
                                                            <a href="updateUserStatus?id=${c.user.id}&status=0" class="btn btn-success btn-block" style="color: white;">Active</a>
                                                        </div>

                                                    </c:if>
                                                    <c:if test="${c.user.status == 0}">
                                                        <div class="col">
                                                            <a href="updateUserStatus?id=${c.user.id}&status=1" class="btn btn-danger btn-block" style="color: white;">Inactive</a>
                                                        </div>
                                                    </c:if>
                                                </td>
                                                <td><a href="editCustomer?cid=${c.user.id}"  
                                                       class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a></td>
                                            </tr>
                                        </c:forEach>   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Shopping &copy; Website 2024</div>

                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>

        <script>
                                function toggleColumn(columnIndex) {
                                    var table = document.getElementById("datatablesSimple");
                                    var headerRow = table.getElementsByTagName("thead")[0].getElementsByTagName("tr")[0];
                                    var bodyRows = table.getElementsByTagName("tbody")[0].getElementsByTagName("tr");

                                    // Ẩn/hiện cột trong hàng tiêu đề
                                    var headerCell = headerRow.getElementsByTagName("th")[columnIndex];
                                    headerCell.classList.toggle("hidden");

                                    // Ẩn/hiện cột trong các hàng dữ liệu
                                    for (var i = 0; i < bodyRows.length; i++) {
                                        var bodyCell = bodyRows[i].getElementsByTagName("td")[columnIndex];
                                        bodyCell.classList.toggle("hidden");
                                    }
                                }
        </script>
        
        

    </body>
</html>

