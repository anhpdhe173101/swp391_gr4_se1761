<%-- 
    Document   : blog
    Created on : Jan 13, 2024, 4:47:39 PM
    Author     : Acer
--%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="entity.Product" %>
<%@page import="entity.Category" %>
<%@page import="java.util.Vector" %>

<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Blog</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <jsp:useBean id="b" class="model.BlogDAO" scope="request"/>
        <jsp:useBean id="bc" class="model.BlogCategoryDAO" scope="request"/>
        <jsp:useBean id="t" class="model.ProductDAO" scope="request"/>
    </head>

    <body>
        <!-- Page Preloder -->
    <di<body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <%@include file='header.jsp'%>
        <!-- Hero Section Begin -->
        <section class="hero hero-normal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="hero__categories">
                            <div class="hero__categories__all">
                                <i class="fa fa-bars"></i>
                                <span>All categories</span>
                            </div>
                            <ul>
                                <li><a href="#">Smartphone</a></li>
                                <li><a href="#">Laptop</a></li>
                                <li><a href="#">Tablet</a></li>
                                <li><a href="#">Smartwatch</a></li>
                                <li><a href="#">Accessories</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="#">
                                    <input type="text" placeholder="What do you need?">
                                    <button type="submit" class="site-btn">SEARCH</button>
                                </form>
                            </div>
                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>+84 81 661 3231</h5>
                                    <span>Support 24/7 time</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-section set-bg" data-setbg="img/blog.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="breadcrumb__text">
                            <h2 style="color: black">Blog</h2>
                            <div class="breadcrumb__option">
                                <span style="color: black">Discover the unknown</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Blog Section Begin -->
        <section class="blog spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        <div class="blog__sidebar">
                            <div class="blog__sidebar__search">
                                <form action="blogSearch">
                                    <input type="text" placeholder="Search..." name="key">
                                    <button type="submit"><span class="icon_search"></span></button>
                                </form>
                            </div>                            
                            <div class="blog__sidebar__item">
                                <h4>Categories</h4>
                                <ul>
                                    <li><a href="blog">All</a></li>
                                        <c:forEach items="${requestScope.allcate}" var="c">
                                        <li><a href="blogCate?cid=${c.categoryID}">${c.categoryName}</a></li>
                                        </c:forEach>
                                </ul>
                            </div>
                            <div class="blog__sidebar__item">
                                <h4>Recent News</h4>
                                <c:forEach items="${requestScope.newblog}" var="n">
                                    <div class="blog__sidebar__recent" style="margin-bottom: 20px;">
                                        <a type="submit" href="blogDetail?bid=${n.id}" class="blog__sidebar__recent__item">
                                            <div class="blog__sidebar__recent__item__pic">
                                                <img src="img/blog/${n.image}" style="width: 70px; height: 45px">
                                            </div>
                                            <div class="blog__sidebar__recent__item__text">
                                                <c:set var="words" value="${fn:split(n.title, ' ')}"/>
                                                <h6>
                                                    <c:forEach items="${words}" var="word" varStatus="loop">
                                                        ${word}<c:if test="${loop.index % 4 == 3 && !loop.last}"><br></c:if>
                                                    </c:forEach>
                                                </h6>
                                                <span>${n.datePosted}</span>
                                            </div>
                                        </a>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>                   
                    <div class="col-lg-8 col-md-7">
                        <div class="row">
                            <c:forEach items="${requestScope.data}" var="s">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic" >
                                            <img src="img/blog/${s.image}" style="width: 290px; height: 200px; object-fit: cover;">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i>${s.datePosted}</li>
                                                <li><i class="fa fa-comment-o">${s.totalComment}</i></li>
                                            </ul>
                                            <h5><a href="#">${s.title}</a></h5>
                                            <p>${s.opening}</p>
                                            <a type="submit" href="blogDetail?bid=${s.id}" class="blog__btn">READ MORE <span class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="product__pagination blog__pagination" style="display: flex; justify-content: center;">
                            <c:set var="page" value="${requestScope.page}" />
                            <div class="pagination">
                                <c:if test="${page gt 1}">
                                <li class="page-item" style="margin-right: 16px;">
                                    <a class="page-link" href="blog?page=${page-1}" style="display: flex; justify-content: center; align-items: center;">
                                        <i class="fa fa-long-arrow-left"></i>
                                    </a>
                                </li>
                                </c:if>
                                <c:forEach begin="${1}" end="${requestScope.num}" var="i">
                                    <a href="blog?page=${i}" class="${i==page?"active":""}">${i}</a>
                                </c:forEach>
                                <c:if test="${page lt requestScope.num}">
                                    <li class="page-item">
                                        <a class="page-link" href="blog?page=${page+1}" style="display: flex; justify-content: center; align-items: center;">
                                            <i class="fa fa-long-arrow-right"></i>
                                        </a>
                                    </li>
                                </c:if>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- Blog Section End -->

        <%@include file='footer.jsp'%>

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>



        <!--LamTP-->
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                document.querySelector("#login-popup").addEventListener("click", function () {
                    document.querySelector(".popup").classList.add("active");
                });

                document.querySelector(".popup .close-btn").addEventListener("click", function () {
                    document.querySelector(".popup").classList.remove("active");
                });
            });
        </script>
        <!--LamTP-->
    </body>

</html>