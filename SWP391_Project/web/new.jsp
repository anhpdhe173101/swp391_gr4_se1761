<%-- 
    Document   : home
    Created on : Jan 13, 2024, 4:46:23 PM
    Author     : Acer
--%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.Product" %>
<%@page import="entity.Category" %>
<%@page import="java.util.Vector" %>
<!DOCTYPE html>
<html lang="zxx">
    <!-- For home page: 
    - Show sliders, hot posts, featured products + the sider with the latest posts, static contacts/links
    - Shown slider information includes its image and title; the user is redirected to the slider's backlink on his/her clicking
    - Shown post information includes its thumbnail, title, brief-info; the user is redirected to the post's details on his/her clicking
    - Shown product information includes its thumbnail, title, brief information; the user is redirected to the product's details on his/her clicking -->
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Home</title> 
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <jsp:useBean id="i" class="model.SliderDAO" scope="request"/>
        <jsp:useBean id="t" class="model.ProductDAO" scope="request"/>
    </head>

    <body>

        <!-- DuyAnh -->
        <%
          Vector<Product> listP =(Vector<Product>)request.getAttribute("listP");
          Vector<Category> listCC =(Vector<Category>)request.getAttribute("listCC");
  Product last = (Product)request.getAttribute("p");
          Object value = request.getAttribute("txtS"); 
          String txtS; 
          if(value==null){ txtS=" "; }else txtS = (String)request.getAttribute("txtS");
        %>
        <!-- DuyAnh -->

        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>
        
        <!--Hoang Anh-->
        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="header__top__left">
                                <ul>
                                    <li><i class="fa fa-envelope"></i> hello@project.com</li>
                                    <li>Free Shipping for all Order of $50</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <!--LamTP-->  
                            <div class="header__top__right">
                                <c:if test="${sessionScope.account != null}">
                                    <c:if test="${sessionScope.account.roleID eq 2}">
                                        <div class="header__top__right__social">
                                            <a href="#">Manage Bills</a>
                                            <a href="#">Mangage product</a>
                                            <a href="#">Mangage Order</a>

                                        </div>
                                    </c:if>
                                    <div class="header__top__right__auth" >
                                        <a href="#"><i class="fa fa-user"></i> ${sessionScope.account.fullName}</a>
                                    </div>
                                    <div class="header__top__right__auth" style="margin-left: 20px;" >
                                        <a href="logout"> Logout</a>
                                    </div>
                                </c:if> 
                                <c:if test="${sessionScope.account == null}">  
                                    <div class="header__top__right__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </div>

                                    <div class="header__top__right__auth">
                                        <a href="#" id="login-popup"><i class="fa fa-user"></i> Login</a>
                                    </div>
                                </c:if>

                                <!--LamTP-->     


                                <!--LamTP-->
                                <div class="popup">
                                    <form action="login">
                                        <div class="close-btn">&times;</div>
                                        <div class="form">
                                            <h2>Log in</h2>
                                            <div class="form-element">
                                                <label for="email"> Email: </label>
                                                <input type="text" name="email" placeholder="Enter email" value="${cookie.cuser.value}">
                                            </div>
                                            <div class="form-element">
                                                <label for="password"> Password: </label>
                                                <input type="password" name="pass" placeholder="Enter password" value="${cookie.cpass.value}">
                                            </div>
                                            <div class="form-element">
                                                <input type="checkbox" id="remember-me" name="rem" value="on" 
                                                       ${cookie.crem.value eq 'on'?'checked':''} >
                                                <label for="remember-me">Remember me</label>
                                            </div>
                                            <div class="form-element">
                                                <button>Sign in</button>
                                            </div>
                                            <div class="form-element signup">
                                                <a href="#">Sign up</a>
                                                <a href="ForgotPassword.jsp">Forgot password?</a>
                                            </div>
                                        </div>
                                    </form>

                                </div>

                            </div>
                            <!--LamTP-->  
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="header__logo">
                            <a href="./home.jsp"><img src="img/p_logo.png" alt="" style="width: 60%"></a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <nav class="header__menu">
                            <ul>
                                <li class="active"><a href="./home.jsp">Home</a></li>
                                <li><a href="shop">Shop</a></li>
                                <li><a href="./blog.jsp">Blog</a></li>
                                <li><a href="./contact.jsp">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3">
                        <div class="header__cart">
                            <ul>
                                <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>0</span></a></li>
                            </ul>
                            <div class="header__cart__price">Total: <span>$000.00</span></div>
                        </div>
                    </div>
                </div>
                <div class="humberger__open">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
        </header>
        <!-- Header Section End -->
        <!--Hoang Anh-->

        <!--Hoang Anh-->
        <!-- Hero Section Begin -->
        <section class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="hero__categories">
                            <div class="hero__categories__all">
                                <i class="fa fa-bars"></i>
                                <span>All categories</span>
                            </div>
                            <!--Duy Anh-->
                            <ul>
                                <% for (Category o : listCC) { %>
                                <li><a href="category?cid=<%= o.getCateID() %>"><%=o.getName()%></a></li>
                                    <%}%>
                            </ul>
                            <!--Duy Anh-->
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="search" method="post">
                                    <input type="text" name="txt" placeholder="What do you need?">
                                    <button type="submit" class="site-btn">SEARCH</button>
                                </form>
                            </div>
                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>+84 81 661 3231</h5>
                                    <span>Support 24/7 time</span>
                                </div>
                            </div>
                        </div>
                        <div class="hero__item" style="background-image: url('img/background.jpg');">
                            <div class="slide-container" style="width: 100%;height: 100%;object-fit: cover;">
                                <c:forEach items="${requestScope.slider}" var="s">
                                    <c:set var="id" value="${s.id}"/> 
                                    <div class="slide">
                                        <a href="shop">
                                            <img  src="img/${s.image}">
                                        </a>
                                    </div>
                                </c:forEach>
                                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                                <a class="next" onclick="plusSlides(1)">&#10095;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Hero Section End -->
        <!--Hoang Anh-->

        <!--Hoang Anh-->
        <!-- Latest Product Section Begin -->
        <section class="latest-product spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title from-blog__title">
                            <h2>Latest product</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <c:forEach items="${requestScope.product}" var="p">
                            <c:set var="id" value="${p.id}"/>
                            <div class="card" style="width: 60%;">
                                <img src="img/product_image/${p.image}" class="card-img-top" alt="Product image">
                                <div class="card-body">
                                    <h5 class="card-title">${p.name}</h5>
                                    <p class="card-text">${p.origin_price}<span>${p.sale_price}</span></p>
                                    <a href="./product_detail.jsp" class="btn btn-primary">Detail</a>
                                </div>
                            </div>
                        </c:forEach>   
                    </div>
                </div>
                <div class="text-center">
                    <a href="shop" class="see">See more</a>
                </div>
            </div>
        </section>
        <!-- Latest Product Section End -->
        <!--Hoang Anh-->
        
        <!--Hoang Anh-->
        <!-- Blog Section Begin -->
        <section class="from-blog spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title from-blog__title">
                            <h2>From The Blog</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="img/blog/blog-1.jpg" alt="">
                            </div>
                            <div class="blog__item__text">
                                <ul>
                                    <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                                    <li><i class="fa fa-comment-o"></i> 5</li>
                                </ul>
                                <h5><a href="#">Cooking tips make cooking simple</a></h5>
                                <p>Sed quia non numquam modi tempora indunt ut labore et dolore magnam aliquam quaerat </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="img/blog/blog-2.jpg" alt="">
                            </div>
                            <div class="blog__item__text">
                                <ul>
                                    <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                                    <li><i class="fa fa-comment-o"></i> 5</li>
                                </ul>
                                <h5><a href="#">6 ways to prepare breakfast for 30</a></h5>
                                <p>Sed quia non numquam modi tempora indunt ut labore et dolore magnam aliquam quaerat </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="img/blog/blog-3.jpg" alt="">
                            </div>
                            <div class="blog__item__text">
                                <ul>
                                    <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                                    <li><i class="fa fa-comment-o"></i> 5</li>
                                </ul>
                                <h5><a href="#">Visit the clean farm in the US</a></h5>
                                <p>Sed quia non numquam modi tempora indunt ut labore et dolore magnam aliquam quaerat </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Blog Section End -->
        <!--Hoang Anh-->

        <!--Hoang Anh-->
        <!-- Footer Section Begin -->
        <footer class="footer spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="./index.html"><img src="img/p_logo.png" alt="" style="width: 60%"></a>
                            </div>
                            <ul>
                                <li>Address: Hoa Lac High-Tech Park, Km29 Thang Long Avenue, Thach That District, Hanoi, Vietnam</li>
                                <li>Phone: +84 81 661 3231</li>
                                <li>Email: hello@project.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                        <div class="footer__widget">
                            <h6>Informations and policies</h6>
                            <ul>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Warranty Policy</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="footer__widget">
                            <h6>Join Our Newsletter Now</h6>
                            <p>Get E-mail updates about our latest shop and special offers.</p>
                            <form action="#">
                                <input type="text" placeholder="Enter your mail">
                                <button type="submit" class="site-btn">Subscribe</button>
                            </form>
                            <div class="footer__widget__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment">
                                <h6>Payment methods</h6>
                                <img src="img/payment.png" alt="" style="width: 30%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->
        <!--Hoang Anh-->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
        <script>
                                        $(document).ready(function () {
                                            $('.slider').slick({
                                                autoplay: true,
                                                autoplaySpeed: 3000
                                            });
                                        });
        </script>

        <!--Hoang Anh-->
        <!-- This script controls a slideshow -->
        <script>
            var slideIndex = 1;
            showSlides(slideIndex);

            function plusSlides(n) {
                showSlides(slideIndex += n);
            }

            function showSlides(n) {
                var i;
                var slides = document.getElementsByClassName("slide");
                if (n > slides.length) {
                    slideIndex = 1
                }
                if (n < 1) {
                    slideIndex = slides.length
                }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                    slides[i].style.opacity = 0;
                }
                slides[slideIndex - 1].style.display = "block";
                setTimeout(function () {
                    slides[slideIndex - 1].style.opacity = 1;
                }, 50);
            }
        </script>
        <!--Hoang Anh-->

        <!--LamTP-->
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                document.querySelector("#login-popup").addEventListener("click", function () {
                    document.querySelector(".popup").classList.add("active");
                });

                document.querySelector(".popup .close-btn").addEventListener("click", function () {
                    document.querySelector(".popup").classList.remove("active");
                });
            });


        </script>
        <!--LamTP-->
    </body>

</html>