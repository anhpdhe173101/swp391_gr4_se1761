USE [master]
drop database if exists SWP391_SP24_Gr4
GO
/****** Object:  Database [SWP391_SP24_Gr4]    Script Date: 1/10/2024 9:46:03 PM ******/
CREATE DATABASE [SWP391_SP24_Gr4]
USE [SWP391_SP24_Gr4]
GO
/****** Object:  Table [dbo].[Cart]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userID] [int] NULL,
	[productID] [int] NULL,
	[quantity] [int] NULL,
 CONSTRAINT [PK_Cart] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](255) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContactInfo]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactInfo](
	[id] [int] NOT NULL,
	[fullName] [nvarchar](50) NULL,
	[phone] [varchar](50) NULL,
	[note] [nvarchar](255) NULL,
	[userID] [int] NULL,
	[Address] [nvarchar](255) NULL,
	[email] [varchar](50) NOT NULL
 CONSTRAINT [PK_ContactInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feature]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feature](
	[id] [int] IDENTITY(1,1)NOT NULL,
	[name] [nvarchar](50) NULL,
	[url] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
 CONSTRAINT [PK_Feature] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[id] [int] IDENTITY(1,1)NOT NULL,
	[userID] [int] NULL,
	[contactInfoID] [int] NULL,
	[date] [datetime] NULL,
	[note] [nvarchar](255) NULL,
	[statusID] [int] NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[orderID] [int] NULL,
	[productID] [int] NULL,
	[quantity] [int] NULL,
	[price] [int] NULL,
 CONSTRAINT [PK_OrderDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[id] [int] IDENTITY(1,1)NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](255) NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL,
	[categoryID] [int] NULL,
	[price] [float] NULL,
	[description] [nvarchar](255) NULL,
	[image] [nvarchar](max) NULL,
	[status] [bit] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductStock]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductStock](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productID] [int] NULL,
	[userID] [int] NULL,
	[quantity] [int] NULL,
	[date] [datetime] NULL,
 CONSTRAINT [PK_ProductStock] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](255) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleFeature]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleFeature](
	[roleID] [int] IDENTITY(1,1)NOT NULL,
	[featureID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SalePrice]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalePrice](
	[id] [int] IDENTITY(1,1)NOT NULL,
	[productID] [int] NULL,
	[startDate] [date] NULL,
	[endDate] [date] NULL,
	[price] [float] NULL,
 CONSTRAINT [PK_SalePrice] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Slider]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slider](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](255) NULL,
	[url] [nvarchar](255) NULL,
	[image] [nvarchar](max) NULL,
	[status] [bit] NULL,
 CONSTRAINT [PK_Slider] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 1/10/2024 9:46:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[id] [int] NOT NULL,
	[email] [varchar](50) NOT NULL UNIQUE ,
	[password] [varchar](64) NOT NULL,
	[roleID] [int] NULL,
	[confirmation] [bit] NULL,
	[status] [bit] NULL,
	[fullName] [nvarchar](50) NULL,
	[gender] [bit] NULL,
	[imageURL] [varchar](255) NULL,
	[Date] date
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [FK_Cart_Product] FOREIGN KEY([productID])
REFERENCES [dbo].[Product] ([id])
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [FK_Cart_Product]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [FK_Cart_User] FOREIGN KEY([userID])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [FK_Cart_User]
GO
ALTER TABLE [dbo].[ContactInfo]  WITH CHECK ADD  CONSTRAINT [FK_ContactInfo_User] FOREIGN KEY([userID])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[ContactInfo] CHECK CONSTRAINT [FK_ContactInfo_User]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_ContactInfo] FOREIGN KEY([contactInfoID])
REFERENCES [dbo].[ContactInfo] ([id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_ContactInfo]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderStatus] FOREIGN KEY([statusID])
REFERENCES [dbo].[OrderStatus] ([id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderStatus]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_User] FOREIGN KEY([userID])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_User]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Order] FOREIGN KEY([orderID])
REFERENCES [dbo].[Order] ([id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_Order]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Product] FOREIGN KEY([productID])
REFERENCES [dbo].[Product] ([id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_Product]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Category] FOREIGN KEY([categoryID])
REFERENCES [dbo].[Category] ([id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Category]
GO
ALTER TABLE [dbo].[ProductStock]  WITH CHECK ADD  CONSTRAINT [FK_ProductStock_Product] FOREIGN KEY([productID])
REFERENCES [dbo].[Product] ([id])
GO
ALTER TABLE [dbo].[ProductStock] CHECK CONSTRAINT [FK_ProductStock_Product]
GO
ALTER TABLE [dbo].[ProductStock]  WITH CHECK ADD  CONSTRAINT [FK_ProductStock_User] FOREIGN KEY([userID])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[ProductStock] CHECK CONSTRAINT [FK_ProductStock_User]
GO
ALTER TABLE [dbo].[RoleFeature]  WITH CHECK ADD  CONSTRAINT [FK_RoleFeature_Feature] FOREIGN KEY([featureID])
REFERENCES [dbo].[Feature] ([id])
GO
ALTER TABLE [dbo].[RoleFeature] CHECK CONSTRAINT [FK_RoleFeature_Feature]
GO
ALTER TABLE [dbo].[RoleFeature]  WITH CHECK ADD  CONSTRAINT [FK_RoleFeature_Role] FOREIGN KEY([roleID])
REFERENCES [dbo].[Role] ([id])
GO
ALTER TABLE [dbo].[RoleFeature] CHECK CONSTRAINT [FK_RoleFeature_Role]
GO
ALTER TABLE [dbo].[SalePrice]  WITH CHECK ADD  CONSTRAINT [FK_SalePrice_Product] FOREIGN KEY([productID])
REFERENCES [dbo].[Product] ([id])
GO
ALTER TABLE [dbo].[SalePrice] CHECK CONSTRAINT [FK_SalePrice_Product]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([roleID])
REFERENCES [dbo].[Role] ([id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO

CREATE TABLE BlogCategory (
    categoryID INT IDENTITY(1,1) PRIMARY KEY,
    categoryName NVARCHAR(MAX)
);


CREATE TABLE Blog (
    id INT IDENTITY(1,1) PRIMARY KEY,
	categoryID INT,
    authorID INT,
	image NVARCHAR(MAX),
    title NVARCHAR(MAX),
	opening NVARCHAR(MAX),
	date_posted DATE,
    content TEXT,
    FOREIGN KEY (categoryID) REFERENCES BlogCategory(categoryID),
    FOREIGN KEY (authorID) REFERENCES [User](id)
);

CREATE TABLE Product_Favorite (
    userID INT,
    productID INT,
    PRIMARY KEY (userID, productID), 
    FOREIGN KEY (userID) REFERENCES [dbo].[User] ([id]),
    FOREIGN KEY (productID) REFERENCES [dbo].[Product] ([id])
);
GO

CREATE TABLE Cookie_Identity (
	id INT IDENTITY(1,1) PRIMARY KEY
);
GO

CREATE TABLE Comment (
    CommentID INT IDENTITY(1,1) PRIMARY KEY,
    PostUserID INT,
    BlogID INT,
    CommentContent NVARCHAR(MAX),
	CommentDate DATE,
    FOREIGN KEY (PostUserID) REFERENCES [User](id),
    FOREIGN KEY (BlogID) REFERENCES Blog(id)
);
GO

CREATE TRIGGER AddCommentDate ON Comment INSTEAD OF INSERT
AS
BEGIN
    INSERT INTO Comment (PostUserID, BlogID, CommentContent, CommentDate)
    SELECT PostUserID, BlogID, CommentContent, GETDATE() FROM inserted;
END
GO

Create trigger addStockDate on ProductStock instead of Insert
AS
Begin
	insert into ProductStock(productID, userID, quantity, [date])
	select productID, userID, quantity, getDATE() from INSERTED;
End
GO

CREATE TRIGGER addBlogDate ON Blog INSTEAD OF INSERT
AS
BEGIN
    INSERT INTO Blog (categoryID, authorID, image, title, opening, date_posted, content)
    SELECT categoryID, authorID, image, title, opening, GETDATE(), content FROM inserted;
END
GO



Create table AfterChange(
aID int identity primary key,
[fullName] [nvarchar](50) NULL,
gender bit,
[phone] [varchar](50) NULL,
[Address] [nvarchar](255) NULL,
)

Create table BeforeChange(
bID int identity primary key,
[fullName] [nvarchar](50) NULL,
gender bit,
[phone] [varchar](50) NULL,
[Address] [nvarchar](255) NULL,
)


Create table History(
userID int,
editerID int,
dateChange Date,
afterID int,
beforeID int,
primary key(userID, editerID,beforeID,afterID),

FOREIGN KEY (userID) REFERENCES [dbo].[User] ([id]),
FOREIGN KEY (editerID) REFERENCES [dbo].[User] ([id]),
FOREIGN KEY (afterID) REFERENCES [dbo].[AfterChange] ([aID]),
FOREIGN KEY (beforeID) REFERENCES [dbo].[BeforeChange] ([bID]),
)
GO


CREATE TRIGGER InsertHistoryDateTrigger
ON History
AFTER INSERT
AS
BEGIN
    SET NOCOUNT ON;
    UPDATE History
    SET dateChange = GETDATE()
    FROM inserted
    WHERE History.userID = inserted.userID
    AND History.editerID = inserted.editerID
    AND History.beforeID = inserted.beforeID
    AND History.afterID = inserted.afterID;
END;

GO
CREATE TRIGGER UpdateGenderTrigger
ON AfterChange
AFTER INSERT
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRANSACTION;

    UPDATE bc
    SET bc.gender = NULL
    FROM BeforeChange bc
    INNER JOIN inserted i ON bc.bID = i.aID
    INNER JOIN AfterChange ac ON bc.bID = ac.aID AND ac.gender = bc.gender;

    UPDATE ac
    SET ac.gender = NULL
    FROM AfterChange ac
    INNER JOIN inserted i ON ac.aID = i.aID
    INNER JOIN BeforeChange bc ON ac.aID = bc.bID AND bc.gender IS NULL;

    COMMIT TRANSACTION;
END;

GO

CREATE TRIGGER UpdateUserDateTrigger
ON [User]
AFTER INSERT
AS
BEGIN
    SET NOCOUNT ON;

    UPDATE [User]
    SET [Date] = COALESCE(i.[Date], GETDATE())
    FROM [User]
    INNER JOIN inserted i ON [User].id = i.id
    WHERE i.[Date] IS NULL;
END;

GO
ALTER TABLE Blog
ADD Status BIT;
Go
ALTER TABLE Product
ADD season nvarchar(255);
GO

CREATE TRIGGER SetDefaultStatusOnBlogInsert
ON Blog
AFTER INSERT
AS
BEGIN
    UPDATE Blog
    SET Status = 1
    WHERE id IN (SELECT id FROM inserted)
END
